<?php

declare(strict_types=1);

namespace Cleever\App\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Integration\Api\IntegrationServiceInterface;
use Magento\Integration\Api\OauthServiceInterface;
use Magento\Integration\Model\Integration as IntegrationModel;
use Magento\Integration\Model\Oauth\Token\Provider as TokenProvider;
use Magento\Integration\Model\Oauth\TokenFactory as TokenFactory;
use Magento\Integration\Model\ResourceModel\Integration;
use Magento\Integration\Model\ResourceModel\Oauth\Consumer;
use Magento\Integration\Model\ResourceModel\Oauth\Token;

/**
 * Create integration
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2024 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CreateIntegration implements DataPatchInterface
{
    private const ACCESS_TOKEN = "k2rh3cjgi6sx0peny3i5rxlutwi9mpr3";
    private const TOKEN_SECRET = "s94evha1iks4h7siwta8ct6ygs1t6rad";
    private const CONSUMER_KEY = "yzqf4122x3ff9lt7cvzxfv13wsbc50k7";
    private const CONSUMER_SECRET = "a1l9tvymzsjd8es1ynp7gaosqcs5lp0f";
    /**
     * @var \Magento\Integration\Api\IntegrationServiceInterface
     */
    protected IntegrationServiceInterface $integrationService;
    /**
     * @var \Magento\Integration\Api\OauthServiceInterface
     */
    protected OauthServiceInterface $oauthService;
    /**
     * @var \Magento\Integration\Model\ResourceModel\Oauth\Consumer
     */
    protected Consumer $consumerResourceModel;
    /**
     * @var \Magento\Integration\Model\Oauth\TokenFactory
     */
    protected TokenFactory $tokenFactory;
    /**
     * @var \Magento\Integration\Model\Oauth\Token\Provider
     */
    protected TokenProvider $tokenProvider;
    /**
     * @var \Magento\Integration\Model\ResourceModel\Oauth\Token
     */
    protected Token $tokenResourceModel;
    /**
     * @var \Magento\Integration\Model\ResourceModel\Integration
     */
    protected Integration $integrationResourceModel;

    /**
     * Constructor
     *
     * @param \Magento\Integration\Api\IntegrationServiceInterface $integrationService
     * @param \Magento\Integration\Api\OauthServiceInterface $oauthService
     * @param \Magento\Integration\Model\ResourceModel\Oauth\Consumer $consumerResourceModel
     * @param \Magento\Integration\Model\Oauth\TokenFactory $tokenFactory
     * @param \Magento\Integration\Model\Oauth\Token\Provider $tokenProvider
     * @param \Magento\Integration\Model\ResourceModel\Oauth\Token $tokenResourceModel
     * @param \Magento\Integration\Model\ResourceModel\Integration $integrationResourceModel
     */
    public function __construct(
        IntegrationServiceInterface $integrationService,
        OauthServiceInterface $oauthService,
        Consumer $consumerResourceModel,
        TokenFactory $tokenFactory,
        TokenProvider $tokenProvider,
        Token $tokenResourceModel,
        Integration $integrationResourceModel
    ) {
        $this->integrationService = $integrationService;
        $this->oauthService = $oauthService;
        $this->consumerResourceModel = $consumerResourceModel;
        $this->tokenFactory = $tokenFactory;
        $this->tokenProvider = $tokenProvider;
        $this->tokenResourceModel = $tokenResourceModel;
        $this->integrationResourceModel = $integrationResourceModel;
    }

    /**
     * Get dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * Get aliases
     *
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * Create integration
     *
     * @return $this
     * @throws \Magento\Framework\Exception\IntegrationException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Oauth\Exception
     */
    public function apply(): CreateIntegration
    {
        // Create integration
        $integrationData = [
            "name" => "Cleever",
            "all_resources" => true,
        ];

        $integration = $this->integrationService->create($integrationData);

        // Create consumer
        $consumer = $this->oauthService->loadConsumer($integration->getConsumerId());
        $consumer->setKey(self::CONSUMER_KEY);
        $consumer->setSecret(self::CONSUMER_SECRET);
        $this->consumerResourceModel->save($consumer);

        // Create token
        $this->tokenFactory->create()->createVerifierToken($integration->getConsumerId());
        $this->tokenProvider->createRequestToken($consumer);
        $this->tokenProvider->getAccessToken($consumer);
        $token = $this->tokenProvider->getIntegrationTokenByConsumerId($integration->getConsumerId());
        $token->setToken(self::ACCESS_TOKEN);
        $token->setSecret(self::TOKEN_SECRET);
        $this->tokenResourceModel->save($token);

        // Activate integration
        $integration->setStatus(IntegrationModel::STATUS_ACTIVE);
        $this->integrationResourceModel->save($integration);

        return $this;
    }
}
