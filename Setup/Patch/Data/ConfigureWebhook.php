<?php

declare(strict_types=1);

namespace Cleever\App\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Cleever\App\Model\Api;

class ConfigureWebhook implements DataPatchInterface
{
    /**
     * @var \Cleever\App\Model\Api
     */
    protected Api $api;

    /**
     * @param \Cleever\App\Model\Api $api
     */
    public function __construct(
        Api $api
    ) {
        $this->api = $api;
    }

    /**
     * Get dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * Get aliases
     *
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * Configure secret key
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply(): ConfigureWebhook
    {
        $this->api->configureSecretKey();

        return $this;
    }
}
