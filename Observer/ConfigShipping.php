<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Cleever\App\Model\Api;

/**
 * Config shipping
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ConfigShipping extends AbstractConfig implements ObserverInterface
{
    /**
     * @var Api
     */
    protected Api $api;
    /**
     * @var Context
     */
    protected Context $context;

    /**
     * @param Context $context
     * @param Api $api
     */
    public function __construct(
        Context $context,
        Api $api
    ) {
        $this->api = $api;
        $this->context = $context;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     *
     * @return $this
     */
    public function execute(EventObserver $observer): ConfigShipping
    {
        /** @var mixed[] $groups */
        $groups = $this->context->getRequest()->getPost('groups');
        /** @var mixed[] $cleeverSettings */
        $cleeverSettings = $groups['cleever'];

        if (!isset($cleeverSettings)) {
            return $this;
        }
        /** @var mixed[] $fields */
        $fields = $cleeverSettings['fields'];

        $this->api->sendPluginStatus([
            'status' => 'update-slot-shipping-only',
            'options' => json_encode($fields),
        ]);

        return $this;
    }
}
