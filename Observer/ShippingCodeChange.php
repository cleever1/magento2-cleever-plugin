<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Observer change shipping code
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ShippingCodeChange implements ObserverInterface
{
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var CartRepositoryInterface
     */
    private CartRepositoryInterface $cartRepository;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param CartRepositoryInterface $cartRepository
     * @param SerializerInterface $serializer
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        CartRepositoryInterface $cartRepository,
        SerializerInterface $serializer
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->cartRepository = $cartRepository;
        $this->serializer = $serializer;
    }

    /**
     * @param Observer $observer
     *
     * @return $this
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer): ShippingCodeChange
    {
        /** @var OrderInterface $order */
        $order = $observer->getEvent()->getOrder();

        if ($order->getId()) {
            return $this;
        }

        if ($order->getShippingMethod() === 'cleever_cleever') {
            $quote = $this->cartRepository->get($order->getQuoteId());
            /** @var string $carrierReference */
            $carrierReference = $this->scopeConfig->getValue(
                'carriers/cleever/reference',
                ScopeInterface::SCOPE_STORE,
                $this->storeManager->getStore()
            );
            /** @var string $title */
            $title = $this->scopeConfig->getValue(
                'carriers/cleever/title',
                ScopeInterface::SCOPE_STORE,
                $this->storeManager->getStore()->getId()
            );

            $order->setShippingMethod($carrierReference);

            if (!empty($quote->getCleeverInformations())) {
                $cleeverInformation = $this->serializer->unserialize($quote->getCleeverInformations());

                $order->setShippingDescription('(' . $cleeverInformation['cleever_shipping_carrier'] . ')' . $title);
            }
        }

        return $this;
    }
}
