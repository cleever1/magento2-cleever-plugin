<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManager;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\Config\Source\PaymentMethod;
use Cleever\App\Model\ConfigProvider;

/**
 * Observer prevent email
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PreventEmail implements ObserverInterface
{
    /**
     * Payment method
     *
     * @var PaymentMethod
     */
    protected PaymentMethod $methodSource;

    /**
     * PreventEmail constructor
     *
     * @param PaymentMethod $paymentMethod
     */
    public function __construct(
        PaymentMethod $paymentMethod
    ) {
        $this->methodSource = $paymentMethod;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer): void
    {
        /** @var Payment $payment */
        $payment = $observer->getEvent()->getPayment();

        if (in_array($payment->getMethod(), $this->methodSource->getAvailablePaymentMethods())) {
            $payment->getOrder()->setCanSendNewEmailFlag(false);
        }
    }
}
