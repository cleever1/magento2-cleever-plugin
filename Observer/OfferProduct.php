<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class OfferProduct implements ObserverInterface
{
    protected ConfigProvider $configProvider;
    protected PriceCurrencyInterface $priceCurrency;
    protected Api $api;
    protected Session $checkoutSession;
    protected StoreManagerInterface $storeManager;

    /**
     * @param ConfigProvider $configProvider
     * @param Api $api
     * @param PriceCurrencyInterface $priceCurrency
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigProvider $configProvider,
        Api $api,
        PriceCurrencyInterface $priceCurrency,
        Session $checkoutSession,
        StoreManagerInterface $storeManager
    ) {
        $this->configProvider = $configProvider;
        $this->priceCurrency = $priceCurrency;
        $this->api = $api;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function execute(Observer $observer): void
    {
        $request = $observer->getEvent()->getRequest();

        if ($request->getParam('cleever_slot_product')) {
            if ($request->getParam('cleever_slot_product')) {
                $this->configProvider->createCookieSlotProduct($request->getParam('cleever_discount'));
            }
            $quote = $this->checkoutSession->getQuote();
            $this->api->sendAmplitudeRequest([
                'event_type' => 'Click - Product page - Add to cart',
                'event_properties' => [
                    'currency' => $quote->getQuoteCurrencyCode(),
                    'merchantID' => $this->configProvider->getMerchantId(),
                    'merchantSite' => $this->storeManager->getStore()->getName(),
                    'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                    'slotProductEnabled' => $this->configProvider->getSlotProductActive(),
                    'slotProductChecked' => $request->getParam('cleever_slot_product'),
                ],
            ]);
        }
    }
}
