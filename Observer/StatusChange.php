<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\Config\Source\PaymentMethod;
use Cleever\App\Model\ConfigProvider;

/**
 * Observer status change
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class StatusChange implements ObserverInterface
{
    /**
     * Payment method
     *
     * @var PaymentMethod
     */
    protected PaymentMethod $methodSource;
    /**
     * Cleever api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;

    /**
     * StatusChange constructor
     *
     * @param PaymentMethod          $paymentMethod
     * @param Api                    $api
     * @param StoreManagerInterface  $storeManager
     * @param ConfigProvider         $configProvider
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        PaymentMethod $paymentMethod,
        Api $api,
        StoreManagerInterface $storeManager,
        ConfigProvider $configProvider,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->methodSource   = $paymentMethod;
        $this->api            = $api;
        $this->storeManager   = $storeManager;
        $this->configProvider = $configProvider;
        $this->priceCurrency  = $priceCurrency;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     *
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer): void
    {
        /** @var OrderInterface $order */
        $order = $observer->getEvent()->getOrder();
        /** @var Payment $payment */
        $payment = $order->getPayment();

        // Manage the payment method ID
        /** @var string $paymentMethod */
        $paymentMethod = $payment->getMethod();
        if ($paymentMethod === 'cleever') {
            $paymentMethod = 'cleever-discount';
        } elseif ($paymentMethod === 'cleever_payment') {
            $paymentMethod = 'cleever-payment';
        }

        if ($order->getId() !== null && $payment->getId() !== null) {
            $this->api->sendAmplitudeRequest([
                'event_type'       => 'Order - Status',
                'event_properties' => [
                    'currency'           => $order->getOrderCurrencyCode(),
                    'currencySymbol'     => $this->priceCurrency->getCurrencySymbol(),
                    'merchantID'         => $this->configProvider->getMerchantId(),
                    'merchantSite'       => $this->storeManager->getStore()->getName(),
                    'merchantURL'        => $this->storeManager->getStore()->getBaseUrl(),
                    'orderID'            => $order->getIncrementId(),
                    'orderTotalValue'    => number_format((float)$order->getGrandTotal(), 2, '.', ','),
                    'paymentMethodID'    => $paymentMethod,
                    'paymentMethodLabel' => $payment->getAdditionalInformation('front_title') ?? $payment->getMethodInstance()->getTitle(),
                    'orderStatus'        => $order->getStatus(),
                ],
                'quote_id' => $order->getQuoteId()
            ]);
        }
    }
}
