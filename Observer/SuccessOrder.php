<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * Observer success order
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class SuccessOrder implements ObserverInterface
{
    /**
     * Cleever api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * Order repository interface
     *
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;

    /**
     * SuccessOrder constructor
     *
     * @param Api                      $api
     * @param StoreManagerInterface    $storeManager
     * @param ConfigProvider           $configProvider
     * @param PriceCurrencyInterface   $priceCurrency
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        Api $api,
        StoreManagerInterface $storeManager,
        ConfigProvider $configProvider,
        PriceCurrencyInterface $priceCurrency,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->api             = $api;
        $this->storeManager    = $storeManager;
        $this->configProvider  = $configProvider;
        $this->orderRepository = $orderRepository;
        $this->priceCurrency   = $priceCurrency;
    }

    /**
     * Execute
     *
     * @param Observer $observer
     *
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer): void
    {
        /** @var string[] $orderIds */
        $orderIds = $observer->getEvent()->getOrderIds();

        /** @var int $orderId */
        foreach ($orderIds as $orderId) {
            /** @var OrderInterface $order */
            $order = $this->orderRepository->get($orderId);
            /** @var Payment $payment */
            $payment = $order->getPayment();

            if ($order->getId() !== null && $payment->getId() !== null) {
                // Manage the payment method ID
                /** @var string $paymentMethod */
                $paymentMethod = $payment->getMethod();
                if ($paymentMethod === 'cleever') {
                    $paymentMethod = 'cleever-discount';
                } elseif ($paymentMethod === 'cleever_payment') {
                    $paymentMethod = 'cleever-payment';
                }

                $this->api->sendAmplitudeRequest([
                    'event_type'       => 'View - Order confirmation',
                    'event_properties' => [
                        'currency'           => $order->getOrderCurrencyCode(),
                        'currencySymbol'     => $this->priceCurrency->getCurrencySymbol(),
                        'merchantID'         => $this->configProvider->getMerchantId(),
                        'merchantSite'       => $this->storeManager->getStore()->getName(),
                        'merchantURL'        => $this->storeManager->getStore()->getBaseUrl(),
                        'orderID'            => $order->getIncrementId(),
                        'orderTotalValue'    => number_format((float)$order->getGrandTotal(), 2, '.', ','),
                        'orderStatus'        => $order->getStatus(),
                        'paymentMethodID'    => $paymentMethod,
                        'paymentMethodLabel' => $payment->getAdditionalInformation('front_title') ?? $payment->getMethodInstance()->getTitle(),
                    ],
                ]);
                // Send order data to coupon webservice
                if ($order->getCouponCode()) {
                    $streetFormatted = $order->getBillingAddress()->getStreet();
                    $data = [
                        'currency'           => $order->getOrderCurrencyCode(),
                        'currencySymbol'     => $this->priceCurrency->getCurrencySymbol(),
                        'merchantID'         => $this->configProvider->getMerchantId(),
                        'merchantSite'       => $this->storeManager->getStore()->getName(),
                        'merchantURL'        => $this->storeManager->getStore()->getBaseUrl(),
                        'orderID'            => $order->getIncrementId(),
                        'orderTotalValue'    => number_format((float)$order->getGrandTotal(), 2, '.', ','),
                        'paymentMethodID'    => $paymentMethod,
                        'paymentMethodLabel' => $payment->getAdditionalInformation('front_title') ?? $payment->getMethodInstance()->getTitle(),
                        'billing' => [
                            'first_name' => $order->getBillingAddress()->getFirstname(),
                            'last_name' => $order->getBillingAddress()->getLastname(),
                            'company' => $order->getBillingAddress()->getCompany(),
                            'address_1' => ($streetFormatted[0]) ?? '',
                            'address_2' =>  ($streetFormatted[1]) ?? '',
                            'city' =>  $order->getBillingAddress()->getCity(),
                            'state' => $order->getBillingAddress()->getRegion(),
                            'postcode' => $order->getBillingAddress()->getPostcode(),
                            'country' => $order->getBillingAddress()->getCountryId(),
                            'email' => $order->getBillingAddress()->getEmail(),
                            'phone' => $order->getBillingAddress()->getTelephone()
                        ],
                        'coupon' => [
                            'code' => $order->getCouponCode(),
                            'discount' => $order->getDiscountAmount(),
                        ]
                    ];
                    error_log(print_r($data, true), 3, '/var/www/html/staging-ecommerce/var/log/caro.log');
                    $this->api->sendToApiCoupon($data);
                }
            }
        }
    }
}
