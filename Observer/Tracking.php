<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\ScopeInterface;
use Cleever\App\Model\Api;
use Cleever\App\Provider\OrderProvider;

/**
 * Observer tracking
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Tracking extends AbstractConfig implements ObserverInterface
{
    /**
     * @var Api
     */
    protected Api $api;
    /**
     * @var Context
     */
    protected Context $context;
    /**
     * @var WriterInterface
     */
    protected WriterInterface $configWriter;
    /**
     * @var ManagerInterface
     */
    protected ManagerInterface $manager;

    /**
     * @param Context $context
     * @param Api $api
     * @param WriterInterface $configWriter
     * @param ManagerInterface $manager
     */
    public function __construct(
        Context $context,
        Api $api,
        WriterInterface $configWriter,
        ManagerInterface $manager
    ) {
        $this->api = $api;
        $this->context = $context;
        $this->configWriter = $configWriter;
        $this->manager = $manager;
    }

    /**
     * Execute
     *
     * @param EventObserver $observer
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(EventObserver $observer): Tracking
    {
        /** @var mixed[] $groups */
        $groups = $this->context->getRequest()->getPost('groups');
        /** @var mixed[] $cleeverSettings */
        $cleeverSettings = $groups['cleever'];

        if (!isset($cleeverSettings)) {
            return $this;
        }

        /** @var mixed[] $fields */
        $fields = $cleeverSettings['groups']['cleever_settings']['fields'];
        /** @var bool $valueCleeverDiscount */
        $valueCleeverDiscount = (isset($fields['active']['value']) ?? false) ?? $fields['active']['inherit'];
        /** @var bool $valueCleeverDiscount */
        $valueCleeverPayment = $fields['active_payment']['value'] ?? $fields['active_payment']['inherit'];
        /** @var string $websiteId */
        $websiteId = $this->context->getRequest()->getParam('website');
        /** @var string $storeId */
        $storeId = $this->context->getRequest()->getParam('store');
        /** @var string $scopeType */
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        /** @var int|string $scopeCode */
        $scopeCode = 0;

        if ($valueCleeverPayment !== null && !$valueCleeverPayment) {
            if ($storeId) {
                $scopeType = ScopeInterface::SCOPE_STORES;
                $scopeCode = $storeId;
            }

            if ($websiteId) {
                $scopeType = ScopeInterface::SCOPE_WEBSITES;
                $scopeCode = $websiteId;
            }
            $this->configWriter->save('carriers/cleever/active', $valueCleeverPayment, $scopeType, $scopeCode);
        }

        /** @var string|null $merchantId */
        $merchantId = $this->api->sendPluginStatus([
            'status' => ($valueCleeverDiscount) ? OrderProvider::STATUS_ENABLED : OrderProvider::STATUS_DISABLED,
            'status_main' => ($valueCleeverPayment) ? OrderProvider::STATUS_ENABLED : OrderProvider::STATUS_DISABLED,
            'options' => json_encode($cleeverSettings['groups']),
        ]);

        if (empty($merchantId)) {
            $this->manager->addErrorMessage(__('The publishable key or the secret key is wrong.'));
            $this->configWriter->save('payment/cleever_payment/active', 0, $scopeType, $scopeCode);
            $this->configWriter->save('payment/cleever/active', 0, $scopeType, $scopeCode);
            return $this;
        }

        $this->configWriter->save('payment/cleever/merchant_id', $merchantId);

        if (!$this->api->hasSecretkey()) {
            $this->api->configureSecretKey();
        }

        return $this;
    }
}
