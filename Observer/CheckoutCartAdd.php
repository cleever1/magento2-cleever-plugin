<?php

declare(strict_types=1);

namespace Cleever\App\Observer;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * Observer checkout cart add
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CheckoutCartAdd implements ObserverInterface
{
    /**
     * @var Api
     */
    protected Api $api;
    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * @var Session
     */
    protected Session $checkoutSession;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $config;
    /**
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;

    /**
     * @param Api $api
     * @param StoreManagerInterface $storeManager
     * @param Session $checkoutSession
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigProvider $config
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        Api $api,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        ConfigProvider $config,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->api = $api;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
        $this->config = $config;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer): void
    {
        $quote = $this->checkoutSession->getQuote();
        $cleeverShippingActive = $this->scopeConfig->isSetFlag(
            'carriers/cleever/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingPosition = $this->scopeConfig->getValue(
            'carriers/cleever/sort_order',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingLabel = $this->scopeConfig->getValue(
            'carriers/cleever/label',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingReference = $this->scopeConfig->getValue(
            'carriers/cleever/reference',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $this->api->sendAmplitudeRequest([
            'event_type' => 'Click - Add item to cart',
            'event_properties' => [
                'ecommerceSolution' => 'Magento2',
                'ecommerceVersion' => $this->config->getVersion(),
                'ecommerceShortVersion' => $this->config->getVersionShort(),
                'pluginVersion' => $this->config->getPluginVersion(),
                'currency' => $quote->getQuoteCurrencyCode(),
                'currencySymbol' => $this->priceCurrency->getCurrencySymbol(),
                'merchantID' => $this->config->getMerchantId(),
                'merchantSite' => $this->storeManager->getStore()->getName(),
                'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                'cleeverDiscountPosition' => $this->config->getPosition(),
                'cleeverPaymentPosition' => $this->config->getPositionPayment(),
                'orderTotalValue' => number_format((float)$quote->getGrandTotal(), 2, '.', ','),
                'slotShippingEnabled' => $cleeverShippingActive,
                'slotShippingPosition' => $cleeverShippingPosition,
                'slotShippingLabel' => $cleeverShippingLabel,
                'slotShippingReference' => $cleeverShippingReference,
            ],
        ]);
    }
}
