<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Psr\Log\LoggerInterface;

/**
 * Debug log
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Debug
{
    /**
     * Logger interface
     *
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $config;

    /**
     * Debug constructor
     *
     * @param LoggerInterface $logger
     * @param ConfigProvider  $configProvider
     */
    public function __construct(
        LoggerInterface $logger,
        ConfigProvider $configProvider
    ) {
        $this->logger = $logger;
        $this->config = $configProvider;
    }

    /**
     * Add debug message
     *
     * @param          $message
     * @param string[] $context
     *
     * @return $this
     */
    public function addDebugMessage($message, array $context = []): Debug
    {
        if ($this->config->isDebugModeEnabled()) {
            $this->logger->debug($message, $context);
        }

        return $this;
    }
}
