<?php

declare(strict_types=1);

namespace Cleever\App\Model\Message;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Cleever\App\Model\Config\Source\Environment;

/**
 * Api key message
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ApiKey implements MessageInterface
{
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;

    /**
     * @param UrlInterface $urlBuilder
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     *
     * @return string
     */
    public function getIdentity(): string
    {
        // phpcs:ignore Magento2.Security.InsecureFunction
        return md5('MOONA_API_KEY_INVALID');
    }

    /**
     *
     * @return bool
     */
    public function isDisplayed(): bool
    {
        /** @var bool $isStaging */
        $isStaging = ($this->scopeConfig->getValue('payment/cleever/environment') === Environment::ENVIRONMENT_TEST);
        /** @var string|null $publicKey */
        $publicKey = $this->scopeConfig->getValue(
            ($isStaging ? 'payment/cleever/test_public_key' : 'payment/cleever/live_public_key')
        );
        /** @var string|null $secretKey */
        $secretKey = $this->scopeConfig->getValue(
            ($isStaging ? 'payment/cleever/test_secret_key' : 'payment/cleever/live_secret_key')
        );

        return !$publicKey || !$secretKey;
    }

    /**
     *
     * @return Phrase
     */
    public function getText()
    {
        /** @var string $url */
        $url = $this->urlBuilder->getUrl('adminhtml/system_config/edit/section/payment/');

        //@codingStandardsIgnoreStart
        return __(
            'Cleever payment credentials are required. <a href="%1">Click here to enter you Cleever API keys</a>. These are required to process payments on your website.',
            $url
        );
        //@codingStandardsIgnoreEnd
    }

    /**
     *
     * @return int
     */
    public function getSeverity(): int
    {
        return self::SEVERITY_MAJOR;
    }
}
