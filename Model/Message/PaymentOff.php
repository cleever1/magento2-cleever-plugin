<?php

declare(strict_types=1);

namespace Cleever\App\Model\Message;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Notification\MessageInterface;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Cleever\App\Model\Config\Source\Environment;

/**
 * Payment off
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PaymentOff implements MessageInterface
{
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;

    /**
     * @param UrlInterface $urlBuilder
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     *
     * @return string
     */
    public function getIdentity(): string
    {
        // phpcs:ignore Magento2.Security.InsecureFunction
        return md5('MOONA_PAYMENT_OFF_INVALID');
    }

    /**
     *
     * @return bool
     */
    public function isDisplayed(): bool
    {
        /** @var bool $isCleeverPaymentActive */
        $isCleeverPaymentActive = $this->scopeConfig->isSetFlag('payment/cleever_payment/active');

        return !$isCleeverPaymentActive;
    }

    /**
     *
     * @return Phrase
     */
    public function getText(): Phrase
    {
        /** @var string $url */
        $url = $this->urlBuilder->getUrl('adminhtml/system_config/edit/section/payment/');

        //@codingStandardsIgnoreStart
        return __(
            'Cleever shipping slot can not be activated because Cleever payment is disabled. <a href="%1">Click here to set-up.</a>.',
            $url
        );
        //@codingStandardsIgnoreEnd
    }

    /**
     *
     * @return int
     */
    public function getSeverity(): int
    {
        return self::SEVERITY_MAJOR;
    }
}
