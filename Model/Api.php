<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\HTTP\Adapter\Curl;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\Oauth\Helper\Oauth;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Config\Source\PaymentMethod;
use Zend_Http_Client;
use Zend_Http_Response;

/**
 * Api cleever
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Api
{
    /**
     * Endpoint test
     *
     * @var string ENDPOINT_TEST
     */
    public const ENDPOINT_TEST = 'https://staging.api.moona.com';
    /**
     * Endpoint live
     *
     * @var string ENDPOINT_LIVE
     */
    public const ENDPOINT_LIVE = 'https://api.moona.com';
    /**
     * Endpoint amplitude
     *
     * @var string ENDPOINT_AMPLITUDE
     */
    public const ENDPOINT_AMPLITUDE = 'https://api2.amplitude.com/2/httpapi';
    /**
     * Key amplitude test
     *
     * @var string KEY_AMPLITUDE_TEST
     */
    public const KEY_AMPLITUDE_TEST = 'a19d8d2c919ec80110ac6d2353253c9e';
    /**
     * Key amplitude live
     *
     * @var string KEY_AMPLITUDE_LIVE
     */
    public const KEY_AMPLITUDE_LIVE = 'fceaa7f5144bfd64c3281bbca7cf0bac';
    /**
     * Path xml of secret key coupon for test mode
     */
    public const PATH_SECRET_KEY_COUPON_TEST_XML = 'payment/cleever/coupon_secret_test';
    /**
     * Path xml of secret key coupon for live mode
     */
    public const PATH_SECRET_KEY_COUPON_LIVE_XML = 'payment/cleever/coupon_secret_live';
    /**
     * Endpoint coupon live
     */
    public const ENDPOINT_COUPON_LIVE = 'https://toevk4xdp2.execute-api.eu-west-2.amazonaws.com/main/handlePaidOrders-prod';
    /**
     * Endpoint coupon test
     */
    public const ENDPOINT_COUPON_TEST = 'https://toevk4xdp2.execute-api.eu-west-2.amazonaws.com/staging/handlePaidOrders-dev';
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $config;
    /**
     * Body
     *
     * @var string
     */
    protected string $body = '';
    /**
     * Status
     *
     * @var int
     */
    protected int $status;
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;
    /**
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $cartRepository;
    /**
     * Curl factory
     *
     * @var CurlFactory
     */
    private CurlFactory $curlFactory;
    /**
     * Cookie manager interface
     *
     * @var PhpCookieManager
     */
    private PhpCookieManager $cookieManager;
    /**
     * @var Session
     */
    private Session $checkoutSession;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;
    /**
     * @var \Magento\Framework\Oauth\Helper\Oauth
     */
    protected Oauth $oauth;
    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected WriterInterface $writer;
    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected EncryptorInterface $encryptor;

    /**
     * Api constructor
     *
     * @param ConfigProvider $configProvider
     * @param CurlFactory $curlFactory
     * @param PhpCookieManager $cookieManager
     * @param Session $checkoutSession
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param SerializerInterface $serializer
     * @param CartRepositoryInterface $cartRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\Oauth\Helper\Oauth $oauth
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $writer
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     */
    public function __construct(
        ConfigProvider $configProvider,
        CurlFactory $curlFactory,
        PhpCookieManager $cookieManager,
        Session $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        SerializerInterface $serializer,
        CartRepositoryInterface $cartRepository,
        PriceCurrencyInterface $priceCurrency,
        Oauth $oauth,
        WriterInterface $writer,
        EncryptorInterface $encryptor
    ) {
        $this->config = $configProvider;
        $this->curlFactory = $curlFactory;
        $this->cookieManager = $cookieManager;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->serializer = $serializer;
        $this->cartRepository = $cartRepository;
        $this->priceCurrency = $priceCurrency;
        $this->oauth = $oauth;
        $this->writer = $writer;
        $this->encryptor = $encryptor;
    }

    /**
     * Get redirect url
     *
     * @param string[] $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getRedirectUrl(array $data): array
    {
        $this->initRequest('', $data);

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Init request
     *
     * @param string $url
     * @param string[] $data
     * @param int|null $storeId
     *
     * @throws LocalizedException
     */
    private function initRequest(string $url = '', array $data = [], int $storeId = null): void
    {
        $data['ecommerce_solution'] = ConfigProvider::ECOMMERCE_SOLUTION;
        $data['ecommerce_version'] = $this->config->getVersion();
        $data['plugin_version'] = $this->config->getPluginVersion();
        $data['api_version'] = ConfigProvider::API_VERSION;
        $data['currency'] = $this->priceCurrency->getCurrency()->getCurrencyCode();
        $data['website_code'] = $this->storeManager->getWebsite()->getCode();

        if ($this->config->getMerchantId($storeId)) {
            $data['merchant_id'] = $this->config->getMerchantId($storeId);
        }

        /** @var Curl $httpAdapter */
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(
            Zend_Http_Client::POST,
            ($this->config->isStaging($storeId) ? self::ENDPOINT_TEST : self::ENDPOINT_LIVE) . $url,
            '1.1',
            ['Content-Type:application/json; charset=utf-8', 'x-api-key:' . $this->config->getPublishableSecret($storeId)],
            json_encode($data)
        );
        /** @var string $result */
        $result = $httpAdapter->read();
        /** @var string body */
        $this->body = Zend_Http_Response::extractBody($result);
        /** @var int status */
        $this->status = Zend_Http_Response::extractCode($result);
        $httpAdapter->close();
    }

    /**
     * Get banners
     *
     * @param array $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getBanners(array $data): array
    {
        $this->initRequest('/ecommerce/banners', $data);
        $status = $this->status;
        $body = $this->body;

        $banners = [];
        if ($status === 200) {
            $banners = json_decode($body, true);
            $banners = $banners['banners'] ?? [];
        }

        return $banners;
    }

    /**
     * Get total
     *
     * @param string[] $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getTotal(array $data): array
    {
        $this->initRequest('/ecommerce/display_discount', $data);

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Get checkout banner
     *
     * @param string[] $data
     * @param string $method
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getCheckoutBanner(array $data, string $method = PaymentMethod::CLEEVER): array
    {
        $this->initRequest(
            $method === PaymentMethod::CLEEVER ? '/ecommerce/checkout' : '/ecommerce/checkout_main',
            $data
        );

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Init request amplitude
     *
     * @param string[] $data
     *
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function sendAmplitudeRequest(array $data = []): void
    {
        if (!$this->config->isActive()) {
            return;
        }

        /** @var bool $cleeverShippingActive */
        $cleeverShippingActive = $this->scopeConfig->isSetFlag(
            'carriers/cleever/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        /** @var string $cleeverShippingPosition */
        $cleeverShippingPosition = $this->scopeConfig->getValue(
            'carriers/cleever/sort_order',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        /** @var string $cleeverShippingLabel */
        $cleeverShippingLabel = $this->scopeConfig->getValue(
            'carriers/cleever/label',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        /** @var string $cleeverShippingReference */
        $cleeverShippingReference = $this->scopeConfig->getValue(
            'carriers/cleever/reference',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        /** @var bool $slotShippingDisplayed */
        $slotShippingDisplayed = $this->checkoutSession->getCleeverDisplayShipping();
        /** @var bool $slotShippingChecked */
        $slotShippingChecked = false;

        if (isset($data['quote_id'])) {
            $data['device_id'] = $this->getDeviceId($data['quote_id']);
        } else {
            $data['device_id'] = $this->getDeviceId();
        }
        $data['event_properties']['ecommerceSolution'] = ConfigProvider::ECOMMERCE_SOLUTION;
        $data['event_properties']['ecommerceVersion'] = $this->config->getVersion();
        $data['event_properties']['ecommerceShortVersion'] = $this->config->getVersionShort();
        $data['event_properties']['pluginVersion'] = $this->config->getPluginVersion();
        $data['event_properties']['slotShippingEnabled'] = $cleeverShippingActive;
        $data['event_properties']['slotShippingPosition'] = $cleeverShippingPosition;
        $data['event_properties']['slotShippingLabel'] = $cleeverShippingLabel;
        $data['event_properties']['slotShippingReference'] = $cleeverShippingReference;
        $data['event_properties']['slotShippingDisplayed'] = $slotShippingDisplayed;

        if ($this->checkoutSession->hasCleeverShipping() && !empty($this->checkoutSession->getCleeverShipping())) {
            $cleeverShippingInfo = $this->checkoutSession->getCleeverShipping();
            $quote = $this->checkoutSession->getQuote();

            if ($quote->getShippingAddress()->getShippingMethod() === 'cleever_cleever') {
                $slotShippingChecked = true;
            }
            $data['event_properties']['slotShippingMaxAllowedDiscount'] = $cleeverShippingInfo['discount'];
            $data['event_properties']['slotShippingPriceReference'] = $cleeverShippingInfo['shipping_price_reference'];
            $data['event_properties']['slotShippingTitle'] = $cleeverShippingInfo['title'];
            $data['event_properties']['slotShippingDescription'] = $cleeverShippingInfo['description'];
        }

        $data['event_properties']['slotShippingChecked'] = $slotShippingChecked;

        if ($this->config->isAbTestingEnable()) {
            if ($this->config->isStaging()) {
                $data['event_properties']['experienceId'] = 'CleeverExperience_ABTest_Percentage_Staging';
            } else {
                $data['event_properties']['experienceId'] = 'CleeverExperience_ABTest_Percentage_Prod';
            }

            $data['event_properties']['experienceName'] = 'Merchant - ABTest';

            if ($this->cookieManager->getCookie(ConfigProvider::COOKIE_NAME_ABTEST) === 'no') {
                $data['event_properties']['abVersion'] = 'cleeverNotDisplayed';
            } else {
                $data['event_properties']['abVersion'] = 'cleeverDisplayed';
            }

            $data['event_properties']['percentage'] = $this->config->getTrafficPercentage();
        }

        if (isset($data['quote_id'])) {
            unset($data['quote_id']);
        }

        /** @var Curl $httpAdapter */
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(
            Zend_Http_Client::POST,
            '1.1',
            ['Content-Type:application/json; charset=utf-8', 'Accept:*/*'],
            json_encode([
                'api_key' => ($this->config->isStaging() ? self::KEY_AMPLITUDE_TEST : self::KEY_AMPLITUDE_LIVE),
                'events' => [
                    $data,
                ],
            ])
        );
        /** @var string $result */
        $result = $httpAdapter->read();
        /** @var string body */
        $this->body = Zend_Http_Response::extractBody($result);

        /** @var int status */
        $this->status = Zend_Http_Response::extractCode($result);
        $httpAdapter->close();
    }

    /**
     * Get Device Id
     *
     * @param null $quoteId
     *
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getDeviceId($quoteId = null): ?string
    {
        $deviceId = null;

        if ($quoteId) {
            $quote = $this->cartRepository->get($quoteId);

            /** @var array[] $cleeverInformation */
            $cleeverInformation = [];

            if (!empty($quote->getCleeverInformations())) {
                $cleeverInformation = $this->serializer->unserialize($quote->getCleeverInformations());
            }
            if (isset($cleeverInformation['cleever_device_id']) && !empty($cleeverInformation['cleever_device_id'])) {
                $deviceId = $cleeverInformation['cleever_device_id'];
            }
        }

        if (!$deviceId) {
            $deviceId = $this->cookieManager->getCookie('device_id');
        }

        return $deviceId;
    }

    /**
     * Get shipping infos
     *
     * @param string[] $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getShippingInfos(array $data = []): array
    {
        $this->initRequest(
            '/ecommerce/get_shipping_slot_infos',
            $data
        );

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * @param array $data
     *
     * @return array
     * @throws LocalizedException
     */
    public function getProductInfos(array $data): array
    {
        $this->initRequest(
            '/ecommerce/product_page_offer',
            $data
        );

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Get subscriptions
     *
     * @param string[] $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getSubscriptions(array $data = []): array
    {
        $this->initRequest(
            '/ecommerce/get_shopper_infos',
            $data
        );

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Send Plugin Status
     *
     * @param string[] $data
     *
     * @return string|null
     * @throws LocalizedException
     */
    public function sendPluginStatus(array $data): ?string
    {
        $this->initRequest('/ecommerce/status', $data);

        /** @var int $status */
        $status = $this->status;
        /** @var string $body */
        $body = $this->body;

        /** @var string|null $merchantId */
        $merchantId = '';
        if ($status === 200) {
            $merchantId = json_decode($body, true);
            $merchantId = $merchantId['merchant_id'] ?? '';
        }

        return $merchantId;
    }

    /**
     * Get display mode
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function getDisplayMode(): array
    {
        $this->initRequest('/ecommerce/display_mode');

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Send refund
     *
     * @param string[] $data
     *
     * @return string[]
     * @throws LocalizedException
     */
    public function sendRefund(array $data, int $storeId): array
    {
        $this->initRequest('/ecommerce/refund', $data, $storeId);

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Send secret key
     *
     * @param array $data
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendSecretKey(array $data) : array
    {
        $this->initRequest('/ecommerce/merchant', $data);

        return [
            'error' => $this->status !== 200,
            'response' => $this->body,
        ];
    }

    /**
     * Has secret key
     *
     * @return bool
     */
    public function hasSecretkey(): bool
    {
        if ($this->config->isStaging()) {
            $value = self::PATH_SECRET_KEY_COUPON_TEST_XML;
        } else {
            $value = self::PATH_SECRET_KEY_COUPON_LIVE_XML;
        }

        return $this->scopeConfig->isSetFlag($value);
    }

    /**
     * Get secret key
     *
     * @return mixed
     */
    public function getSecretkey()
    {
        if ($this->config->isStaging()) {
            $value = self::PATH_SECRET_KEY_COUPON_TEST_XML;
        } else {
            $value = self::PATH_SECRET_KEY_COUPON_LIVE_XML;
        }

        return $this->scopeConfig->getValue($value);
    }

    /**
     * Configure secret key
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function configureSecretKey()
    {
        if ($this->config->isMoonaPaymentActive()) {
            $secretKey = $this->encryptor->encrypt($this->oauth->generateTokenSecret());
            if ($this->config->isStaging()) {
                $this->writer->save(Api::PATH_SECRET_KEY_COUPON_TEST_XML, $secretKey);
            } else {
                $this->writer->save(Api::PATH_SECRET_KEY_COUPON_LIVE_XML, $secretKey);
            }
            $this->sendSecretKey(['webhook_secret' => $secretKey]);
        }
    }

    /**
     * Send to api coupon
     *
     * @param array $data
     * @param int|null $storeId
     *
     * @return void
     */
    public function sendToApiCoupon(array $data = [], int $storeId = null)
    {
        $baseUrl = $this->scopeConfig->getValue('web/secure/base_url');
        /** @var Curl $httpAdapter */
        $httpAdapter = $this->curlFactory->create();
        $httpAdapter->write(
            Zend_Http_Client::POST,
            ($this->config->isStaging($storeId) ? self::ENDPOINT_COUPON_TEST : self::ENDPOINT_COUPON_LIVE),
            '1.1',
            ['Content-Type:application/json; charset=utf-8', 'x-magento-webhook-signature:' . $this->getSecretkey(), 'x-magento-webhook-source:' . $baseUrl, 'user-agent: Magento2'],
            json_encode($data)
        );
        /** @var string $result */
        $result = $httpAdapter->read();
        /** @var string body */
        $this->body = Zend_Http_Response::extractBody($result);
        /** @var int status */
        $this->status = Zend_Http_Response::extractCode($result);
        $httpAdapter->close();
    }
}
