<?php

declare(strict_types=1);

namespace Cleever\App\Model\Carrier;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Item;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\AbstractCarrierInterface;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\CarrierFactoryInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;
use Psr\Log\LoggerInterface;

/**
 * Carrier cleever
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Cleever extends AbstractCarrier implements CarrierInterface
{
    public const CODE = 'cleever';
    public const CARRIER_MATRIXRATE = 'matrixrate';
    /**
     * Code
     *
     * @var string
     */
    protected $_code = self::CODE;
    /**
     * Is fixed
     *
     * @var bool
     */
    protected $_isFixed = true;
    /**
     * result factory
     *
     * @var ResultFactory
     */
    protected ResultFactory $rateResultFactory;
    /**
     * Method factory
     *
     * @var MethodFactory
     */
    protected MethodFactory $rateMethodFactory;
    /**
     * Carrier factory interface
     *
     * @var CarrierFactoryInterface
     */
    protected CarrierFactoryInterface $carrierFactory;
    /**
     * api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * Checkout session
     *
     * @var Session
     */
    protected Session $session;
    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    /**
     * @var Resolver
     */
    protected Resolver $locale;
    /**
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;
    /**
     * @var Debug
     */
    protected Debug $debug;
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;
    /**
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $quoteRepository;
    /**
     * @var PhpCookieManager
     */
    protected PhpCookieManager $cookieManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param CarrierFactoryInterface $carrierFactory
     * @param Api $api
     * @param ConfigProvider $configProvider
     * @param Session $session
     * @param StoreManagerInterface $storeManager
     * @param Resolver $locale
     * @param PriceCurrencyInterface $priceCurrency
     * @param Debug $debug
     * @param SerializerInterface $serializer
     * @param CartRepositoryInterface $quoteRepository
     * @param PhpCookieManager $cookieManager
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        CarrierFactoryInterface $carrierFactory,
        Api $api,
        ConfigProvider $configProvider,
        Session $session,
        StoreManagerInterface $storeManager,
        Resolver $locale,
        PriceCurrencyInterface $priceCurrency,
        Debug $debug,
        SerializerInterface $serializer,
        CartRepositoryInterface $quoteRepository,
        PhpCookieManager $cookieManager,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);

        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->carrierFactory = $carrierFactory;
        $this->api = $api;
        $this->configProvider = $configProvider;
        $this->session = $session;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->locale = $locale;
        $this->priceCurrency = $priceCurrency;
        $this->debug = $debug;
        $this->serializer = $serializer;
        $this->quoteRepository = $quoteRepository;
        $this->cookieManager = $cookieManager;
    }

    /**
     * Collect rate
     *
     * @param RateRequest $request
     *
     * @return false|Result
     */
    public function collectRates(RateRequest $request)
    {
        $this->session->setCleeverDisplayShipping(false);

        if (!$this->getConfigFlag('active') || !$this->configProvider->isCleeverPaymentActive()) {

            return false;
        }

        /** @var Result $result */
        $result = $this->rateResultFactory->create();
        /** @var Method $method */
        $method = $this->rateMethodFactory->create();
        /** @var string $reference */
        $reference = $this->getConfigData('reference');
        /** @var string $label */
        $label = $this->getConfigData('label');
        /** @var float|null $shippingCost */
        $shippingCost = null;
        /** @var float|null $shippingCost */
        $oldShippingCost = null;
        /** @var string|null $email */
        $email = null;
        /** @var int $subscriptionStatus */
        $subscriptionStatus = 0;
        /** @var Quote|bool $quote */
        $quote = $this->getQuote($request);
        /** @var array[] $cleeverInformation */
        $cleeverInformation = [];

        if (!$reference || !$quote) {

            return false;
        }

        if (!empty($quote->getCleeverInformations())) {
            $cleeverInformation = $this->serializer->unserialize($quote->getCleeverInformations());
        }

        /** @var mixed[] $carrier */
        $carrier = explode('_', $reference);

        if (!isset($carrier[0], $carrier[1])) {
            return false;
        }

        /** @var bool|AbstractCarrierInterface $carrierReference */
        $carrierReference = $this->carrierFactory->createIfActive($carrier[0]);

        if (!$carrierReference) {
            return false;
        }

        /** @var bool|Result $resultReference */
        $resultReference = $carrierReference->collectRates($request);

        /** @var Method[] $rate */
        foreach ($resultReference->getAllRates() as $rate) {
            if ($carrier[1] === self::CARRIER_MATRIXRATE) {
                $carrierMatrixRate = explode('_', $rate->getData('method'));
                if ($carrierMatrixRate[0] !== $carrier[1]) {
                    continue;
                }
            } elseif ($rate->getData('method') !== $carrier[1]) {
                continue;
            }
            $shippingCost = (float)$rate->getPrice();
            $oldShippingCost = (float)$rate->getPrice() * 100;
            $methodTitle = $rate->getMethodTitle();
            $carrierTitle = $rate->getCarrierTitle();
        }

        if ($shippingCost === null || !$methodTitle || !$carrierTitle) {

            return false;
        }

        if ($this->session->hasCleeverSubscriptions() && !empty($this->session->getCleeverSubscriptions())) {
            $cleeverInformation['cleever_subscriptions'] = $this->session->getCleeverSubscriptions();
        }

        if (isset($cleeverInformation['cleever_subscriptions']) && isset($cleeverInformation['cleever_subscriptions']['sbfs'])) {
            $subscriptionStatus = $cleeverInformation['cleever_subscriptions']['sbfs'];
        }

        if ($this->session->hasCleeverEmail()) {
            $email = $this->session->getCleeverEmail();
        }

        if (!isset($cleeverInformation['cleever_shipping_api'])) {
            /** @var mixed[] $configShipping */
            $configShipping = $this->callShippingInfos(
                $quote,
                $label,
                $methodTitle,
                $carrierTitle,
                $oldShippingCost,
                $reference,
                ($quote->getGrandTotal() * 100),
                $email
            );
        } else {
            $configShipping = $cleeverInformation['cleever_shipping_api'];

            if ((isset($configShipping['call_on_update_shipping']) && $configShipping['call_on_update_shipping'])) {
                $configShipping = $this->callShippingInfos(
                    $quote,
                    $label,
                    $methodTitle,
                    $carrierTitle,
                    $oldShippingCost,
                    $reference,
                    ($quote->getGrandTotal() * 100),
                    $email
                );
            }
        }

        if (!isset($configShipping['new_user'], $configShipping['returning_user'])) {

            return false;
        }

        if ($subscriptionStatus === 0) {
            $configShipping = $configShipping['new_user'];
            $mode = 'newCustomer';
        } else {
            $configShipping = $configShipping['returning_user'];
            $mode = 'returningCustomer';
        }

        if (!isset($configShipping['display_slot']) || !$configShipping['display_slot']) {

            return false;
        }

        /** @var float $miniToDisplay */
        $minAmountToDisplay = (float)$configShipping['min_amount_to_display'] / 100;

        if ($quote->getSubtotal() < $minAmountToDisplay) {

            return false;
        }

        if (isset($configShipping['discount'])) {
            /** @var float $discount */
            $discount = (float)$configShipping['discount'] / 100;
            $shippingCost -= $discount;

            if ($shippingCost < 0) {
                $shippingCost = 0;
            }
        }

        $method->setCarrier($this->_code);

        if ((int)$shippingCost === 0) {
            $method->setCarrierTitle($configShipping['description_free']);
            $method->setMethodTitle($configShipping['title_free']);
        } else {
            $method->setCarrierTitle($configShipping['description']);
            $method->setMethodTitle($configShipping['title']);
        }

        $method->setMethod($this->_code);
        $method->setPrice($shippingCost);
        $method->setCost($shippingCost);

        /** @var string[] $shippingInfo */
        $shippingInfo = [
            'shipping_price_reference' => $oldShippingCost,
            'shipping_method_title' => $methodTitle,
            'shipping_carrier_title' => $carrierTitle,
            'shipping_reference' => $reference,
            'shipping_label' => $label,
            'discount' => (float)$configShipping['discount'],
            'min_amount_to_display' => $configShipping['min_amount_to_display'],
            'display_slot' => $configShipping['display_slot'],
            'title' => $method->getMethodTitle(),
            'description' => $method->getCarrierTitle(),
            'subscription_status' => $subscriptionStatus,
        ];

        $this->session->setCleeverShipping($shippingInfo);
        $this->session->setDeviceId($this->cookieManager->getCookie('device_id'));
        $this->session->setCleeverDisplayShipping(true);
        $this->session->setCleeverShippingCarrier($carrierTitle);

        $result->append($method);

        $this->api->sendAmplitudeRequest([
            'event_type' => 'View - Shipping slot',
            'event_properties' => [
                'merchantID' => $this->configProvider->getMerchantId(),
                'merchantSite' => $this->storeManager->getStore()->getName(),
                'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                'mode' => $mode
            ],
        ]);

        return $result;
    }

    /**
     * @param RateRequest $request
     *
     * @return false|Quote
     */
    private function getQuote(RateRequest $request)
    {
        /** @var mixed[] $items */
        $items = $request->getAllItems();

        if (empty($items)) {
            return false;
        }

        /** @var Item $firstItem */
        $firstItem = reset($items);

        if (!$firstItem) {
            return false;
        }

        $quote = $firstItem->getQuote();

        if (!($quote instanceof Quote)) {
            return false;
        }

        return $quote;
    }

    /**
     * @param Quote $quote
     * @param string|null $label
     * @param string|Phrase $methodTitle
     * @param string $carrierTitle
     * @param float $oldShippingCost
     * @param string $reference
     * @param float $total
     * @param string|null $email
     *
     * @return string[]
     * @throws LocalizedException
     */
    private function callShippingInfos(
        Quote $quote,
        $label,
        $methodTitle,
        string $carrierTitle,
        float $oldShippingCost,
        string $reference,
        float $total,
        string $email = null
    ): array {
        /** @var string[] $configShipping */
        $configShipping = [];

        /** @var string[] $data */
        $data = [
            'lang' => $this->locale->getLocale(),
            'currency_code' => $quote->getQuoteCurrencyCode(),
            'currency_symbol' => $this->priceCurrency->getCurrencySymbol(),
            'shipping_label' => $label,
            'shipping_method_title' => $methodTitle,
            'shipping_carrier_title' => $carrierTitle,
            'shipping_price_reference' => $oldShippingCost,
            'shipping_reference' => $reference,
            'cart_price' => $total,
        ];

        if ($this->session->hasCleeverSubscriptions() && !empty($this->session->getCleeverSubscriptions())) {
            $data['payload']['user_infos'] = $this->session->getCleeverSubscriptions();
        }

        if ($email) {
            $data['email'] = $email;
        }

        /** @var string[] $ShippingInfos */
        $shippingInfos = $this->api->getShippingInfos($data);

        if ($shippingInfos['error']) {
            $this->debug->addDebugMessage('An error has occured with api get_shipping_slot_infos. ' . $shippingInfos['response']);
        } else {
            /** @var mixed $responseShippingInfos */
            $responseShippingInfos = json_decode($shippingInfos['response'], true);
            $configShipping = $responseShippingInfos['body'];
            $this->session->setCleeverShippingApi($configShipping);
        }

        return $configShipping;
    }

    /**
     * Get Allowed Methods
     *
     * @return mixed[]
     */
    public function getAllowedMethods(): array
    {
        return [$this->_code => $this->getConfigData('title')];
    }
}
