<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Configuration checkout infos provider
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ConfigInfos implements ConfigProviderInterface
{
    /**
     * Checkout session
     *
     * @var Session
     */
    protected Session $session;
    /**
     * Api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * Scope config interface
     *
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;
    /**
     * @var CheckoutSession
     */
    protected CheckoutSession $checkoutSession;

    /**
     * ConfigInfos constructor
     *
     * @param Api $api
     * @param Debug $debug
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $session
     * @param CheckoutSession $checkoutSession
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Api $api,
        Debug $debug,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        Session $session,
        CheckoutSession $checkoutSession,
        SerializerInterface $serializer
    ) {
        $this->api = $api;
        $this->debug = $debug;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
        $this->serializer = $serializer;
    }

    /**
     * Get config
     *
     * @return mixed[]
     * @throws NoSuchEntityException
     */
    public function getConfig(): array
    {
        /** @var string[] $cleeverSubscriptionInfos */
        $cleeverSubscriptionInfos = [];
        $quote = $this->checkoutSession->getQuote();
        /** @var array[] $cleeverInformation */
        $cleeverInformation = [];

        if (!empty($quote->getCleeverInformations())) {
            $cleeverInformation = $this->serializer->unserialize($quote->getCleeverInformations());
        }

        // Get subscriptions infos
        if (isset($cleeverInformation['cleever_subscriptions'])) {
            $cleeverSubscriptionInfos['cleever_subscriptions'] = $cleeverInformation['cleever_subscriptions'];
        }

        if ($this->session->isLoggedIn() || ($this->checkoutSession->hasCleeverEmail() && !empty($this->checkoutSession->getCleeverEmail()))) {

            if ($this->session->isLoggedIn()) {
                /** @var string $email */
                $email = $quote->getCustomerEmail();
            }

            if ($this->checkoutSession->hasCleeverEmail()) {
                $email = $this->checkoutSession->getCleeverEmail();
            }

            /** @var string[] $data */
            $data = [
                'email' => $email
            ];

            /** @var string[] $subscriptions */
            $subscriptions = $this->api->getSubscriptions($data);

            if ($subscriptions['error']) {
                $this->debug->addDebugMessage('An error has occured with api get_shopper_infos. ' . $subscriptions['response']);
            } else {
                /** @var mixed $responseSubscriptionInfos */
                $responseSubscriptionInfos = json_decode($subscriptions['response'], true);
                $cleeverInformation['cleever_subscriptions'] = $responseSubscriptionInfos['body'];
                $this->checkoutSession->setCleeverSubscriptions($responseSubscriptionInfos['body']);
                $cleeverSubscriptionInfos['cleever_subscriptions'] = $cleeverInformation['cleever_subscriptions'];
                $this->session->setCleeverEmail($email);
            }
        }

        // Get cleever shipping infos
        /** @var int $cleeverShippingActive */
        $cleeverShippingActive = $this->scopeConfig->isSetFlag(
            'carriers/cleever/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        if ($cleeverShippingActive) {
            $cleeverSubscriptionInfos['cleever_shipping_active'] = $cleeverShippingActive;
        }

        return $cleeverSubscriptionInfos;
    }
}
