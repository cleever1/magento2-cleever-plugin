<?php

declare(strict_types=1);

namespace Cleever\App\Model\Method;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\Logger;
use Cleever\App\Model\AbstractMethod;
use Cleever\App\Model\Config;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Provider\OrderProvider;

/**
 * Cleever method
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CleeverPayment extends AbstractMethod
{
    /**
     * Method code cleever payment
     *
     * @var string METHOD_CODE
     */
    public const METHOD_CODE = 'cleever_payment';
    /**
     * Method title
     *
     * @var string METHOD_TITLE
     */
    private const METHOD_TITLE = 'SECURE PAYMENT';
    /**
     * @var string
     */
    protected $_code = self::METHOD_CODE;
    /**
     * @var string
     */
    protected $_infoBlockType = \Cleever\App\Block\Info\CleeverPayment::class;
    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param Config $config
     * @param OrderProvider $orderProvider
     * @param ConfigProvider $configProvider
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Config $config,
        OrderProvider $orderProvider,
        ConfigProvider $configProvider,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $config,
            $orderProvider,
            $resource,
            $resourceCollection,
            $data
        );

        $this->configProvider = $configProvider;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return self::METHOD_TITLE;
    }

    /**
     * @param null $storeId
     *
     * @return bool
     */
    public function isActive($storeId = null): bool
    {
        $merchantId = $this->configProvider->getMerchantId();

        if (!$merchantId) {
            return false;
        }
        return $this->configProvider->isCleeverPaymentActive();
    }
}
