<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Exception;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Config\Source\Environment;
use Cleever\App\Model\Method\Cleever;
use Cleever\App\Model\Method\CleeverPayment;

/**
 * Configuration checkout provider
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     * Plugin version
     *
     * @var string PLUGIN_VERSION
     */
    public const PLUGIN_VERSION = 'M2_2.2.6';
    /**
     * Api version
     *
     * @var string API_VERSION
     */
    public const API_VERSION = '1.0.0';
    /**
     * Ecommerce solution
     *
     * @var string ECOMMERCE_SOLUTION
     */
    public const ECOMMERCE_SOLUTION = 'Magento2';
    /**
     * AB_TESTING_ENABLE
     */
    public const AB_TESTING_ENABLE = 'ab_test';
    /**
     * TRAFFIC_PERCENTAGE
     */
    public const TRAFFIC_PERCENTAGE = 'traffic_percentage';
    /**
     * COOKIE_NAME_ABTEST
     */
    public const COOKIE_NAME_ABTEST = 'cleever_abtest';
    /**
     * Cookie name for slot product
     */
    public const COOKIE_NAME_SLOT_PRODUCT = 'cleever_slot_product';
    /**
     * Method Interface
     *
     * @var MethodInterface
     */
    protected MethodInterface $methodInstance;
    /**
     * Config
     *
     * @var Config
     */
    protected Config $config;
    /**
     * Url Interface
     *
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;
    /**
     * Encryptor Interface
     *
     * @var EncryptorInterface
     */
    protected EncryptorInterface $encryptor;
    /**
     * Product Metadata Interface
     *
     * @var ProductMetadataInterface
     */
    protected ProductMetadataInterface $productMetadata;
    /**
     * @var PriceCurrencyInterface
     */
    protected PriceCurrencyInterface $priceCurrency;
    /**
     * @var CookieManagerInterface
     */
    protected CookieManagerInterface $cookieManager;
    /**
     * @var CookieMetadataFactory
     */
    protected CookieMetadataFactory $cookieMetadataFactory;
    /**
     * @var ConfigInterface
     */
    protected ConfigInterface $sessionConfig;
    protected StoreManagerInterface $storeManager;

    /**
     * ConfigProvider constructor
     *
     * @param ConfigFactory $configFactory
     * @param UrlInterface $urlBuilder
     * @param EncryptorInterface $encryptor
     * @param ProductMetadataInterface $productMetadata
     * @param PriceCurrencyInterface $priceCurrency
     * @param CookieManagerInterface $cookieManager
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param ConfigInterface $sessionConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ConfigFactory $configFactory,
        UrlInterface $urlBuilder,
        EncryptorInterface $encryptor,
        ProductMetadataInterface $productMetadata,
        PriceCurrencyInterface $priceCurrency,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        ConfigInterface $sessionConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->config = $configFactory->create();
        $this->urlBuilder = $urlBuilder;
        $this->encryptor = $encryptor;
        $this->productMetadata = $productMetadata;
        $this->priceCurrency = $priceCurrency;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionConfig = $sessionConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Set store id
     *
     * @param $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId): ConfigProvider
    {
        $this->config->setStoreId($storeId);

        return $this;
    }

    /**
     * Set method instance
     *
     * @param $method
     *
     * @return $this
     */
    public function setMethodInstance($method): ConfigProvider
    {
        $this->methodInstance = $method;

        return $this;
    }

    /**
     * Get method code
     *
     * @return string|null
     */
    public function getMethodCode(): ?string
    {
        return $this->config->getMethodCode();
    }

    /**
     * Set method code
     *
     * @param $methodCode
     *
     * @return $this
     */
    public function setMethodCode($methodCode): ConfigProvider
    {
        $this->config->setMethodCode($methodCode);

        return $this;
    }

    /**
     * Get config
     *
     * @return string[]
     */
    public function getConfig(): array
    {
        return [
            'cleever_payment' => [
                'cancel_url' => $this->getBackRedirectUrl(),
                'return_url' => $this->getSuccessRedirectUrl(),
                'notification_url' => $this->getNotificationRedirectUrl(),
                'iframe_url' => $this->getIframeUrl(),
                'plugin_version' => $this->getPluginVersion(),
                'magento_version' => $this->getVersion(),
                'api_version' => self::API_VERSION,
                'active' => $this->isCleeverDiscountActive(),
                'mp_active' => $this->isCleeverPaymentActive(),
                'mode' => $this->getMode(),
                'cancel_redirect_type' => $this->getCancelCartRedirect(),
                'creation_order' => $this->getCreationOrder(),
                'slot_product' => $this->getSlotProductActive()
            ],
        ];
    }

    /**
     * Get Back Redirect Url
     *
     * @return string
     */
    protected function getBackRedirectUrl(): string
    {
        return $this->urlBuilder->getDirectUrl('cleever/payment/back');
    }

    /**
     * Get Success Redirect Url
     *
     * @return string
     */
    protected function getSuccessRedirectUrl(): string
    {
        return $this->urlBuilder->getDirectUrl('cleever/payment/successOrder');
    }

    /**
     * Get Notification Redirect Url
     *
     * @return string
     */
    protected function getNotificationRedirectUrl(): string
    {
        return $this->urlBuilder->getDirectUrl('cleever/payment/notify');
    }

    /**
     * Get iframe url
     *
     * @return string
     */
    protected function getIframeUrl(): string
    {
        return ($this->isStaging(
            ) ? 'https://staging.secure-payment-ecommerce.com/card-form?cms=Magento2' : 'https://secure-payment-ecommerce.com/card-form?cms=Magento2') . '&v=' . $this->getVersion(
            ) . '&pv=' . $this->getPluginVersion() . '&mid=' . $this->getMerchantId() . '&cur=' . $this->priceCurrency->getCurrency()->getCurrencyCode() . '&did=' . $this->cookieManager->getCookie('device_id');
    }

    /**
     * Is Staging
     *
     * @return bool
     */
    public function isStaging($storeId = null): bool
    {
        return ($this->config->getValue(
                'environment',
                $storeId,
                Cleever::METHOD_CODE
            ) === Environment::ENVIRONMENT_TEST);
    }

    /**
     * Get Version
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->productMetadata->getVersion();
    }

    /**
     * Get Plugin Version
     *
     * @return string
     */
    public function getPluginVersion(): string
    {
        return self::PLUGIN_VERSION;
    }

    /**
     * Get Merchant Id
     *
     * @param null $storeId
     *
     * @return string|null
     */
    public function getMerchantId($storeId = null): ?string
    {
        return $this->config->getValue(
            'merchant_id',
            $storeId,
            Cleever::METHOD_CODE
        );
    }

    /**
     * Is App Discount Active
     *
     * @return bool
     */
    public function isCleeverDiscountActive(): bool
    {
        $enable = (bool)$this->config->getValue('active', null, Cleever::METHOD_CODE);

        if ($this->isAbTestingEnable()) {
            $enable = (string)$this->cookieManager->getCookie(self::COOKIE_NAME_ABTEST);
            if (empty($enable)) {
                $enable = $this->createCookieAbTest();
            }
            if ($enable === 'yes') {
                $enable = true;
            } else {
                $enable = false;
            }
        }

        return $enable;
    }

    /**
     * @return bool
     */
    public function getSlotProductChecked(): bool
    {
        $productSlotChecked = $this->cookieManager->getCookie(self::COOKIE_NAME_SLOT_PRODUCT);

        return !empty($productSlotChecked);
    }

    /**
     * @param string $productSlotChecked
     *
     * @return void
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     */
    public function createCookieSlotProduct(string $productSlotChecked): void
    {
        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
        $cookieMetadata->setPath($this->sessionConfig->getCookiePath());
        $cookieMetadata->setDomain($this->sessionConfig->getCookieDomain());
        $cookieMetadata->setSecure($this->sessionConfig->getCookieSecure());
        $cookieMetadata->setHttpOnly(false);
        $cookieMetadata->setSameSite($this->sessionConfig->getCookieSameSite());
        $this->cookieManager->setPublicCookie(self::COOKIE_NAME_SLOT_PRODUCT, $productSlotChecked, $cookieMetadata);
    }

    /**
     * @return void
     * @throws FailureToSendException
     * @throws InputException
     */
    public function deleteCookieSlotProduct(): void
    {
        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
        $cookieMetadata->setPath($this->sessionConfig->getCookiePath());
        $cookieMetadata->setDomain($this->sessionConfig->getCookieDomain());
        $cookieMetadata->setSecure($this->sessionConfig->getCookieSecure());
        $cookieMetadata->setHttpOnly(false);
        $cookieMetadata->setSameSite($this->sessionConfig->getCookieSameSite());
        $this->cookieManager->deleteCookie(self::COOKIE_NAME_SLOT_PRODUCT, $cookieMetadata);
    }

    /**
     * @return bool
     */
    public function isAbTestingEnable(): bool
    {
        return (bool)$this->config->getValue(self::AB_TESTING_ENABLE, null, Cleever::METHOD_CODE);
    }

    /**
     * @return string
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     * @throws Exception
     */
    public function createCookieAbTest(): string
    {
        $randEnable = $this->getRandomAbTest();
        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
        $cookieMetadata->setPath($this->sessionConfig->getCookiePath());
        $cookieMetadata->setDomain($this->sessionConfig->getCookieDomain());
        $cookieMetadata->setSecure($this->sessionConfig->getCookieSecure());
        $cookieMetadata->setHttpOnly(false);
        $cookieMetadata->setSameSite($this->sessionConfig->getCookieSameSite());

        if ($randEnable) {
            $enable = 'yes';
        } else {
            $enable = 'no';
        }

        $this->cookieManager->setPublicCookie(self::COOKIE_NAME_ABTEST, $enable, $cookieMetadata);

        return $enable;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function getRandomAbTest(): bool
    {
        $percent = $this->getTrafficPercentage() / 100;
        $rand = (float)random_int(0, 100) / 100;

        return $rand <= $percent;
    }

    /**
     * @return string
     */
    public function getTrafficPercentage(): string
    {
        return $this->config->getValue(self::TRAFFIC_PERCENTAGE, null, Cleever::METHOD_CODE);
    }

    /**
     * Is App Discount Active
     *
     * @return bool
     */
    public function isCleeverPaymentActive(): bool
    {
        $enable = (bool)$this->config->getValue('active', null, CleeverPayment::METHOD_CODE);

        if ($this->isAbTestingEnable()) {
            $enable = (string)$this->cookieManager->getCookie(self::COOKIE_NAME_ABTEST);
            if (empty($enable)) {
                $enable = $this->createCookieAbTest();
            }
            if ($enable === 'yes') {
                $enable = true;
            } else {
                $enable = false;
            }
        }

        return $enable;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode(): string
    {
        return $this->config->getValue('mode', null, CleeverPayment::METHOD_CODE);
    }

    /**
     * @return string
     */
    public function getCancelCartRedirect(): string
    {
        return $this->config->getValue(
            'canceled_redirect_cart',
            null,
            Cleever::METHOD_CODE
        );
    }

    /**
     * @return string
     */
    public function getCreationOrder(): string
    {
        return $this->config->getValue(
            'order_creation_preference',
            null,
            Cleever::METHOD_CODE
        );
    }

    /**
     * Is Active
     *
     * @return bool
     */
    public function isActive(): bool
    {
        $cleeverDiscountActive = (bool)$this->config->getValue('active', null, Cleever::METHOD_CODE);
        $cleeverPaymentActive = (bool)$this->config->getValue('active', null, CleeverPayment::METHOD_CODE);

        if ($cleeverDiscountActive || $cleeverPaymentActive) {
            return true;
        }

        return false;
    }

    /**
     * Get Publishable Key
     *
     * @return string
     */
    public function getPublishableKey(): string
    {
        return $this->config->getValue(
            ($this->isStaging() ? 'test_public_key' : 'live_public_key'),
            null,
            Cleever::METHOD_CODE
        );
    }

    /**
     * Is Debug Mode Enabled
     *
     * @return bool
     */
    public function isDebugModeEnabled(): bool
    {
        return $this->config->isDebugModeEnabled();
    }

    /**
     * Get Publishable Secret
     *
     * @param null $storeId
     *
     * @return string
     */
    public function getPublishableSecret($storeId = null): string
    {
        $value = $this->config->getValue(
            ($this->isStaging() ? 'test_secret_key' : 'live_secret_key'),
            $storeId,
            Cleever::METHOD_CODE
        );

        return $this->encryptor->decrypt($value);
    }

    /**
     * Get Staging Publishable Secret
     *
     * @return string
     */
    public function getStagingPublishableSecret(): string
    {
        /** @var string $value */
        $value = $this->config->getValue('test_secret_key', null, Cleever::METHOD_CODE);

        return $this->encryptor->decrypt($value);
    }

    /**
     * Get Live Publishable Secret
     *
     * @return string
     */
    public function getLivePublishableSecret(): string
    {
        /** @var string $value */
        $value = $this->config->getValue('live_secret_key', null, Cleever::METHOD_CODE);

        return $this->encryptor->decrypt($value);
    }

    /**
     * Is Banner Available
     *
     * @param string $section
     *
     * @return bool
     */
    public function isBannerAvailable(string $section): bool
    {
        return (bool)$this->config->getValue($section);
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition(): int
    {
        /** @var string $position */
        $position = $this->config->getValue('sort_order', null, Cleever::METHOD_CODE);

        return $this->isCleeverDiscountActive() && !empty($position) ? (int)$position : -1;
    }

    /**
     * Get Position Payment
     *
     * @return int
     */
    public function getPositionPayment(): int
    {
        /** @var int $position */
        $position = $this->config->getPositionPayment();

        return $this->isCleeverPaymentActive() && !empty($position) ? (int)$position : -1;
    }

    /**
     * Get Version Short
     *
     * @return string
     */
    public function getVersionShort(): string
    {
        /** @var string[] $version */
        $version = explode('.', $this->getVersion());

        return $version[0] . '.' . $version[1];
    }

    /**
     * Is Allow Specific
     *
     * @return bool
     */
    public function isAllowSpecific(): bool
    {
        return (bool)$this->config->getValue('allowspecific', null, Cleever::METHOD_CODE);
    }

    /**
     * Get Specific Country
     *
     * @return string|null
     */
    public function getSpecificCountry(): ?string
    {
        return $this->config->getValue('specificcountry', null, Cleever::METHOD_CODE);
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function getSlotProductActive(): bool
    {
        $paymentActive = $this->isCleeverPaymentActive();
        $slotProductActive = $this->getSystemConfigValue('checkbox_product_enabled', $this->storeManager->getStore()->getId(), Cleever::METHOD_CODE);

        return $paymentActive && $slotProductActive;
    }

    /**
     * Get System Config Value
     *
     * @param      $key
     * @param null $storeId
     * @param null $methodCode
     *
     * @return string
     */
    public function getSystemConfigValue($key, $storeId = null, $methodCode = null): string
    {
        return $this->config->getValue($key, $storeId, $methodCode);
    }
}
