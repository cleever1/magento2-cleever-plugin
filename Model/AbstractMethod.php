<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Exception;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod as BaseAbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Cleever\App\Provider\OrderProvider;

/**
 * Abstract method payment
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
abstract class AbstractMethod extends BaseAbstractMethod
{
    /**
     * Config capture on invoice
     *
     * @var string CAPTURE_ON_INVOICE
     */
    public const CAPTURE_ON_INVOICE = 'invoice';
    /**
     * Config capture on shipment
     *
     * @var string CAPTURE_ON_SHIPMENT
     */
    public const CAPTURE_ON_SHIPMENT = 'shipment';
    /**
     * Channel name
     *
     * @var string CHANNEL_NAME
     */
    public const CHANNEL_NAME = 'App';
    /**
     * Can authorize
     *
     * @var bool
     */
    protected $_canAuthorize = true;
    /**
     * Can capture
     *
     * @var bool
     */
    protected $_canCapture = true;
    /**
     * @var bool
     */
    protected $_isInitializeNeeded = true;
    /**
     * Config
     *
     * @var Config
     */
    protected Config $config;
    /**
     * Can refund
     *
     * @var bool
     */
    protected $_canRefund = true;
    /**
     * Can refund invoice partially
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;
    /**
     * Order provider
     *
     * @var OrderProvider
     */
    protected OrderProvider $orderProvider;

    /**
     * @param Context                    $context
     * @param Registry                   $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory      $customAttributeFactory
     * @param Data                       $paymentData
     * @param ScopeConfigInterface       $scopeConfig
     * @param Logger                     $logger
     * @param Config                     $config
     * @param OrderProvider              $orderProvider
     * @param AbstractResource|null      $resource
     * @param AbstractDb|null            $resourceCollection
     * @param array                      $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Config $config,
        OrderProvider $orderProvider,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );

        $this->config        = $config;
        $this->orderProvider = $orderProvider;
    }

    /**
     * Get config data
     *
     * @param string $field
     * @param null   $storeId
     *
     * @return mixed|string|null
     */
    public function getConfigData($field, $storeId = null)
    {
        /** @var string $value */
        $value = $this->config->getValue($field, $storeId);

        if (empty($value)) {
            $value = parent::getConfigData($field, $storeId);
        }

        return $value;
    }

    /**
     * Refund
     *
     * @param InfoInterface $payment
     * @param float         $amount
     *
     * @return $this
     * @throws LocalizedException
     */
    public function refund(InfoInterface $payment, $amount): AbstractMethod
    {
        try {
            $this->orderProvider->refund($payment, $amount);
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }

        return $this;
    }
}
