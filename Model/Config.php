<?php

declare(strict_types=1);

namespace Cleever\App\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Model\MethodInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Configuration App
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Config implements ConfigInterface
{
    /**
     * Scope config interface
     *
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    /**
     * Store id
     *
     * @var int
     */
    protected $storeId;
    /**
     * Path pattern
     *
     * @var string
     */
    protected $pathPattern;
    /**
     * Method code
     *
     * @var string
     */
    protected $methodCode;

    /**
     * Config constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Set path pattern
     *
     * @param string $pathPattern
     *
     * @return void
     */
    public function setPathPattern($pathPattern): void
    {
        /** @var string pathPattern */
        $this->pathPattern = $pathPattern;
    }

    /**
     * Get value
     *
     * @param string $field
     * @param null   $storeId
     * @param null   $methodCode
     *
     * @return string|null
     */
    public function getValue($field, $storeId = null, $methodCode = null): ?string
    {
        /** @var string $underscored */
        $underscored = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $field));
        /** @var string $path */
        $path = $this->getSpecificConfigPath($underscored, $methodCode);
        if ($path !== null) {
            return $this->scopeConfig->getValue(
                $path,
                ScopeInterface::SCOPE_STORE,
                $storeId ?? $this->storeId
            );
        }

        return null;
    }

    /**
     * Get specific config path
     *
     * @param string $fieldName
     * @param null   $methodCode
     *
     * @return string|null
     */
    protected function getSpecificConfigPath(string $fieldName, $methodCode = null): ?string
    {
        /** @var string $methodCode */
        $methodCode = $methodCode ?? $this->methodCode;
        if ($this->pathPattern) {
            return sprintf($this->pathPattern, $methodCode, $fieldName);
        }

        return "payment/{$methodCode}/{$fieldName}";
    }

    /**
     * Is debug mode enabled
     *
     * @return bool
     */
    public function isDebugModeEnabled(): bool
    {
        return (bool)$this->scopeConfig->getValue(
            'payment/cleever/debug_mode',
            ScopeInterface::SCOPE_STORE,
            $this->storeId
        );
    }

    /**
     * Set method
     *
     * @param $method
     *
     * @return $this
     */
    public function setMethod($method): Config
    {
        if ($method instanceof MethodInterface) {
            /** @var string methodCode */
            $this->methodCode = $method->getCode();
        } elseif (is_string($method)) {
            /** @var string methodCode */
            $this->methodCode = $method;
        }

        return $this;
    }

    /**
     * Get method code
     *
     * @return string
     */
    public function getMethodCode(): string
    {
        return $this->methodCode;
    }

    /**
     * Set method code
     *
     * @param string $methodCode
     *
     * @return void
     */
    public function setMethodCode($methodCode): void
    {
        /** @var string methodCode */
        $this->methodCode = $methodCode;
    }

    /**
     * Set store id
     *
     * @param int|null $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId): Config
    {
        /** @var int storeId */
        $this->storeId = (int)$storeId;

        return $this;
    }

    /**
     * Get position payment
     *
     * @return int
     */
    public function getPositionPayment(): int
    {
        return (int)$this->scopeConfig->getValue(
            'payment/cleever_payment/sort_order',
            ScopeInterface::SCOPE_STORE,
            $this->storeId
        );
    }
}
