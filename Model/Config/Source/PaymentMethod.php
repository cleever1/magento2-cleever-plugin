<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Cleever\App\Model\Method\Cleever;
use Cleever\App\Model\Method\CleeverPayment;

/**
 * Source configuration payment
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PaymentMethod implements OptionSourceInterface
{
    /**
     * App method code
     *
     * @var string CLEEVER
     */
    public const CLEEVER = Cleever::METHOD_CODE;
    /**
     * App payment method code
     *
     * @var string CLEEVER_PAYMENT
     */
    public const CLEEVER_PAYMENT = CleeverPayment::METHOD_CODE;
    /**
     * Options
     *
     * @var array
     */
    protected array $options;

    /**
     * To option array
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            /** @var string[] $options */
            $options = [];
            /** @var string[] $paymentMethods */
            $paymentMethods = $this->getOptions();

            /**
             * @var string $value
             * @var string $label
             */
            foreach ($paymentMethods as $value => $label) {
                $options[] = [
                    'value' => $value,
                    'label' => $label,
                ];
            }

            $this->options = $options;
        }

        return $this->options;
    }

    /**
     * Get options
     *
     * @return string[]
     */
    public function getOptions(): array
    {
        return [
            self::CLEEVER => __('Cleever Discount'),
            self::CLEEVER_PAYMENT => __('Cleever Payment'),
        ];
    }

    /**
     * Get service name
     *
     * @param string $method
     *
     * @return string
     */
    public function getServiceName(string $method): string
    {
        switch ($method) {
            case self::CLEEVER_PAYMENT:
                /** @var string $serviceName */
                $serviceName = 'Cleever Payment';
                break;
            default:
                /** @var string $serviceName */
                $serviceName = 'Cleever Discount';
                break;
        }

        return $serviceName;
    }

    /**
     * Get Available Payment Methods
     *
     * @return mixed[]
     */
    public function getAvailablePaymentMethods(): array
    {
        return array_keys($this->getOptions());
    }

    /**
     * Get Payment Method Translation
     *
     * @param string $methodCode
     *
     * @return false|string
     */
    public function getPaymentMethodTranslation(string $methodCode)
    {
        $options = $this->getOptions();

        return $options[$methodCode] ?? false;
    }
}
