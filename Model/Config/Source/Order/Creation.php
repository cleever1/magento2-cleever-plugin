<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source\Order;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Creation implements OptionSourceInterface
{
    /**
     * Create order on payment
     */
    public const CREATE_ORDER_PAYMENT = 'payment';
    /**
     * Create order on checkout
     */
    public const CREATE_ORDER_CHECKOUT = 'checkout';

    /**
     * @return array[]
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::CREATE_ORDER_PAYMENT,
                'label' => __('Create order for successful payments only'),
            ],
            [
                'value' => self::CREATE_ORDER_CHECKOUT,
                'label' => __('Create order at checkout'),
            ],
        ];
    }
}
