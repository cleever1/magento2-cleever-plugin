<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source\Order\Status;

use Magento\Sales\Model\Config\Source\Order\Status;
use Magento\Sales\Model\Order;

/**
 * Source order status canceled
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Canceled extends Status
{
    /**
     * State canceld
     * @var string[]
     */
    protected $_stateStatuses = [
        Order::STATE_CANCELED,
        Order::STATE_HOLDED
    ];
}
