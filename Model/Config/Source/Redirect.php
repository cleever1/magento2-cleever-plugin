<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source;

/**
 * Source configuration redirection
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Redirect
{
    /**
     * Redirect to cart
     *
     * @var string TO_CART
     */
    public const TO_CART = 'to_cart';
    /**
     * Redirect to checkout
     *
     * @var string TO_CHECKOUT
     */
    public const TO_CHECKOUT = 'to_checkout';

    /**
     * Options of mode for payment
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::TO_CART,
                'label' => __('To cart'),
            ],
            [
                'value' => self::TO_CHECKOUT,
                'label' => __('To checkout'),
            ],
        ];
    }
}
