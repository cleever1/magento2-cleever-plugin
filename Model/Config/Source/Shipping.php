<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateRequestFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrierInterface;
use Magento\Shipping\Model\Config;
use Cleever\App\Model\Carrier\Cleever;

/**
 * Source configuration shipping
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Shipping
{
    /**
     * Shipping config
     *
     * @var Config
     */
    private Config $config;
    /**
     * Scope config
     *
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var RateRequestFactory
     */
    private RateRequestFactory $rateRequestFactory;
    /**
     * OWEBIA CODE
     */
    private const CODE_OWEBIA = 'owsh1';

    /**
     * Shipping constructor
     *
     * @param Config $config
     * @param ScopeConfigInterface $scopeConfig
     * @param RateRequestFactory $rateRequestFactory
     */
    public function __construct(
        Config $config,
        ScopeConfigInterface $scopeConfig,
        RateRequestFactory $rateRequestFactory
    ) {
        $this->config = $config;
        $this->scopeConfig = $scopeConfig;
        $this->rateRequestFactory = $rateRequestFactory;
    }

    /**
     * Options of mode for payment
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        /** @var string[] $methods */
        $methods = [];
        /** @var $request RateRequest */
        $request = $this->rateRequestFactory->create();
        /** @var AbstractCarrierInterface[] $activeCarriers */
        $activeCarriers = $this->config->getActiveCarriers();

        /** @var AbstractCarrierInterface $carrierModel */
        foreach ($activeCarriers as $carrierCode => $carrierModel) {
            if ($carrierModel->getCarrierCode() === Cleever::CODE) {
                continue;
            }
            /** @var string $carrierTitle */
            $carrierTitle = $this->scopeConfig->getValue('carriers/' . $carrierCode . '/title');
            /** @var string[] $options */
            $options = [];

            if ($carrierModel->getCarrierCode() === self::CODE_OWEBIA) {
                $config = $carrierModel->getConfig($request);

                if (!isset($config) || !is_array($config)) {
                    continue;
                }

                foreach ($config as $index => $item) {
                    $code      = $carrierCode . '_' . $index;
                    $options[] = ['value' => $code, 'label' => $index];
                }
            } elseif ($carrierMethods = $carrierModel->getAllowedMethods()) {
                /**
                 * @var string   $methodCode
                 * @var string[] $method
                 */
                foreach ($carrierMethods as $methodCode => $method) {
                    $code      = $carrierCode . '_' . $methodCode;
                    if (empty($method)) {
                        $method = $carrierTitle;
                    }
                    $options[] = ['value' => $code, 'label' => $method];
                }
            }

            $methods[] = ['value' => $options, 'label' => $carrierTitle];
        }

        return $methods;
    }
}
