<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source configuration environment
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Environment implements OptionSourceInterface
{
    /**
     * Environment production
     *
     * @var string ENVIRONMENT_PRODUCTION
     */
    public const ENVIRONMENT_PRODUCTION = 'production';
    /**
     * Environment test
     *
     * @var string ENVIRONMENT_TEST
     */
    public const ENVIRONMENT_TEST = 'test';

    /**
     * to Option Array
     *
     * @return array[]
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::ENVIRONMENT_TEST,
                'label' => __('Test'),
            ],
            [
                'value' => self::ENVIRONMENT_PRODUCTION,
                'label' => __('Production'),
            ],
        ];
    }
}
