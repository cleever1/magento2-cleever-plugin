<?php

declare(strict_types=1);

namespace Cleever\App\Model\Config\Source;

/**
 * Source configuration redirection
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Redirection
{
    /**
     * Mode integrated form
     *
     * @var string MODE_INTEGRATED
     */
    public const MODE_INTEGRATED = 'integrated';
    /**
     * Mode redirection
     *
     * @var string MODE_REDIRECTION
     */
    public const MODE_REDIRECTION = 'redirection';

    /**
     * Options of mode for payment
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => self::MODE_INTEGRATED,
                'label' => __('Integrated form'),
            ],
            [
                'value' => self::MODE_REDIRECTION,
                'label' => __('Redirection'),
            ],
        ];
    }
}
