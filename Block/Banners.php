<?php

declare(strict_types=1);

namespace Cleever\App\Block;

use Magento\Framework\View\Element\Template;
use Cleever\App\Helper\DataBanner;

/**
 * Manage display banners
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Banners extends Template
{
    /**
     * Section
     *
     * @var string
     */
    protected string $section;
    /**
     * Data banner
     *
     * @var DataBanner
     */
    protected DataBanner $dataBanner;
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'html/banner.phtml';

    /**
     * Banners constructor
     *
     * @param Template\Context $context
     * @param DataBanner       $dataBanner
     * @param array            $data
     */
    public function __construct(
        Template\Context $context,
        DataBanner $dataBanner,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->dataBanner = $dataBanner;
        $this->section = isset($data['section']) ? (string)$data['section'] : '';
    }

    /**
     * Display html
     *
     * @return string
     */
    protected function _toHtml(): string
    {
        if (!$this->dataBanner->isBannerAvailable($this->section)) {
            return '';
        }

        $html = '<div class="cleever-banner">';
        $html .= $this->dataBanner->getHtmlBanner($this->section);
        $html .= '</div>';

        if ($html !== '') {
            $this->setData('html', $html);
        }

        return parent::_toHtml();
    }
}
