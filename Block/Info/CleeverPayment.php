<?php

declare(strict_types=1);

namespace Cleever\App\Block\Info;

use Magento\Payment\Block\Info;

/**
 * Info payment
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CleeverPayment extends Info
{
    /**
     * @var string
     */
    protected $_template = 'Cleever_App::payment/info/cleever_payment.phtml';
}
