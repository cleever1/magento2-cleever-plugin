<?php

declare(strict_types=1);

namespace Cleever\App\Block\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\Render;
use Magento\Framework\View\Element\Template;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Price extends Template
{
    private ProductInterface $product;

    /**
     * @return null
     */
    public function getCacheLifetime()
    {
        return null;
    }

    /**
     * @return ProductInterface
     */
    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    /**
     * @param ProductInterface $product
     *
     * @return $this
     */
    public function setProduct(ProductInterface $product): Price
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @param Product $product
     * @param string $priceType
     * @param string $renderZone
     * @param array $arguments
     *
     * @return string
     * @throws LocalizedException
     */
    public function getProductPriceHtml(
        Product $product,
        string $priceType,
        string $renderZone = Render::ZONE_ITEM_VIEW,
        array $arguments = []
    ): string {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }

        $priceRender = $this->getLayout()->getBlock('cleever.product.price.render.default');
        $price = '';

        if ($priceRender) {
            $price = $priceRender->render($priceType, $product, $arguments);
        }

        return $price;
    }
}
