<?php

declare(strict_types=1);

namespace Cleever\App\Block\Adminhtml\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Block enabled
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Enable extends Field
{
    /**
     * ELEMENT_ACTIVE
     */
    protected const ELEMENT_ACTIVE = 'carriers_cleever_active';
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;

    /**
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param array $data
     * @param SecureHtmlRenderer|null $secureRenderer
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data, $secureRenderer);

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     * @throws NoSuchEntityException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $cleeverPayment = $this->scopeConfig->isSetFlag(
            'payment/cleever_payment/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverCarrier = $this->scopeConfig->isSetFlag(
            'carriers/cleever/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );

        if ($element->getId() === self::ELEMENT_ACTIVE) {
            if ($cleeverPayment) {
                $element->setComment(
                    __('This activates the free shipping service. Select "Yes" and choose below the shipping method which will be associated to free shipping.')
                );
            } else {
                $element->setComment(
                    __('To enable the shipping service, you must enable Cleever payment above. The shipping service can only be processed through Cleever payment. <a href="%1">Click here to enable Cleever payment</a>.', $this->getUrl('adminhtml/system_config/edit/section/payment/'))
                );
            }
        }

        if (!$cleeverPayment || (!$cleeverCarrier && $element->getId() !== self::ELEMENT_ACTIVE)) {
            $element->setDisabled(true);
        }

        return parent::_getElementHtml($element);
    }
}
