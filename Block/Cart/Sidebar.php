<?php

declare(strict_types=1);

namespace Cleever\App\Block\Cart;

use Magento\Catalog\Helper\Image;
use Magento\Checkout\Block\Cart\Sidebar as BaseSidebar;
use Magento\Checkout\Model\Session;
use Magento\Customer\CustomerData\JsLayoutDataProviderPoolInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;
use Magento\Framework\View\Design\ThemeInterface;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

/**
 * Cart sidebar change template
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Sidebar extends BaseSidebar
{
    /**
     * Theme provider interface
     *
     * @var ThemeProviderInterface
     */
    private ThemeProviderInterface $themeProvider;

    /**
     * Sidebar constructor
     *
     * @param Context                           $context
     * @param CustomerSession                   $customerSession
     * @param Session                           $checkoutSession
     * @param Image                             $imageHelper
     * @param JsLayoutDataProviderPoolInterface $jsLayoutDataProvider
     * @param ThemeProviderInterface            $themeProvider
     * @param array                             $data
     * @param Json|null                         $serializer
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        Session $checkoutSession,
        Image $imageHelper,
        JsLayoutDataProviderPoolInterface $jsLayoutDataProvider,
        ThemeProviderInterface $themeProvider,
        array $data = [],
        Json $serializer = null
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $imageHelper,
            $jsLayoutDataProvider,
            $data,
            $serializer
        );

        $this->themeProvider = $themeProvider;
    }

    /**
     * Prepare layout
     *
     * @return $this
     */
    protected function _prepareLayout(): Sidebar
    {
        parent::_prepareLayout();

        /** @var string $themeId */
        $themeId = $this->_scopeConfig->getValue(
            DesignInterface::XML_PATH_THEME_ID,
            ScopeInterface::SCOPE_WEBSITES
        );

        /** @var ThemeInterface $theme */
        $theme = $this->themeProvider->getThemeById($themeId);
        /** @var ThemeInterface $parentTheme */
        $parentTheme = $theme->getParentTheme();

        if (($theme->getCode() === 'Smartwave/porto')
            || ($parentTheme && $parentTheme->getCode() === 'Smartwave/porto')) {
            $this->setTemplate('Cleever_App::cart/minicart.phtml');
        }

        return $this;
    }
}
