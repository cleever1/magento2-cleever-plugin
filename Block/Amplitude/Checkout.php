<?php

declare(strict_types=1);

namespace Cleever\App\Block\Amplitude;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\Config;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * Checkout amplitude
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Checkout extends Template
{
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    private ConfigProvider $config;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;
    /**
     * Json
     *
     * @var Json
     */
    private Json $json;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var Session
     */
    protected Session $checkoutSession;
    /**
     * @var PhpCookieManager
     */
    private PhpCookieManager $cookieManager;
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * Init constructor
     *
     * @param Template\Context $context
     * @param ConfigProvider $config
     * @param PriceCurrencyInterface $priceCurrency
     * @param Json $json
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Session $checkoutSession
     * @param PhpCookieManager $cookieManager
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ConfigProvider $config,
        PriceCurrencyInterface $priceCurrency,
        Json $json,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        PhpCookieManager $cookieManager,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->config = $config;
        $this->priceCurrency = $priceCurrency;
        $this->json = $json;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
        $this->cookieManager = $cookieManager;
        $this->request = $request;
    }

    /**
     * Get api key
     *
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->config->isStaging() ? Api::KEY_AMPLITUDE_TEST : Api::KEY_AMPLITUDE_LIVE;
    }

    /**
     * Get Options
     *
     * @return string
     */
    public function getOptions(): string
    {
        /** @var string[] $options */
        $options = [];

        if ($this->getCookieDomain()) {
            $options['domain'] = $this->getCookieDomain();
        }

        return $this->json->serialize($options);
    }

    /**
     * Get cookie domain
     *
     * @return string
     */
    public function getCookieDomain(): string
    {
        return !empty($this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_DOMAIN)) ? $this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_DOMAIN) : $this->request->getHttpHost();
    }

    /**
     * Get cookie path
     *
     * @return string
     */
    public function getCookiePath(): string
    {
        return $this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_PATH) ?? '/';
    }

    /**
     * Get data event
     *
     * @return string
     * @throws NoSuchEntityException|LocalizedException
     */
    public function getDataEvent(): string
    {
        $quote = $this->checkoutSession->getQuote();
        $cleeverShippingActive = $this->scopeConfig->isSetFlag(
            'carriers/cleever/active',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingPosition = $this->scopeConfig->getValue(
            'carriers/cleever/sort_order',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingLabel = $this->scopeConfig->getValue(
            'carriers/cleever/label',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $cleeverShippingReference = $this->scopeConfig->getValue(
            'carriers/cleever/reference',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );

        /** @var mixed[] $data */
        $data = [
            'ecommerceSolution'     => 'Magento2',
            'ecommerceVersion'      => $this->config->getVersion(),
            'ecommerceShortVersion' => $this->config->getVersionShort(),
            'pluginVersion'         => $this->config->getPluginVersion(),
            'currency'              => $quote->getQuoteCurrencyCode(),
            'currencySymbol'        => $this->priceCurrency->getCurrencySymbol(),
            'merchantID'            => $this->config->getMerchantId(),
            'merchantSite'          => $this->_storeManager->getStore()->getName(),
            'merchantURL'           => $this->_storeManager->getStore()->getBaseUrl(),
            'cleeverDiscountPosition' => $this->config->getPosition(),
            'cleeverPaymentPosition'  => $this->config->getPositionPayment(),
            'orderTotalValue'       => number_format((float)$quote->getGrandTotal(), 2, '.', ','),
            'slotShippingEnabled'   => $cleeverShippingActive,
            'slotShippingPosition'  => $cleeverShippingPosition,
            'slotShippingLabel'     => $cleeverShippingLabel,
            'slotShippingReference' => $cleeverShippingReference
        ];

        if ($this->config->isAbTestingEnable()) {
            if ($this->config->isStaging()) {
                $data['experienceId'] = 'CleeverExperience_ABTest_Percentage_Staging';
            } else {
                $data['experienceId'] = 'CleeverExperience_ABTest_Percentage_Prod';
            }

            $data['experienceName'] = 'Merchant - ABTest';

            if ($this->cookieManager->getCookie(ConfigProvider::COOKIE_NAME_ABTEST) === 'no') {
                $data['abVersion'] = 'cleeverNotDisplayed';
            } else {
                $data['abVersion'] = 'cleeverDisplayed';
            }

            $data['percentage'] = $this->config->getTrafficPercentage();
        }

        return $this->json->serialize($data);
    }

    /**
     * Description _toHtml function
     *
     * @return string
     */
    protected function _toHtml(): string
    {
        if (!$this->config->isActive()) {
            return '';
        }

        return parent::_toHtml();
    }
}
