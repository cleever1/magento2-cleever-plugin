<?php

declare(strict_types=1);

namespace Cleever\App\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Session\Config;
use Magento\Framework\View\Element\Template;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CleeverInit extends Template
{
    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;
    /**
     * @var Json
     */
    private Json $json;
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @param Template\Context $context
     * @param ConfigProvider $configProvider
     * @param Json $json
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ConfigProvider $configProvider,
        Json $json,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->configProvider = $configProvider;
        $this->json = $json;
        $this->request = $request;
    }

    /**
     * @return bool
     */
    public function isAbTestingEnable(): bool
    {
        return $this->configProvider->isAbTestingEnable();
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->configProvider->isStaging() ? Api::KEY_AMPLITUDE_TEST : Api::KEY_AMPLITUDE_LIVE;
    }

    /**
     * Get Options
     *
     * @return string
     */
    public function getOptions(): string
    {
        /** @var string[] $options */
        $options = [];

        if ($this->getCookieDomain()) {
            $options['domain'] = $this->getCookieDomain();
        }

        return $this->json->serialize($options);
    }

    /**
     * Get cookie domain
     *
     * @return string
     */
    public function getCookieDomain(): string
    {
        return !empty($this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_DOMAIN)) ? $this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_DOMAIN) : $this->request->getHttpHost();
    }

    /**
     * Get cookie path
     *
     * @return string
     */
    public function getCookiePath(): string
    {
        return $this->_scopeConfig->getValue(Config::XML_PATH_COOKIE_PATH) ?? '/';
    }

    /**
     * @return bool
     */
    public function isCleeverActive(): bool
    {
        return $this->configProvider->isActive();
    }
}
