<?php

declare(strict_types=1);

namespace Cleever\App\Block;

use Magento\Framework\View\Element\Template;

/**
 * Build iframe form
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class IframeForm extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Cleever_App::payment/iframe-form.phtml';

    /**
     * Get Url Iframe
     *
     * @return string
     */
    public function getUrlIframe(): string
    {
        return $this->getData('url_iframe');
    }

    /**
     * Get Display
     *
     * @return string
     */
    public function getDisplay(): string
    {
        return $this->getData('display');
    }
}
