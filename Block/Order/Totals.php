<?php

declare(strict_types=1);

namespace Cleever\App\Block\Order;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\AbstractBlock;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Totals extends AbstractBlock
{
    /**
     * @return void
     */
    public function initTotals(): void
    {
        $orderTotalsBlock = $this->getParentBlock();
        $order = $orderTotalsBlock->getOrder();
        $additionalInfos = $order->getPayment()->getAdditionalInformation();

        if (isset($additionalInfos['cleever_discount_amount']) && ($additionalInfos['cleever_discount_amount'] > 0)) {
            $orderTotalsBlock->addTotal(new DataObject([
                'code'       => 'cleever_total',
                'label'      => __('New total'),
                'strong'     => true,
                'value'      => $order->getGrandTotal() - ($additionalInfos['cleever_discount_amount'] / 100),
                'base_value' => $order->getGrandTotal() - ($additionalInfos['cleever_discount_amount'] / 100),
            ]), 'grand_total');
        }
    }
}
