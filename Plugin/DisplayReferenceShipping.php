<?php

declare(strict_types=1);

namespace Cleever\App\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\EstimateAddressInterface;
use Magento\Quote\Api\Data\ShippingMethodInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\ShippingMethodManagement;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class DisplayReferenceShipping
{
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function afterGetEstimatedRates(
        ShippingMethodManagement $subject,
        array $result,
        Quote $quote,
        $country,
        $postcode,
        $regionId,
        $region,
        $address = null
    ): array {
        return $this->displayReferenceShipping($result);
    }

    /**
     * @param array $result
     *
     * @return array
     * @throws NoSuchEntityException
     */
    private function displayReferenceShipping(array $result): array
    {
        $methodReference = $this->getMethodReference();

        if ($this->getDisplayReference() || $methodReference === null) {
            return $result;
        }

        /** @var ShippingMethodInterface $method */
        foreach ($result as $key => $method) {
            /** @var string $methodCode */
            $methodCode = $method->getCarrierCode() . '_' . $method->getMethodCode();

            if (($methodCode !== $methodReference)) {
                continue;
            }
            unset($result[$key]);
        }

        return $result;
    }

    /**
     * @return string|null
     * @throws NoSuchEntityException
     */
    private function getMethodReference(): ?string
    {
        return $this->scopeConfig->getValue(
            'carriers/cleever/reference',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    private function getDisplayReference(): bool
    {
        return $this->scopeConfig->isSetFlag(
            'carriers/cleever/display_reference',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @param ShippingMethodManagement $subject
     * @param array $result
     * @param $cartId
     * @param EstimateAddressInterface $address
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function afterEstimateByAddress(
        ShippingMethodManagement $subject,
        array $result,
        $cartId,
        EstimateAddressInterface $address
    ): array {
        return $this->displayReferenceShipping($result);
    }

    /**
     * @param ShippingMethodManagement $subject
     * @param array $result
     * @param $cartId
     * @param $addressId
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function afterEstimateByAddressId(
        ShippingMethodManagement $subject,
        array $result,
        $cartId,
        $addressId
    ): array {
        return $this->displayReferenceShipping($result);
    }

    /**
     * @param ShippingMethodManagement $subject
     * @param array $result
     * @param $cartId
     * @param AddressInterface $address
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function afterEstimateByExtendedAddress(
        ShippingMethodManagement $subject,
        array $result,
        $cartId,
        AddressInterface $address
    ): array {
        return $this->displayReferenceShipping($result);
    }
}
