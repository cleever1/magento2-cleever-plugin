<?php

declare(strict_types=1);

namespace Cleever\App\Plugin;

use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Payment\Model\MethodInterface;
use Magento\Payment\Model\MethodList;
use Magento\Quote\Api\Data\CartInterface;
use Cleever\App\Model\ConfigProvider;

/**
 * Payment list
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PaymentList
{
    /**
     * Method carrier code cleever
     *
     * @var string METHOD_CARRIER_CODE
     */
    public const METHOD_CARRIER_CODE = 'cleever_cleever';
    /**
     * Method cleever payment
     *
     * @var string METHOD_MOONA_PAYMENT
     */
    public const METHOD_MOONA_PAYMENT = 'cleever_payment';
    /**
     * Method cleever discount
     *
     * @var string METHOD_MOONA_DISCOUNT
     */
    public const METHOD_MOONA_DISCOUNT = 'cleever';
    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * @var PhpCookieManager
     */
    protected PhpCookieManager $cookieManager;

    /**
     * @param ConfigProvider $configProvider
     * @param PhpCookieManager $cookieManager
     */
    public function __construct(
        ConfigProvider $configProvider,
        PhpCookieManager $cookieManager
    ) {
        $this->configProvider = $configProvider;
        $this->cookieManager = $cookieManager;
    }

    /**
     * After Get Available Methods
     *
     * @param MethodList         $subject
     * @param mixed[]            $result
     * @param CartInterface|null $quote
     *
     * @return mixed[]
     */
    public function afterGetAvailableMethods(MethodList $subject, array $result, CartInterface $quote = null): array
    {
        if ($quote === null
            || (($quote->getShippingAddress()->getShippingMethod() !== self::METHOD_CARRIER_CODE)
                && !$this->configProvider->isAbTestingEnable() && !$this->cookieManager->getCookie(ConfigProvider::COOKIE_NAME_SLOT_PRODUCT))
        ) {
            return $result;
        }

        if ($this->cookieManager->getCookie(ConfigProvider::COOKIE_NAME_ABTEST) === 'no') {
            return $result;
        }
        /** @var MethodInterface $method */
        foreach ($result as $key => $method) {
            $methodCode = $method->getCode();
            if ((($methodCode === self::METHOD_MOONA_PAYMENT) || ($methodCode === self::METHOD_MOONA_DISCOUNT))) {
                continue;
            }
            if ($this->cookieManager->getCookie(ConfigProvider::COOKIE_NAME_ABTEST) === 'yes' && str_contains($methodCode, 'hipay')) {
                unset($result[$key]);
            } elseif($quote->getShippingAddress()->getShippingMethod() === self::METHOD_CARRIER_CODE) {
                unset($result[$key]);
            }
        }

        return $result;
    }
}
