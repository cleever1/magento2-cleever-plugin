<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Cleever\App\Helper\DataCheckout;
use Cleever\App\Model\Config\Source\Order\Creation;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Manage get redirect
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Redirect extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    protected JsonFactory $jsonResultFactory;
    /**
     * Data checkout
     *
     * @var DataCheckout
     */
    protected DataCheckout $dataCheckoutHelper;
    /**
     * Validator
     *
     * @var Validator
     */
    protected Validator $formKeyValidator;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * @var Session
     */
    protected Session $checkoutSession;
    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;
    /**
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $cartRepository;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Validator $formKeyValidator
     * @param DataCheckout $dataCheckoutHelper
     * @param Debug $debug
     * @param Session $checkoutSession
     * @param ConfigProvider $configProvider
     * @param OrderRepositoryInterface $orderRepository
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Validator $formKeyValidator,
        DataCheckout $dataCheckoutHelper,
        Debug $debug,
        Session $checkoutSession,
        ConfigProvider $configProvider,
        OrderRepositoryInterface $orderRepository,
        CartRepositoryInterface $cartRepository
    ) {
        parent::__construct($context);

        $this->jsonResultFactory  = $jsonFactory;
        $this->dataCheckoutHelper = $dataCheckoutHelper;
        $this->formKeyValidator   = $formKeyValidator;
        $this->debug              = $debug;
        $this->checkoutSession    = $checkoutSession;
        $this->configProvider = $configProvider;
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
    }

    /**
     * Execute
     *
     * @return Json
     * @throws Exception
     */
    public function execute(): Json
    {
        $result = $this->jsonResultFactory->create();
        $errorMessage = "";

        if (!$this->_isAllowed($errorMessage) && !$this->getRequest()->getParam('method_code')) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));

            return $result->setData([
                false
            ]);
        }

        if ($this->configProvider->getCreationOrder() === Creation::CREATE_ORDER_CHECKOUT) {
            $order = $this->orderRepository->get($this->checkoutSession->getLastRealOrder()->getId());
            $quote = $this->cartRepository->get($order->getQuoteId());
        } else {
            $quote = $this->checkoutSession->getQuote();
        }

        /** @var string[] $data */
        $data = $this->dataCheckoutHelper->init(
            $this->getRequest()->getParam('method_code'),
            $quote,
            $this->getRequest()->getParam('mode'),
            $this->getRequest()->getParam('method_title')
        );
        if ($data['error']) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $data['response']));

            return $result->setData([
                false
            ]);
        }
        $result->setData(json_decode($data['response'], true));

        return $result;
    }

    /**
     * Is Allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var string $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in ajax.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request form key is invalid.";
        }
        if ($this->configProvider->getCreationOrder() === Creation::CREATE_ORDER_PAYMENT) {
            if (!$this->checkoutSession->getQuote()->getIsActive()) {
                /** @var bool $isAllowed */
                $isAllowed = false;
                $error     .= "Unable to get active cart.";
            }
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
