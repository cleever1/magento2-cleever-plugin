<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Cleever\App\Model\Debug;

/**
 * Quote reservation
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Reservation extends Action implements HttpPostActionInterface
{
    /**
     * Cart repository interface
     *
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $quoteRepository;
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    protected JsonFactory $jsonResultFactory;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Session
     *
     * @var Session
     */
    protected Session $session;
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;
    /**
     * @var CheckoutSession
     */
    protected CheckoutSession $checkoutSession;

    /**
     * Reservation constructor
     *
     * @param Context $context
     * @param CartRepositoryInterface $quoteRepository
     * @param JsonFactory $jsonFactory
     * @param Debug $debug
     * @param Session $session
     * @param CheckoutSession $checkoutSession
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $quoteRepository,
        JsonFactory $jsonFactory,
        Debug $debug,
        Session $session,
        CheckoutSession $checkoutSession,
        SerializerInterface $serializer
    ) {
        parent::__construct($context);

        $this->quoteRepository = $quoteRepository;
        $this->jsonResultFactory = $jsonFactory;
        $this->debug = $debug;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
        $this->serializer = $serializer;
    }

    /**
     * Execute
     *
     * @return Json|ResultInterface|void
     */
    public function execute()
    {
        $quote = $this->checkoutSession->getQuote();
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();

        $quote->collectTotals();

        if (!$quote->getGrandTotal()) {
            $this->debug->addDebugMessage(
                __(
                    'We can\'t process orders with a zero balance due. ' . 'To finish your purchase, please go through the standard checkout process.'
                )
            );

            return $result->setData([
                false,
            ]);
        }

        if (!$this->session->isLoggedIn()) {
            $quote->setCheckoutMethod(CartManagementInterface::METHOD_GUEST);
        }

        $quote->reserveOrderId();

        if ($this->checkoutSession->hasCleeverShippingApi() || $this->checkoutSession->hasCleeverSubscriptions() || $this->checkoutSession->hasDeviceId() || $this->checkoutSession->hasCleeverShippingCarrier()) {
            /** @var mixed[] $data */
            $data = [];

            if (!empty($this->checkoutSession->getCleeverShippingApi())) {
                $data['cleever_shipping_api'] = $this->checkoutSession->getCleeverShippingApi();
            }

            if (!empty($this->checkoutSession->getCleeverSubscriptions())) {
                $data['cleever_subscriptions'] = $this->checkoutSession->getCleeverSubscriptions();
            }

            if (!empty($this->checkoutSession->getDeviceId())) {
                $data['cleever_device_id'] = $this->checkoutSession->getDeviceId();
            }

            if (!empty($this->checkoutSession->getDeviceId())) {
                $data['cleever_shipping_carrier'] = $this->checkoutSession->getCleeverShippingCarrier();
            }

            $quote->setCleeverInformations($this->serializer->serialize($data));
        }

        $this->quoteRepository->save($quote);

        $this->checkoutSession->unsCleeverShippingApi();
        $this->checkoutSession->unsCleeverSubscriptions();
        $this->checkoutSession->unsCleeverEmail();
        $this->checkoutSession->unsCleeverShippingCarrier();

        return $result->setData([
            true,
        ]);
    }
}
