<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Cleever\App\Model\ConfigProvider;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Abtest extends Action implements HttpPostActionInterface
{
    /**
     * @var ConfigProvider
     */
    private ConfigProvider $configProvider;
    /**
     * @var JsonFactory
     */
    private JsonFactory $jsonResultFactory;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param PhpCookieManager $cookieManager
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        PhpCookieManager $cookieManager,
        ConfigProvider $configProvider
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->configProvider = $configProvider;
    }

    /**
     * @return ResultInterface
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     */
    public function execute(): ResultInterface
    {
        $result = $this->jsonResultFactory->create();
        $enable = $this->configProvider->createCookieAbTest();

        $result->setData([
            'enable' => $enable
        ]);

        return $result;
    }
}
