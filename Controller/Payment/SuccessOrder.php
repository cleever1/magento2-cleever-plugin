<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;

/**
 * Success order
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class SuccessOrder extends Action
{
    /**
     * Order factory
     *
     * @var OrderFactory
     */
    protected OrderFactory $orderFactory;
    /**
     * Checkout session
     *
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * PlaceOrder constructor
     *
     * @param Context      $context
     * @param OrderFactory $orderFactory
     * @param Session      $checkoutSession
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->orderFactory    = $orderFactory;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return void
     */
    public function execute(): void
    {
        /** @var string $orderId */
        $orderId = $this->getRequest()->getParam('orderId');
        /** @var string $cartId */
        $cartId = $this->getRequest()->getParam('cartId');

        $this->checkoutSession
            ->setLastQuoteId($cartId)
            ->setLastSuccessQuoteId($cartId);

        /** @var Order $order */
        $order = $this->orderFactory->create()->loadByIncrementId($orderId);

        if (!$order->getId()) {
            $this->_redirect('checkout/onepage/success');

            return;
        }

        $this->checkoutSession
            ->setLastOrderId($order->getId())
            ->setLastRealOrderId($order->getIncrementId())
            ->setLastOrderStatus($order->getStatus());

        $this->_redirect('checkout/onepage/success');
    }
}
