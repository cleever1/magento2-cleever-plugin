<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PaymentView implements HttpPostActionInterface
{
    protected JsonFactory $jsonResultFactory;
    protected ConfigProvider $config;
    protected StoreManagerInterface $storeManager;
    protected Api $api;
    protected RequestInterface $request;

    /**
     * @param RequestInterface $request
     * @param JsonFactory $jsonFactory
     * @param ConfigProvider $config
     * @param StoreManagerInterface $storeManager
     * @param Api $api
     */
    public function __construct(
        RequestInterface $request,
        JsonFactory $jsonFactory,
        ConfigProvider $config,
        StoreManagerInterface $storeManager,
        Api $api
    ) {
        $this->request = $request;
        $this->jsonResultFactory = $jsonFactory;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->api = $api;
    }

    /**
     * @return ResultInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(): ResultInterface
    {
        $result = $this->jsonResultFactory->create();

        $this->api->sendAmplitudeRequest([
            'event_type' => 'View - ' . $this->request->getParam('type'),
            'event_properties' => [
                'merchantID' => $this->config->getMerchantId(),
                'merchantSite' => $this->storeManager->getStore()->getName(),
                'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                'paymentMethodLabel' => $this->request->getParam('title'),
            ],
        ]);

        $result->setData([
            'status' => 'success',
        ]);

        return $result;
    }
}
