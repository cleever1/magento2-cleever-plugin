<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Traffic extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    private JsonFactory $jsonResultFactory;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    private ConfigProvider $config;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var Api
     */
    private Api $api;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param ConfigProvider $config
     * @param StoreManagerInterface $storeManager
     * @param Api $api
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ConfigProvider $config,
        StoreManagerInterface $storeManager,
        Api $api
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->api = $api;
    }

    /**
     * @return ResultInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute(): ResultInterface
    {
        $result = $this->jsonResultFactory->create();

        $this->api->sendAmplitudeRequest([
            'event_type' => 'Traffic',
            'event_properties' => [
                'merchantID' => $this->config->getMerchantId(),
                'merchantSite' => $this->storeManager->getStore()->getName(),
                'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                'cleeverDiscountPosition' => $this->config->getPosition(),
                'cleeverPaymentPosition' => $this->config->getPositionPayment(),
            ],
        ]);

        $result->setData([
            'status' => 'success',
        ]);

        return $result;
    }
}
