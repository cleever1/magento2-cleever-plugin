<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Phrase;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Config\Source\Order\Creation;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;
use Cleever\App\Model\Method\Cleever as MethodCleever;
use Cleever\App\Model\Method\CleeverPayment;
use Cleever\App\Provider\OrderProvider;

/**
 * Manage get notify
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Notify extends Action
{
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $config;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Transaction additional info
     *
     * @var string[];
     */
    protected array $transactionAdditionalInfo = [];
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;
    /**
     * Request interface
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;
    /**
     * File system
     *
     * @var File
     */
    protected File $filesystem;
    /**
     * Cart repository interface
     *
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $cartRepository;
    /**
     * Cart management interface
     *
     * @var CartManagementInterface
     */
    protected CartManagementInterface $quoteManagement;
    /**
     * Order provider
     *
     * @var OrderProvider
     */
    protected OrderProvider $orderProvider;
    /**
     * Order factory
     *
     * @var OrderFactory
     */
    protected OrderFactory $orderFactory;
    /**
     * Order management interface
     *
     * @var OrderManagementInterface
     */
    protected OrderManagementInterface $orderManagement;
    /**
     * Post values
     *
     * @var array
     */
    protected array $postValues = [];

    /**
     * Notify constructor
     *
     * @param Context                  $context
     * @param ConfigProvider           $config
     * @param Debug                    $debug
     * @param StoreManagerInterface    $storeManager
     * @param File                     $fileSystem
     * @param CartRepositoryInterface  $cartRepository
     * @param CartManagementInterface  $cartManagement
     * @param OrderFactory             $orderFactory
     * @param OrderProvider            $orderProvider
     * @param OrderManagementInterface $orderManagement
     */
    public function __construct(
        Context $context,
        ConfigProvider $config,
        Debug $debug,
        StoreManagerInterface $storeManager,
        File $fileSystem,
        CartRepositoryInterface $cartRepository,
        CartManagementInterface $cartManagement,
        OrderFactory $orderFactory,
        OrderProvider $orderProvider,
        OrderManagementInterface $orderManagement
    ) {
        parent::__construct($context);

        $this->config          = $config;
        $this->debug           = $debug;
        $this->storeManager    = $storeManager;
        $this->filesystem      = $fileSystem;
        $this->cartRepository  = $cartRepository;
        $this->quoteManagement = $cartManagement;
        $this->orderProvider   = $orderProvider;
        $this->orderFactory    = $orderFactory;
        $this->orderManagement = $orderManagement;
        $this->request         = $context->getRequest();
    }

    /**
     * Execute
     *
     * @return void
     */
    public function execute(): void
    {
        try {
            $this->notifyAction();
        } catch (Exception $e) {
            $this->debug->addDebugMessage($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @return bool
     * @throws LocalizedException
     * @throws Exception
     */
    protected function notifyAction(): bool
    {
        if (!$this->hasRequiredData()) {
            $this->debug->addDebugMessage(
                __('error on notifyAction() process with request body empty are not valid JSON')
            );

            return false;
        }

        switch ($this->getPostValue('type')) {
            case 'refund':
                return $this->refund();
            case 'order-paid':
                if ($this->config->getCreationOrder() === Creation::CREATE_ORDER_PAYMENT) {
                    return $this->paidOrder();
                }
                return true;
            case 'payment-failed':
                return $this->cancelOrder();
            default:
                return $this->validateOrder();
        }
    }

    /**
     * Cancerl order
     *
     * @return bool
     * @throws FileSystemException
     */
    protected function cancelOrder(): bool
    {
        if (!$this->isApiKeyAuthorize()) {
            return false;
        }

        try {
            /** @var OrderInterface $order */
            $order = $this->loadOrder();
        } catch (Exception $e) {
            $this->debug->addDebugMessage(
                __('Error on load order # %1, error : %2',
                    $this->getPostValue('order_id'),
                    $e->getMessage()
                )
            );

            return false;
        }

        if (!$order) {
            $this->debug->addDebugMessage(
                __('error on notifyAction() process : Order not exist # %1', $this->getPostValue('order_id'))
            );

            return false;
        }

        try {
            $this->orderManagement->cancel($order->getEntityId());
            $status = $this->config->getSystemConfigValue(
                'order_status_payment_canceled',
                $order->getStoreId(),
                MethodCleever::METHOD_CODE
            );
            $order->setStatus($status);
            $this->orderProvider->addNote($order, 'Payment is refused. Order is canceled');
        } catch (Exception $e) {
            $this->debug->addDebugMessage(
                __('Error on notifyAction() process : Order not cancel # %1 : error : %2',
                    $this->getPostValue('order_id'),
                    $e->getMessage()
                )
            );

            return false;
        }

        return true;
    }

    /**
     * Paid order
     *
     * @return bool
     * @throws FileSystemException
     */
    protected function paidOrder(): bool
    {
        if (!$this->isApiKeyAuthorize()) {
            return false;
        }

        /** @var OrderInterface $orderId */
        $order = $this->orderProvider->createOrder((int)$this->getPostValue('cart_id'));

        if (!$order->getId()) {
            $this->debug->addDebugMessage(
                __('error on notifyAction() process : Order not exist # %1',
                    $this->getPostValue('order_id')
                )
            );

            return false;
        }

        return true;
    }

    /**
     * Is Api Key Authorize function
     *
     * @return bool
     */
    protected function isApiKeyAuthorize(): bool
    {
        if (!$this->request->getServer('HTTP_X_API_KEY') || ($this->request->getServer(
                    'HTTP_X_API_KEY'
                ) !== $this->config->getStagingPublishableSecret() && $this->request->getServer(
                    'HTTP_X_API_KEY'
                ) !== $this->config->getLivePublishableSecret())) {
            $this->debug->addDebugMessage(
                __('error on ipn process with authorisation
               : Not authorized (' . $this->request->getServer('HTTP_X_API_KEY') . ')'
                )
            );

            return false;
        }
        return true;
    }

    /**
     * Has Required Data
     *
     * @return bool
     * @throws FileSystemException
     */
    protected function hasRequiredData(): bool
    {
        return !empty($this->getPostValue('order_id'));
    }

    /**
     * Get Post Value
     *
     * @param string $key
     * @param string $default
     *
     * @return mixed|string
     * @throws FileSystemException
     */
    protected function getPostValue(string $key, string $default = '')
    {
        if (!$this->postValues) {
            /** @var string|false $input */
            $input = $this->filesystem->fileGetContents('php://input');
            /** @var mixed $post */
            $post = json_decode($input, true);
            /** @var mixed $data */
            $data = $post['session'];

            $this->postValues = $data;
        }

        return $this->postValues[$key] ?? $default;
    }

    /**
     * Refund order
     *
     * @return bool
     * @throws FileSystemException
     */
    protected function refund(): bool
    {
        if (!$this->isApiKeyAuthorize()) {
            return false;
        }

        try {
            /** @var OrderInterface $order */
            $order = $this->loadOrder();
        } catch (Exception $e) {
            $this->debug->addDebugMessage(
                __('Error on load order # %1, error :  %2',
                    $this->getPostValue('order_id'),
                    $e->getMessage()
                )
            );

            return false;
        }

        if (!$order) {
            $this->debug->addDebugMessage(
                __('error on notifyAction() process : Order not exist # %1',
                    $this->getPostValue('order_id')
                )
            );

            return false;
        }

        try {
            /** @var mixed $refundAmount */
            $refundAmount = $this->getPostValue('amount') / 100;
            /** @var string $refundId */
            $refundId = $this->getPostValue('refund_id');
            $this->orderProvider->refundIpn($order, $refundAmount, $refundId);

            return true;
        } catch (Exception $e) {
            $this->debug->addDebugMessage(
                __('Refund IPN error order # %1, error : %2',
                    $this->getPostValue('order_id'),
                    $e->getMessage()
                )
            );

            return false;
        }
    }

    /**
     * Load order
     *
     * @return OrderInterface
     * @throws FileSystemException
     */
    protected function loadOrder(): OrderInterface
    {
        return $this->orderFactory->create()->loadByIncrementId($this->getPostValue('order_id'));
    }

    /**
     * Validate Order
     *
     * @return bool
     * @throws FileSystemException
     * @throws LocalizedException
     */
    protected function validateOrder(): bool
    {
        if (!$this->isApiKeyAuthorize()) {
            return false;
        }

        try {
            /** @var OrderInterface $order */
            $order = $this->loadOrder();
        } catch (Exception $e) {
            /** @var OrderInterface $orderId */
            $order = $this->orderProvider->createOrder((int)$this->getPostValue('cart_id'));
        }

        if (!$order->getId()) {
            /** @var OrderInterface $orderId */
            $order = $this->orderProvider->createOrder((int)$this->getPostValue('cart_id'));
        }

        if (!$order) {
            $this->debug->addDebugMessage(
                __('error on notifyAction() process : Order not exist # %1', $this->getPostValue('order_id'))
            );

            return false;
        }

        try {
            /** @var float $orderAmount */
            $orderAmount = round(number_format((float)$order->getGrandTotal(), 2, '.', '') * 100);
            /** @var float $totalAmount */
            $totalAmount = (float)$this->getPostValue('discount_amount') + (float)$this->getPostValue('amount');
            /** @var string $method */
            $method = $order->getPayment()->getMethod();

            if ($orderAmount !== $totalAmount) {
                $this->orderProvider->addNote(
                    $order,
                    sprintf(
                        (string)__(
                            'Fraud : Amounts don\'t match (order amount %1$s != payment amount %2$s)',
                            'discount-payment-cleever'
                        ),
                        number_format((float)$order->getGrandTotal(), 2),
                        number_format($totalAmount / 100, 2)
                    )
                );

                return false;
            }
        } catch (Exception $e) {
            $this->debug->addDebugMessage(
                __('error on check amounts order # %1, error : %2',
                    $this->getPostValue('order_id'),
                    $e->getMessage()
                )
            );

            return false;
        }

        /** @var bool $createInvoice */
        $createInvoice = (bool)$this->config->getSystemConfigValue(
            'create_invoice',
            $order->getStoreId(),
            MethodCleever::METHOD_CODE
        );
        /** @var string $transactionId */
        $transactionId = $this->getPostValue('transaction_id');

        if ($this->isOrderProcessing($order)) {
            $status = Order::STATE_PROCESSING;
        } elseif ($this->isOrderComplete($order)) {
            $status = Order::STATE_COMPLETE;
        } elseif (($status = $this->config->getSystemConfigValue(
                'order_status_payment_accepted',
                $order->getStoreId(),
                MethodCleever::METHOD_CODE
            )) === null) {
            $status = $order->getState();
        }

        if ($createInvoice) {
            $this->orderProvider->saveInvoice($order, $transactionId);
        }
        if ($this->isOrderComplete($order)) {
            $this->orderProvider->saveShipment($order);
        }
        $this->transactionAdditionalInfo['_cleever_id']      = $this->getPostValue('ecommerce_session_id');
        $this->transactionAdditionalInfo['_cleever_user_id'] = $this->getPostValue('cleever_user_id');
        $this->transactionAdditionalInfo['transaction_id'] = $transactionId;
        $order->getPayment()->setAdditionalInformation('cleever_discount_amount', $this->getPostValue('discount_amount'));
        $this->orderProvider->addTransaction($order, $this->transactionAdditionalInfo);

        $this->orderProvider->updateOrder($order, $this->getSuccessfulPaymentMessage($order, $method), $status);

        return true;
    }

    /**
     * Is Order Processing
     *
     * @param OrderInterface $order
     *
     * @return bool
     */
    public function isOrderProcessing(OrderInterface $order): bool
    {
        return $order->getState() === Order::STATE_PROCESSING;
    }

    /**
     * Is Order Complete
     *
     * @param OrderInterface $order
     *
     * @return bool
     */
    public function isOrderComplete(OrderInterface $order): bool
    {
        return $order->getState() === Order::STATE_COMPLETE;
    }

    /**
     * Get successfull payment message
     *
     * @param OrderInterface $order
     * @param string         $method
     *
     * @return string
     * @throws FileSystemException
     */
    protected function getSuccessfulPaymentMessage(OrderInterface $order, string $method): string
    {
        if ($method === CleeverPayment::METHOD_CODE) {
            /** @var Phrase $msg */
            $msg = __('IPN ok | validated by Cleever payment');
        } else {
            /** @var Phrase $msg */
            $msg = __('IPN ok | validated by Cleever discount');
        }

        $msg .= '<br />' . sprintf(
                (string)__(
                    'API : %1$s | PLUGIN : %2$s | transaction : %3$s | customer : %4$s | cleever id : %5$s'
                ),
                $this->getPostValue('api_version'),
                $this->getPostValue('plugin_version'),
                $this->getPostValue('transaction_id'),
                $this->getPostValue('customer'),
                $this->getPostValue('cleever_user_id')
            );
        $msg .= '<br />' . sprintf(
                (string)__('Transfer group : %s'),
                $this->getPostValue('transfer_group')
            );
        $this->transactionAdditionalInfo['_transfer_group'] = $this->getPostValue('transfer_group');

        if ($method === CleeverPayment::METHOD_CODE) {
            if (strncmp($this->getPostValue('transfer_group'), "group_", 6) !== 0) {
                $msg .= '<br />' . sprintf(
                        (string)__('Discount applied : %s'),
                        (float)($this->getPostValue('discount_amount') / 100)
                    );
                $this->transactionAdditionalInfo['_has_cleever_discount']    = true;
                $this->transactionAdditionalInfo['_cleever_discount_amount'] = (float)$this->getPostValue(
                    'discount_amount'
                );
            } else {
                $this->transactionAdditionalInfo['_has_cleever_discount']    = false;
                $this->transactionAdditionalInfo['_cleever_discount_amount'] = 0;
            }
        } else {
            $this->transactionAdditionalInfo['_has_cleever_discount']    = true;
            $this->transactionAdditionalInfo['_cleever_discount_amount'] = (float)$this->getPostValue('discount_amount');
            $msg .= '<br />' . sprintf(
                    (string)__('Discount applied : %s'),
                    (float)($this->getPostValue('discount_amount') / 100)
                );
        }

        $msg .= '<br />' . sprintf(
                (string)__('Payment completed : %1$s (amount => %2$s)'),
                $this->getPostValue('transaction_id'),
                (float)($this->getPostValue('amount') / 100) . ' ' . $order->getOrderCurrency()->getCurrencySymbol()
            );

        return $msg;
    }
}
