<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Locale\Resolver;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;
use Cleever\App\Model\Method\Cleever;

/**
 * Manage get checkout
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Checkout extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    protected JsonFactory $jsonResultFactory;
    /**
     * Api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Validator
     *
     * @var Validator
     */
    protected Validator $formKeyValidator;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Locale resolver
     *
     * @var Resolver
     */
    protected Resolver $locale;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Validator $formKeyValidator
     * @param Api $api
     * @param Debug $debug
     * @param Resolver $locale
     * @param ConfigProvider $configProvider
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Validator $formKeyValidator,
        Api $api,
        Debug $debug,
        Resolver $locale,
        ConfigProvider $configProvider,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->api               = $api;
        $this->formKeyValidator  = $formKeyValidator;
        $this->debug             = $debug;
        $this->locale            = $locale;
        $this->configProvider    = $configProvider;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return Json
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();
        /** @var string $errorMessage */
        $errorMessage = "";

        if (!$this->_isAllowed($errorMessage)) {
            $this->debug->addDebugMessage('An error has occured. ' . $errorMessage);
            $response['error'] = true;
        }

        /** @var string $price */
        $price = $this->getRequest()->getParam('price');
        /** @var string $lang */
        $lang = $this->locale->getLocale();
        /** @var string $method */
        $method = $this->getRequest()->getParam('method');

        /** @var string[] $data */
        $data = $this->api->getCheckoutBanner([
            'price' => $price,
            'lang'  => $lang,
        ], $method);

        /** @var mixed $response */
        $response = json_decode($data['response'], true);

        if ($data['error']) {
            $this->debug->addDebugMessage('An error has occured. ' . $data['response']);
            $response['error'] = true;
        }
        $this->configProvider->setMethodCode(Cleever::METHOD_CODE);
        $response['banners']['enabled'] = $this->configProvider->isBannerAvailable('banner_above_cta_checkout_page');
        $response['icon']               = $response['icon'] ?? false;
        $result->setData($response);

        return $result;
    }

    /**
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null)
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in ajax.";
        }
        if (!$this->checkoutSession->getQuote()->getIsActive()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Unable to get active cart.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request form key is invalid.";
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
