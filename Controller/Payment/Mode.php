<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Cleever\App\Model\Api;
use Cleever\App\Model\Debug;

/**
 * Manage get mode
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Mode extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    protected JsonFactory $jsonResultFactory;
    /**
     * Api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Validator
     *
     * @var Validator
     */
    protected Validator $formKeyValidator;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * Mode constructor
     *
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Validator $formKeyValidator
     * @param Api $api
     * @param Debug $debug
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Validator $formKeyValidator,
        Api $api,
        Debug $debug,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->api               = $api;
        $this->formKeyValidator  = $formKeyValidator;
        $this->debug             = $debug;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return Json
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();
        /** @var string $errorMessage */
        $errorMessage = "";
        if (!$this->_isAllowed($errorMessage)) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));
        }

        /** @var string[] $data */
        $data = $this->api->getDisplayMode();
        if ($data['error']) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $data['response']));
        }
        /** @var mixed $response */
        $response = json_decode($data['response'], true);
        $result->setData($response);

        return $result;
    }

    /**
     * Is allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in ajax.";
        }
        if (!$this->checkoutSession->getQuote()->getIsActive()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Unable to get active cart.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request form key is invalid.";
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
