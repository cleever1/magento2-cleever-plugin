<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Config\Source\Order\Creation;
use Cleever\App\Model\Config\Source\Redirect as ConfigRedirect;
use Cleever\App\Model\ConfigProvider;

/**
 * Manage get back
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Back extends Action
{
    protected ConfigProvider $config;
    protected StoreManagerInterface $storeManager;
    protected Session $checkoutSession;
    protected ScopeConfigInterface $scopeConfig;

    /**
     * _construct function
     *
     * @return void
     */
    protected function _construct()
    {
        $this->config->setMethodCode('cleever');
    }

    public function __construct(
        Context $context,
        ConfigProvider $configProvider,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);

        $this->config = $configProvider;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return Redirect
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws FailureToSendException
     */
    public function execute(): Redirect
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        /** @var string|null $typeRedirect */
        $typeRedirect = $this->scopeConfig->getValue(
            'payment/cleever/canceled_redirect_cart',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        if ((int)$this->getRequest()->getParam('slpcbx') === 0) {
            $this->config->deleteCookieSlotProduct();
        }

        $typeCreation = $this->config->getCreationOrder();

        if ($typeCreation === Creation::CREATE_ORDER_CHECKOUT) {
            $this->fillCart();
        }

        if ($typeRedirect === ConfigRedirect::TO_CHECKOUT) {
            return $resultRedirect->setPath('checkout/', ['_current' => true]);
        }

        return $resultRedirect->setPath('checkout/cart/', ['_current' => true]);
    }

    /**
     * Fill Cart
     *
     * @return void
     */
    protected function fillCart(): void
    {
        $this->checkoutSession->restoreQuote();
    }
}
