<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Model\Order\Payment as ModelPayment;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Manage get change payment
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class ChangePayment extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    private JsonFactory $jsonResultFactory;
    /**
     * Api
     *
     * @var Api
     */
    private Api $api;
    /**
     * Validator
     *
     * @var Validator
     */
    private Validator $formKeyValidator;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    private ConfigProvider $config;
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * Debug
     *
     * @var Debug
     */
    private Debug $debug;
    /**
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * ChangePayment constructor
     *
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param ConfigProvider $config
     * @param Validator $formKeyValidator
     * @param PriceCurrencyInterface $priceCurrency
     * @param Api $api
     * @param StoreManagerInterface $storeManager
     * @param Debug $debug
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ConfigProvider $config,
        Validator $formKeyValidator,
        PriceCurrencyInterface $priceCurrency,
        Api $api,
        StoreManagerInterface $storeManager,
        Debug $debug,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->api               = $api;
        $this->config            = $config;
        $this->formKeyValidator  = $formKeyValidator;
        $this->priceCurrency     = $priceCurrency;
        $this->storeManager      = $storeManager;
        $this->debug             = $debug;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return Json
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();
        /** @var string $errorMessage */
        $errorMessage = "";

        if (!$this->_isAllowed($errorMessage)) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));

            return $result->setData([
                'status' => 'error',
            ]);
        }

        $quote = $this->checkoutSession->getQuote();
        /** @var string $countryId */
        $countryId = $quote->getShippingAddress() !== null ? $quote->getShippingAddress()->getCountryId() : 'GB';
        if (!$this->config->getSpecificCountry()) {
            /** @var string[] $specificCountries */
            $specificCountries = [];
        } else {
            /** @var string[] $specificCountries */
            $specificCountries = explode(',', $this->config->getSpecificCountry());
        }
        /** @var string $method */
        $method = $this->getRequest()->getParam('method');
        /** @var string|null $additionalData */
        $additionalData = $this->getRequest()->getParam('additional_data');

        if ($this->config->isAllowSpecific() && !in_array($countryId, $specificCountries)) {
            return $result->setData([
                'status' => 'error',
            ]);
        }

        if ($method) {
            /** @var  ModelPayment $payment */
            $payment = $quote->getPayment();
            $payment->setMethod($method);

            // Manage the payment method ID
            /** @var string $paymentMethod */
            $paymentMethod = $payment->getMethod();
            if ($paymentMethod === 'cleever') {
                /** @var string $paymentMethod */
                $paymentMethod = 'cleever-discount';
            } elseif ($paymentMethod === 'cleever_payment') {
                /** @var string $paymentMethod */
                $paymentMethod = 'cleever-payment';
            }

            $this->api->sendAmplitudeRequest([
                'event_type'       => 'Click - Merchant checkout - Change payment method',
                'event_properties' => [
                    'merchantID'            => $this->config->getMerchantId(),
                    'merchantSite'          => $this->storeManager->getStore()->getName(),
                    'merchantURL'           => $this->storeManager->getStore()->getBaseUrl(),
                    'cleeverDiscountPosition' => $this->config->getPosition(),
                    'cleeverPaymentPosition'  => $this->config->getPositionPayment(),
                    'currency'              => $this->priceCurrency->getCurrency()->getCurrencyCode(),
                    'currencySymbol'        => $this->priceCurrency->getCurrencySymbol(),
                    'orderTotalValue'       => number_format((float)$quote->getGrandTotal(), 2, '.', ','),
                    'paymentMethodID'       => $paymentMethod,
                    'paymentMethodLabel'    => $additionalData['front_title'] ?? $payment->getMethodInstance()->getTitle(),
                ],
            ]);
        }

        $result->setData([
            'status' => 'success',
        ]);

        return $result;
    }

    /**
     * Is allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in ajax.";
        }
        if (!$this->checkoutSession->getQuote()->getIsActive()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Unable to get active cart.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request form key is invalid.";
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
