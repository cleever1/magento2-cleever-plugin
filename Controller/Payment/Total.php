<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderRepository;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Manage get total
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Total extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    private JsonFactory $jsonResultFactory;
    /**
     * Api
     *
     * @var Api
     */
    private Api $api;
    /**
     * Validator
     *
     * @var Validator
     */
    private Validator $formKeyValidator;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;
    /**
     * Cart
     *
     * @var Cart
     */
    private Cart $cart;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    private ConfigProvider $config;
    /**
     * Debug
     *
     * @var Debug
     */
    private Debug $debug;
    /**
     * Locale resolver
     *
     * @var Resolver
     */
    private Resolver $locale;
    /**
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param ConfigProvider $config
     * @param Validator $formKeyValidator
     * @param PriceCurrencyInterface $priceCurrency
     * @param Api $api
     * @param Debug $debug
     * @param Resolver $locale
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ConfigProvider $config,
        Validator $formKeyValidator,
        PriceCurrencyInterface $priceCurrency,
        Api $api,
        Debug $debug,
        Resolver $locale,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->api               = $api;
        $this->config            = $config;
        $this->formKeyValidator  = $formKeyValidator;
        $this->priceCurrency     = $priceCurrency;
        $this->debug             = $debug;
        $this->locale            = $locale;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return Json
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();
        /** @var string $errorMessage */
        $errorMessage = "";

        if (!$this->_isAllowed($errorMessage)) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));

            return $result->setData([
                'status' => 'error',
            ]);
        }

        $quote = $this->checkoutSession->getQuote();
        /** @var string $countryId */
        $countryId = $quote->getShippingAddress() !== null ? $quote->getShippingAddress()->getCountryId() : 'GB';
        if (!$this->config->getSpecificCountry()) {
            /** @var string[] $specificCountries */
            $specificCountries = [];
        } else {
            /** @var string[] $specificCountries */
            $specificCountries = explode(',', $this->config->getSpecificCountry());
        }

        /** @var string $lang */
        $lang = $this->locale->getLocale();
        /** @var string $method */
        $method = $this->getRequest()->getParam('method');
        /** @var float $grandTotal */
        $grandTotal = $this->getRequest()->getParam('grand_total');
        /** @var float $mpChecked */
        $mpChecked = $this->getRequest()->getParam('mp_checked');

        if ($this->config->isAllowSpecific() && !in_array(
            $countryId,
            $specificCountries
        )) {
            $this->debug->addDebugMessage('Method is not allowed for this country');

            return $result->setData([
                'status' => 'error',
            ]);
        }

        /** @var string[] $arrayData */
        $arrayData = [
            'price'           => $grandTotal * 100,
            'lang'            => $lang,
            'currency_symbol' => $this->priceCurrency->getCurrencySymbol(),
            'currency_code'   => $this->priceCurrency->getCurrency()->getCurrencyCode(),
            'source'          => 'checkout',
            'mp_checked'      => $mpChecked
        ];

        if ($method) {
            $arrayData['payment_method'] = $method;
        }

        /** @var string[] $data */
        $data = $this->api->getTotal($arrayData);

        if (empty($data)) {
            $this->debug->addDebugMessage('Method is not enable.');

            return $result->setData([
                'status' => 'error',
            ]);
        }

        if ($data['error']) {
            $this->debug->addDebugMessage('An error has occured. ' . $data['response']);

            return $result->setData([
                'status' => 'error',
            ]);
        }

        /** @var mixed $response */
        $response           = json_decode($data['response'], true);
        $response['status'] = 'success';
        $result->setData($response);

        return $result;
    }

    /**
     * Is allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request is not in ajax.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Request form key is invalid.";
        }
        if (!$this->checkoutSession->getQuote()->getIsActive()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error     .= "Unable to get active cart.";
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
