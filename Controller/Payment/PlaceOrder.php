<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Click place order
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class PlaceOrder extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    private JsonFactory $jsonResultFactory;
    /**
     * Api
     *
     * @var Api
     */
    private Api $api;
    /**
     * Validator
     *
     * @var Validator
     */
    private Validator $formKeyValidator;
    /**
     * Price currency interface
     *
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    private ConfigProvider $config;
    /**
     * Store manager interface
     *
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * Debug
     *
     * @var Debug
     */
    private Debug $debug;
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;
    /**
     * @var Session
     */
    protected Session $checkoutSession;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param ConfigProvider $config
     * @param Validator $formKeyValidator
     * @param PriceCurrencyInterface $priceCurrency
     * @param Api $api
     * @param StoreManagerInterface $storeManager
     * @param Debug $debug
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ConfigProvider $config,
        Validator $formKeyValidator,
        PriceCurrencyInterface $priceCurrency,
        Api $api,
        StoreManagerInterface $storeManager,
        Debug $debug,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->api = $api;
        $this->config = $config;
        $this->formKeyValidator = $formKeyValidator;
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
        $this->debug = $debug;
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Execute
     *
     * @return Json
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->jsonResultFactory->create();
        /** @var string $errorMessage */
        $errorMessage = "";

        if (!$this->_isAllowed($errorMessage)) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));

            return $result->setData([
                'status' => 'error',
            ]);
        }

        $quote = $this->checkoutSession->getQuote();
        /** @var string $method */
        $paymentMethod = $this->getRequest()->getParam('paymentMethod');
        $paymentTitle = $this->scopeConfig->getValue(
            'payment/' . $paymentMethod . '/title',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );

        if ($paymentMethod === 'cleever') {
            $paymentMethod = 'cleever-discount';
        } elseif ($paymentMethod === 'cleever_payment') {
            $paymentMethod = 'cleever-payment';
        }

        $this->api->sendAmplitudeRequest([
            'event_type' => 'Click - Merchant checkout',
            'event_properties' => [
                'merchantID' => $this->config->getMerchantId(),
                'merchantSite' => $this->storeManager->getStore()->getName(),
                'merchantURL' => $this->storeManager->getStore()->getBaseUrl(),
                'cleeverDiscountPosition' => $this->config->getPosition(),
                'cleeverPaymentPosition' => $this->config->getPositionPayment(),
                'currency' => $this->priceCurrency->getCurrency()->getCurrencyCode(),
                'currencySymbol' => $this->priceCurrency->getCurrencySymbol(),
                'orderTotalValue' => number_format((float)$quote->getGrandTotal(), 2, '.', ','),
                'paymentMethodID' => $paymentMethod,
                'paymentMethodLabel' => $paymentTitle,
            ],
        ]);

        $result->setData([
            'status' => 'success',
        ]);

        return $result;
    }

    /**
     * Is allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request is not in ajax.";
        }
        if (!$this->checkoutSession->getQuote()->getIsActive()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Unable to get active cart.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request form key is invalid.";
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }
}
