<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Payment;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\LayoutFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Cleever\App\Block\IframeForm;
use Cleever\App\Helper\DataCheckout;
use Cleever\App\Model\Config\Source\Order\Creation;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Manage get iframe
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Iframe extends Action implements HttpPostActionInterface
{
    /**
     * Json factory
     *
     * @var JsonFactory
     */
    protected JsonFactory $jsonResultFactory;
    /**
     * Data checkout
     *
     * @var DataCheckout
     */
    protected DataCheckout $dataCheckoutHelper;
    /**
     * Validator
     *
     * @var Validator
     */
    protected Validator $formKeyValidator;
    /**
     * Layout factory
     *
     * @var LayoutFactory
     */
    protected LayoutFactory $layoutFactory;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * @var Session
     */
    protected Session $checkoutSession;
    /**
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;
    /**
     * @var CartRepositoryInterface
     */
    protected CartRepositoryInterface $cartRepository;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param LayoutFactory $layoutFactory
     * @param Session $checkoutSession
     * @param Validator $formKeyValidator
     * @param DataCheckout $dataCheckoutHelper
     * @param Debug $debug
     * @param ConfigProvider $configProvider
     * @param OrderRepositoryInterface $orderRepository
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        LayoutFactory $layoutFactory,
        Session $checkoutSession,
        Validator $formKeyValidator,
        DataCheckout $dataCheckoutHelper,
        Debug $debug,
        ConfigProvider $configProvider,
        OrderRepositoryInterface $orderRepository,
        CartRepositoryInterface $cartRepository
    ) {
        parent::__construct($context);

        $this->jsonResultFactory = $jsonFactory;
        $this->layoutFactory = $layoutFactory;
        $this->dataCheckoutHelper = $dataCheckoutHelper;
        $this->formKeyValidator = $formKeyValidator;
        $this->debug = $debug;
        $this->checkoutSession = $checkoutSession;
        $this->configProvider = $configProvider;
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
    }

    /**
     * Execute
     *
     * @return Json
     * @throws LocalizedException
     * @throws InputException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(): Json
    {
        $result = $this->jsonResultFactory->create();
        $errorMessage = "";

        if ($this->configProvider->getCreationOrder() === Creation::CREATE_ORDER_CHECKOUT) {
            $order = $this->orderRepository->get($this->checkoutSession->getLastRealOrder()->getId());
            $quote = $this->cartRepository->get($order->getQuoteId());
        } else {
            $quote = $this->checkoutSession->getQuote();
        }

        if (!$this->_isAllowed($errorMessage) && !$this->getRequest()->getParam('method_code')) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $errorMessage));

            return $result->setData([
                false,
            ]);
        }

        /** @var string[] $payload */
        $payload = $this->getRequest()->getParam('payload', []);
        /** @var string[] $data */
        $data = $this->dataCheckoutHelper->init(
            $this->getRequest()->getParam('method_code'),
            $quote,
            $this->getRequest()->getParam('mode', 'iframe'),
            $this->getRequest()->getParam('method_title'),
            $payload
        );

        if ($data['error']) {
            $this->debug->addDebugMessage(__('An error has occured. ' . $data['response']));

            return $result->setData([
                'error' => true,
            ]);
        }

        /** @var string[] $response */
        $response = json_decode($data['response'], true);

        if (!empty($payload)) {
            /** @var string $display */
            $display = 'B';
        } else {
            /** @var string $display */
            $display = $response['display'] ?? 'A';
        }

        $result->setData([
            'html' => $this->getIframeHtml($response['url'], $display),
            'display' => $display,
        ]);

        return $result;
    }

    /**
     * Is allowed
     *
     * @param null $errorMessage
     *
     * @return bool
     */
    private function _isAllowed(&$errorMessage = null): bool
    {
        /** @var bool $isAllowed */
        $isAllowed = true;
        /** @var string $error */
        $error = "";

        if (!$this->getRequest()->isPost()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request is not in post mode.";
        }
        if (!$this->getRequest()->isAjax()) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request is not in ajax.";
        }
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            /** @var bool $isAllowed */
            $isAllowed = false;
            $error .= "Request form key is invalid.";
        }
        if ($this->configProvider->getCreationOrder() === Creation::CREATE_ORDER_PAYMENT) {
            if (!$this->checkoutSession->getQuote()->getIsActive()) {
                /** @var bool $isAllowed */
                $isAllowed = false;
                $error .= "Unable to get active cart.";
            }
        }
        if (isset($errorMessage)) {
            /** @var string $errorMessage */
            $errorMessage = $error;
        }

        return $isAllowed;
    }

    /**
     * Get iframe Html
     *
     * @param string $url
     * @param string $display
     *
     * @return string
     */
    private function getIframeHtml(string $url, string $display): string
    {
        $block = $this->layoutFactory->create()->createBlock(IframeForm::class, 'iframe_form', [
            'data' => [
                'url_iframe' => $url,
                'display' => $display,
            ],
        ]);

        return $block->toHtml();
    }
}
