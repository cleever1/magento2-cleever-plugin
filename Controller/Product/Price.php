<?php

declare(strict_types=1);

namespace Cleever\App\Controller\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\ViewInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class Price implements HttpPostActionInterface
{
    protected Configurable $configurable;
    protected ProductRepositoryInterface $productRepository;
    protected JsonFactory $resultJsonFactory;
    protected RequestInterface $request;
    protected Registry $registry;
    protected ViewInterface $view;

    public function __construct(
        Configurable $configurable,
        ProductRepositoryInterface $productRepository,
        JsonFactory $resultJsonFactory,
        RequestInterface $request,
        ViewInterface $view
    ) {
        $this->configurable = $configurable;
        $this->productRepository = $productRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->request = $request;
        $this->view = $view;
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $this->view->loadLayout();

        $product = $this->productRepository->getById($this->request->getParam('product'));

        if ($this->request->getParam('cleever_discount')) {
            if ($this->request->getParam('super_attribute')) {
                $attributesInfo = [];
                foreach ($this->request->getParam('super_attribute') as $key => $value) {
                    $attributesInfo[$key] = $value;
                }

                $configurable = $this->configurable->getProductByAttributes($attributesInfo, $product);
                if ($configurable) {
                    $product = $configurable;
                    $product->setMinimalPrice($product->getFinalPrice());
                }
            }

            if ($this->request->getParam('cleever_slot_product') !== null) {
                $oldPrice = $product->getFinalPrice();
                $newPrice = $oldPrice - ((float)$this->request->getParam('cleever_discount')/100);
            } else {
                $newPrice = $product->getFinalPrice();
            }

            $product->setSpecialPrice($newPrice);
        }

        $priceHtml = $this->view->getLayout()
            ->getBlock('cleever.price')
            ->setProduct($product);

        $result->setData([
            'html' => $priceHtml->toHtml(),
        ]);

        return $result;
    }

    /**
     * @param ProductInterface $product
     *
     * @return void
     */
    protected function registerProduct(ProductInterface $product): void
    {
        $this->registry->unregister('product');
        $this->registry->register('product', $product);
        $this->registry->unregister('current_product');
        $this->registry->register('current_product', $product);
    }
}
