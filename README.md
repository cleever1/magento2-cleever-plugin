Cleever payment solution unlocks £5 discounts for your customers on each order, and at no cost to you!

# Description

Cleever allows any online e-commerce to monetise their website for free, while offering funded incentives to your cusotmers.

This free plugin provides 2 main features:

**Funded Incentives** - Our incentives, valued up to £15, are tailored to encourage repeat visits to your website. By offering these attractive rewards, you'll turn one-time buyers into loyal, returning customers, boosting your long-term engagement and growth.

**New Revenue Stream** - Cleever will pay you to show and offer the incentives to your customers, not only will the customer pay less, you will also increase your margin per order.

Cleever can be installed in a few clicks. No coding required. in less than 5 minutes.

# What will Cleever Do for you?
1. **Boost Margin:** For any order where an incentive get's displayed, Cleever will increase your margin by paying you a net commission
2. **Increase Conversion:** Thanks to the offers and discounts more customers will check out, and the disocunts won't eat into your margin.
3. **Increase Repeat Business:** Our offers are designed to drive traffic back to your website, by offering discounts that Cleever pays for.
4. **Bigger Carts:** Getting a discount on their current or next order means customers are will add more to their cart, increasing thier basket value.
5. **Easy integration:** Getting started with Cleever is easy for all Magento2 stores.

# Why will your shoppers love Cleever?
1. **More Discounts:** Any shopper that takes a discount, will gain access to one of our services offering daily discounts and offers.
2. **More Items:** With Cleever's discounts your customers will buy more items, as they select the discounts we fund for them.


**How do I get Started?**
It only takes 5 minutes, to increase boost your margin and sales.
Follow these Steps:

**Sign-up to Cleever**
1. Go to [https://partners.cleever.com/register](https://partners.cleever.com/register)
2. Enter your details, and follow the steps

# Installation with composer
## **Extension Installation**

1. Go to your magento repository and to install the module, the following command should be used:
```shell
composer config repositories.cleever-plugin vcs https://gitlab.com/cleever1/magento2-cleever-plugin.git
// Will be the last tag version => stable
composer require cleever/cleever
```

2. To activate the extension, the commands below should be executed:
```shell
php bin/magento setup:upgrade
php bin/magento cache:flush
```

3. If Magento is in **Production** mode, the following commands should also be run:
```shell
php bin/magento setup:di:compile 
php bin/magento setup:static-content:deploy (your locale)
```

## **Extension Update**

### After an installation manually

The best way is to use composer. To do it, you have to delete the previous manual installation first. Then install the module with composer (go to the section "Extension installation" just above).
```shell
rm -rf app/code/App
```

### After an installation with composer

The extension update process is the same as the module installation process. The following command will run the update for a specified extension:
```shell
composer update cleever/cleever
```

After the extension update, the same commands as after the extension installation should be run:
```shell
php bin/magento setup:upgrade
php bin/magento cache:flush
php bin/magento setup:di:compile 
php bin/magento setup:static-content:deploy (your locale)
```

### **Extension Deleting**

The command below will delete the specified extension:
```shell
composer remove cleever/cleever
```

## 2.2.7 (16/01/24)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
New Features:
- Enable management of discount codes to support post-purchase funded discount offer.
- Enable hook communication to ensure transfer of funds to merchants.

Improvements: 
- Remove access to legacy paymenmt option.
- Remove pre-purchase slot on product page.


## 2.2.6 (14/03/23)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Improve discount display on product page
- Improve discount display on cart page
- Improve discount display on checkout page

## 2.2.5 (01/03/23)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Display new price with discount on product page
- Display new price with discount in mini-cart
- Display discount in email confirmation
- Display more information about shipping method in order page in Magento back-office
- Handle different discount amount
- Align back-office labels with new brand name
- Add more warning and error messages in back-office to notify users about configuration issues

## 2.2.4 (18/10/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Display all payment methods when we tick the checkbox on product page
- Handle new shipping method (WebShopApps Matrix Rate)
- Handle specific case to display shipping methods with empty title

## 2.2.3 (03/10/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Display discount on total order when there is only one payment solution displayed at checkout and the discount checkbox in the integrated form is checked

## 2.2.2 (02/10/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Add product slot > checkbox

# Release Notes
## 2.2.1 (02/09/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Add csp_whitelist to authorize Amplify and new domain name
- Keep the cart with "back button" on "create order at checkout" mode
- Handle store views for abtest
- Update URL card-form

## 2.2.0 (01/08/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Orders can be created in Magento BO only if payment is successful to generate only meaningful orders and speed up checkout.
- New shipping slot to propose free shipping subscription to shoppers.
- Add currency information in the request.
- Allow to AB test the payment solution.
- Display discount on order confirmation email.
- Display discount at checkout on MP - Integrated form - Checkbox.
- Handle different configuration for each website.

## 2.1.1 (24/02/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Improve cookie management

## 2.1.0 (23/02/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Cleever payment can be enabled alone
- Update function name
- Optimize call to the WS
- Remove useless call on WS banners at checkout
- Improve Amplitude implementation (init with JS, more details, pm label)
- Send Cleever Payment status and position on save settings
- Send right current locale for the lang
- Optimize way to call WS and save merchant id
- Handle refund with other currencies
- Update displaying of decimals on order notes
- Save merchant id on send plugin status
- MD : Send right value to display the discount when currency is different from the basic one
- Remove useless images and codes
- Update default text and icon
- Banners are disable by default
- No more default banners
- Improve scope handling

## 2.0.2 (24/01/22)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Handle Porto theme automatically
- Add information on card-form URL to identify the merchant website
- Add redirection mode on m-payment
- Fix back button issue with Cleever payment
- Optimise HTTP errors to avoid service interruption
- Synchronize m-discount and m-payment functions (empty cart,...)

## 2.0.1 (23/11/21)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Handle scope by store (no more default one)
- Redirect to success page if order is processing or complete on cancel default page
- Update warning text for refund system

## 2.0.0 (08/11/21)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- New gateway to manage payments without discount
- Enable total and partial refunds from back-office

## 1.1.0 (09/08/21)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Display discount information at checkout
- Allow to display Cleever payment gateway in an iframe

## 1.0.0 (03/05/21)
Compatible with Open Source (CE) : 2.1 2.2 2.3 2.4
Compatible with Commerce on prem (EE) : 2.1 2.2 2.3 2.4
Stability: Stable Build
Description:
- Add Cleever payment gateway at checkout
- Display Cleever banners
