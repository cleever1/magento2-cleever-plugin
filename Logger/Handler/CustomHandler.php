<?php

declare(strict_types=1);

namespace Cleever\App\Logger\Handler;

use Monolog\Logger;
use Magento\Framework\Logger\Handler\Base;

/**
 * Custom handler log
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class CustomHandler extends Base
{
    /**
     * Filename
     *
     * @var string
     */
    protected $fileName = '/var/log/cleever.log';

    /**
     * Logger type
     *
     * @var int
     */
    protected $loggerType = Logger::DEBUG;
}
