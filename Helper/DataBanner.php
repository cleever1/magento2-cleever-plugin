<?php

declare(strict_types=1);

namespace Cleever\App\Helper;

use Magento\Framework\Locale\Resolver;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Method\Cleever;

/**
 * Manage display banners
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class DataBanner
{
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * Banners
     *
     * @var string[]
     */
    protected array $banners;

    /**
     * @param ConfigProvider        $configProvider
     * @param Resolver              $locale
     * @param Api                   $api
     */
    public function __construct(
        ConfigProvider $configProvider,
        Resolver $locale,
        Api $api
    ) {
        $this->configProvider = $configProvider;
        $this->configProvider->setMethodCode(Cleever::METHOD_CODE);
        $this->banners = $api->getBanners([
            'lang' => $locale->getLocale()
        ]);
    }

    /**
     * Is Banner Available
     *
     * @param string $section
     *
     * @return bool
     */
    public function isBannerAvailable(string $section): bool
    {
        return $this->configProvider->isBannerAvailable($section);
    }

    /**
     * Get Html Banner
     *
     * @param string $section
     *
     * @return string
     */
    public function getHtmlBanner(string $section): string
    {
        return $this->banners[$section] ?? '';
    }
}
