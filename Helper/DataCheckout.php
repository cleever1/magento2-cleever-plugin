<?php

declare(strict_types=1);

namespace Cleever\App\Helper;

use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Model\Quote;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * Manage display banners
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class DataCheckout
{
    /**
     * Front title
     *
     * @var string FRONT_TITLE
     */
    private const FRONT_TITLE = 'front_title';
    /**
     * Customer session
     *
     * @var Session
     */
    protected Session $customerSession;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $configProvider;
    /**
     * Method code
     *
     * @var null
     */
    protected $methodCode = null;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Api
     *
     * @var Api
     */
    protected Api $api;
    /**
     * Locale resolver
     *
     * @var Resolver
     */
    protected Resolver $locale;
    /**
     * Checkout session
     *
     * @var Session
     */
    protected Session $session;
    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;

    /**
     * DataCheckout constructor
     *
     * @param Session $customerSession
     * @param ConfigProvider $configProvider
     * @param Api $api
     * @param Debug $debug
     * @param Resolver $locale
     * @param Session $session
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Session $customerSession,
        ConfigProvider $configProvider,
        Api $api,
        Debug $debug,
        Resolver $locale,
        Session $session,
        SerializerInterface $serializer
    ) {
        $this->customerSession = $customerSession;
        $this->configProvider = $configProvider;
        $this->debug = $debug;
        $this->api = $api;
        $this->locale = $locale;
        $this->session = $session;
        $this->serializer = $serializer;
    }

    /**
     * Init checkout
     *
     * @param string $methodCode
     * @param Quote $quote
     * @param string $mode
     * @param string $methodTitle
     * @param mixed[] $payload
     *
     * @return mixed[]
     * @throws LocalizedException
     */
    public function init(string $methodCode, Quote $quote, string $mode, string $methodTitle, array $payload = []): array
    {
        $this->reset();
        $this->configProvider->setStoreId($quote->getStoreId());
        $this->setMethod($methodCode);

        /** @var string[] $data */
        $data = $this->api->getRedirectUrl(
            $this->fillData($quote, $mode, $payload)
        );

        if ($data['error']) {
            $this->debug->addDebugMessage('error on init() process : ' . $data['response']);
        } else {
            /** @var mixed $response */
            $response = json_decode($data['response'], true);
            /** @var string $urlToRedirect */
            $urlToRedirect = $response['url'];
            /** @var string $cleeverSessionId */
            $cleeverSessionId = explode('sessionid=', $urlToRedirect)[1];

            $payment = $quote->getPayment();
            $payment->setAdditionalInformation('cleever_session_id', $cleeverSessionId);
            $payment->setAdditionalInformation(self::FRONT_TITLE, $methodTitle);
            $payment->save();
        }

        return $data;
    }

    /**
     * Reset
     *
     * @return $this
     */
    public function reset(): DataCheckout
    {
        $this->configProvider->setStoreId(null);
        $this->setMethod(null);

        return $this;
    }

    /**
     * Set method
     *
     * @param string|null $methodCode
     *
     * @return $this
     */
    public function setMethod($methodCode): DataCheckout
    {
        /** @var string|null methodCode */
        $this->methodCode = $methodCode;
        $this->configProvider->setMethodCode($methodCode);

        return $this;
    }

    /**
     * Fill data
     *
     * @param Quote $quote
     * @param string $mode
     * @param mixed[] $payload
     *
     * @return mixed[]
     * @throws NoSuchEntityException
     */
    private function fillData(Quote $quote, string $mode, array $payload = []): array
    {
        /** @var string[] $config */
        $config = $this->configProvider->getConfig();
        /** @var string[] $config */
        $config = $config['cleever_payment'];
        /** @var AddressInterface|null $address */
        $address = $quote->getBillingAddress();
        /** @var array[] $cleeverInformation */
        $cleeverInformation = [];

        if (!empty($quote->getCleeverInformations())) {
            $cleeverInformation = $this->serializer->unserialize($quote->getCleeverInformations());
        }

        /** @var mixed[] $data */
        $data = [
            "plugin_version" => $config['plugin_version'],
            "api_version" => $config['api_version'],
            "amount" => ($quote->getGrandTotal() * 100),
            "currency" => $quote->getQuoteCurrencyCode(),
            "email" => $address->getEmail(),
            "firstname" => $address->getFirstname(),
            "lastname" => $address->getLastname(),
            "phone" => $address->getTelephone(),
            "address" => $this->getStreetFormatted($address->getStreet()),
            "postcode" => $address->getPostcode(),
            "city" => $address->getCity(),
            "country" => $address->getCountryId(),
            "return_url" => $config['return_url'] . '?orderId=' . $quote->getReservedOrderId() . '&cartId=' . $quote->getId(),
            "cancel_url" => $config['cancel_url'],
            "notification_url" => $config['notification_url'],
            "cart_id" => $quote->getId(),
            "order_id" => $quote->getReservedOrderId(),
            'device_id' => $this->api->getDeviceId(),
            'mode' => $mode,
            'lang' => $this->locale->getLocale(),
            'creation_order' => $config['creation_order']
        ];

        if (isset($cleeverInformation['cleever_subscriptions']) && !empty($cleeverInformation['cleever_subscriptions'])) {
            $payload['user_infos'] = $cleeverInformation['cleever_subscriptions'];
        }

        if ($quote->getShippingAddress()->getShippingMethod() === 'cleever_cleever') {
            $payload['slot_shipping'] = $this->session->getCleeverShipping();
        }

        if (isset($payload['payment_method'])) {
            $data['payment_method'] = $payload['payment_method'];
        }

        if ($this->configProvider->getSlotProductActive()) {
            $payload['slot_product_checkbox_checked'] = $this->configProvider->getSlotProductChecked();
        }

        if (!empty($payload)) {
            $data['payload'] = json_encode($payload);
        }

        return $data;
    }

    /**
     * @param string[]|string $street
     *
     * @return string[]|string
     */
    private function getStreetFormatted($street)
    {
        if (is_array($street)) {
            $streetFormatted = implode(' ', $street);
        } else {
            $streetFormatted = $street;
        }

        return $streetFormatted;
    }
}
