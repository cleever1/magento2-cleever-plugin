/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */

var config = {
    map: {
        '*': {
            cleeverCheckoutSuccess: 'Cleever_App/js/view/checkout/success',
            cleeverProduct: 'Cleever_App/js/view/product',
            cleeverCheckoutCart: 'Cleever_App/js/view/checkout/cart',
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Cleever_App/js/model/checkout-data-resolver': true
            }
        }
    }
};
