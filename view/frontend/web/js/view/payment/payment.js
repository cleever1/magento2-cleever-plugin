/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'cleever',
                component: 'Cleever_App/js/view/payment/method-renderer/cleever'
            },
            {
                type: 'cleever_payment',
                component: 'Cleever_App/js/view/payment/method-renderer/cleever-payment'
            }
        );
        return Component.extend({});
    }
);
