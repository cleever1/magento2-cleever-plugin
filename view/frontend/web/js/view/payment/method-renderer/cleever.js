/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor',
        'Cleever_App/js/action/redirect',
        'Cleever_App/js/action/iframe',
        'Magento_Checkout/js/model/payment/additional-validators'
    ],
    function (
        ko,
        $,
        Component,
        urlBuilder,
        fullScreenLoader,
        errorProcessor,
        redirectAction,
        iframeAction,
        additionalValidators
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cleever_App/payment/cleever'
            },
            isVisible: ko.observable(true),
            paymentMethod: {
                title: '<span style="color: grey;">CARD: £5 OFF</span>',
                icon: window.require.toUrl('Cleever_App/image/checkout-secure-card-logos.svg'),
                description: '<span style="color: grey;">Your £5 welcome discount will be applied on the payment page.<span>',
                banners : {
                    enabled : 0,
                    banner_above_cta_checkout_page: ''
                }
            },

            initialize: function () {
                this._super();
                this.initPaymentMethodInfos();
                this.redirectAfterPlaceOrder = false;
                var serviceUrl = urlBuilder.build('cleever/payment/paymentview'),
                    params = {
                        isAjax: true,
                        form_key: $('input[name="form_key"]').val(),
                        type: 'MP2',
                        title: this.getPaymentMethodTitle()
                    };
                $.ajax(
                    {url: serviceUrl, data: params, type: 'POST'}
                ).done(function (data) {});
                return this;
            },

            /**
             * @returns {*}
             */
            creationOrder: function() {
                return window.checkoutConfig.cleever_payment.creation_order;
            },

            /**
             * @override
             */
            placeOrder: function (data, event) {
                var self = this;
                if (event) {
                    event.preventDefault();
                }

                if (this.validate() && additionalValidators.validate()
                    && this.isPlaceOrderActionAllowed() === true) {

                    if (this.creationOrder() === 'payment') {
                        this.isPlaceOrderActionAllowed(false);
                        var serviceUrl = urlBuilder.build('cleever/payment/reservation'),
                            params = {
                                isAjax: true,
                                form_key: $('input[name="form_key"]').val()
                            };

                        fullScreenLoader.startLoader();

                        $.ajax(
                            {url: serviceUrl, data: params, type: 'POST'}
                        ).done(function (data) {
                            if (!data) {
                                fullScreenLoader.stopLoader(true);
                                errorProcessor.process(data);
                                return false;
                            } else {
                                self.afterPlaceOrder();
                            }
                        }).always(function (data) {
                            self.isPlaceOrderActionAllowed(true);
                        });
                    } else {
                        this._super();
                        this.isPlaceOrderActionAllowed(false);
                    }
                } else {
                    this.isPlaceOrderActionAllowed(true);
                    return false;
                }
            },

            /**
             * @override
             */
            afterPlaceOrder: function () {
                var self = this;
                var serviceUrl = urlBuilder.build('cleever/payment/mode'),
                    params = {
                        isAjax: true,
                        form_key: $('input[name="form_key"]').val()
                    };

                $.ajax(
                    {url: serviceUrl, data: params, type: 'POST'}
                ).done(function (data) {
                    if(data.mode === 'iframe') {
                        self.bindEventIframe();
                        $.when(iframeAction(self.getCode(),data.display, self.getPaymentMethodTitle()));
                    } else {
                        $.when(redirectAction(self.getCode(), 'redirection', self.getPaymentMethodTitle()));
                    }
                }).fail(function (response) {
                    errorProcessor.process(response);
                });
            },

            bindEventIframe: function () {
                window.addEventListener('message', handleEvent, false);
                function handleEvent(e) {
                    if(
                        e.data.source !== undefined &&
                        e.data.action !== undefined &&
                        e.data.source === 'App'
                    ) {
                        var action = e.data.action;

                        if(action === 'cancel') {
                            if (window.checkoutConfig.cleever_payment.cancel_redirect_type === 'to_checkout' &&
                                window.checkoutConfig.cleever_payment.creation_order === 'payment' ) {
                                $('.cleever-popin-container').remove();
                            } else {
                                window.location = window.checkoutConfig.cleever_payment.cancel_url;
                            }
                        } else if(action === 'go-to-cart') {
                            window.location = window.checkoutConfig.cartUrl;
                        } else if(action === 'go-to-checkout') {
                            window.location = window.checkoutConfig.checkoutUrl;
                        } else if(action === 'paid') {
                            //window.location = window.checkoutConfig.cleever_payment.return_url;
                        } else if(action === 'finish-cleever-process') {
                            window.location = e.data.url !== undefined ? e.data.url : window.checkoutConfig.cleever_payment.return_url;
                        } else if(action === 'open-new-tab') {
                            window.open(e.data.url,'_blank');
                        } else if(action === 'redirect') {
                            window.location = e.data.url;
                        }
                    }
                }
            },

            initObservable: function () {
                this._super()
                    .observe('paymentMethod');
                return this;
            },

            getPaymentMethodIcon: function() {
                return this.paymentMethod()['icon'];
            },

            getPaymentMethodDescription: function() {
                return this.paymentMethod()['description'];
            },

            getPaymentMethodTitle: function() {
                return this.paymentMethod()['title'];
            },

            getPaymentMethodBottom: function() {
                return this.paymentMethod()['banners']['enabled'] ? this.paymentMethod()['banners']['banner_above_cta_checkout_page'] : ''
            },

            initPaymentMethodInfos: function() {
                var self = this;
                var serviceUrl = urlBuilder.build('cleever/payment/checkout'),
                    params = {
                        price: window.checkoutConfig.quoteData.grand_total,
                        isAjax: true,
                        method: 'cleever',
                        form_key: $('input[name="form_key"]').val()
                    };

                fullScreenLoader.startLoader();

                $.ajax(
                    {url: serviceUrl, data: params, type: 'POST'}
                ).done(function (data) {
                    fullScreenLoader.stopLoader();
                    if (data.error) {
                        self.isVisible(false);
                    }
                    self.paymentMethod(data);
                }).fail(function (response) {
                    errorProcessor.process(response);
                });
            },

            getData: function () {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'front_title': this.getPaymentMethodTitle()
                    }
                };
            },
        });
    }
);
