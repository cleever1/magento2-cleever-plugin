/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Cleever_App/js/action/iframe',
        'Cleever_App/js/action/redirect',
        'Magento_Checkout/js/checkout-data',
        'Magento_Customer/js/customer-data',
        'mage/cookies',
    ],
    function (
        ko,
        $,
        Component,
        urlBuilder,
        fullScreenLoader,
        errorProcessor,
        additionalValidators,
        iframeAction,
        redirectAction,
        checkoutData,
        customerData
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cleever_App/payment/cleever-payment'
            },
            isVisible: ko.observable(true),
            paymentMethod: {
                title: '<span style="color: grey;">Credit/Debit Card</span>',
                icon: window.require.toUrl('Cleever_App/image/checkout-secure-card-logos.svg'),
                description: ''
            },

            initialize: function () {
                this._super();
                this.initPaymentMethodInfos();
                if(this.getMode() === 'integrated') {
                    this.bindEventIframePayment();
                }
                this.redirectAfterPlaceOrder = false;
                var serviceUrl = urlBuilder.build('cleever/payment/paymentview'),
                    params = {
                        isAjax: true,
                        form_key: $('input[name="form_key"]').val(),
                        type: 'MP1',
                        title: this.getPaymentMethodTitle()
                    };
                $.ajax(
                    {url: serviceUrl, data: params, type: 'POST'}
                ).done(function (data) {});
                return this;
            },

            /**
             * Get mode
             * @returns string
             */
            getMode: function () {
                return window.checkoutConfig.cleever_payment.mode;
            },

            /**
             * @override
             */
            afterPlaceOrder: function (payload) {
                var self = this;
                if(this.getMode() === 'integrated') {
                    self.bindEventIframeSuccess();
                    $.when(iframeAction(self.getCode(),'B', self.getPaymentMethodTitle(), payload));
                } else {
                    $.when(redirectAction(self.getCode(), 'main-payment-redirection', self.getPaymentMethodTitle()));
                }
            },

            /**
             * @returns {*}
             */
            creationOrder: function() {
                return window.checkoutConfig.cleever_payment.creation_order;
            },

            /**
             * @override
             */
            placeOrder: function (data, event) {
                var self = this;
                fullScreenLoader.startLoader();
                if (this.getMode() === 'integrated') {
                    if (event) {
                        event.preventDefault();
                    }
                    const message = { action: 'cta:click', source: 'Moona' };
                    let cleeverIframe = document.querySelector('#cleever-form-integrated').contentWindow;
                    cleeverIframe.postMessage(message, '*');

                    fullScreenLoader.stopLoader(true);
                    return false;
                } else {
                    if (this.validate() && additionalValidators.validate()
                        && this.isPlaceOrderActionAllowed() === true) {

                        if (this.creationOrder() === 'payment') {
                            this.isPlaceOrderActionAllowed(false);
                            var serviceUrl = urlBuilder.build('cleever/payment/reservation'),
                                params = {
                                    isAjax: true,
                                    form_key: $('input[name="form_key"]').val()
                                };
                            $.ajax(
                                {url: serviceUrl, data: params, type: 'POST'}
                            ).done(function (data) {
                                if (!data) {
                                    errorProcessor.process(data);
                                    return false;
                                } else {
                                    self.afterPlaceOrder();
                                }
                            }).always(function (data) {
                                self.isPlaceOrderActionAllowed(true);
                            });
                        } else {
                            this._super();
                            this.isPlaceOrderActionAllowed(false);
                        }
                    } else {
                        this.isPlaceOrderActionAllowed(true);
                        return false;
                    }
                    fullScreenLoader.stopLoader(true);
                }
            },

            initObservable: function () {
                this._super()
                    .observe('paymentMethod')
                return this;
            },

            getPaymentMethodIcon: function() {
                return this.paymentMethod()['icon'];
            },

            getPaymentMethodDescription: function() {
                return this.paymentMethod()['description'];
            },

            getPaymentMethodTitle: function() {
                return this.paymentMethod()['title'];
            },

            getIframeUrl: function() {
              return window.checkoutConfig.cleever_payment.iframe_url + this.getSubscription();
            },

            getSubscription: function() {
                var cart = customerData.get('cart');
                var url = '&cp=' + (cart().subtotalAmount * 100);
                $.each(window.checkoutConfig.cleever_subscriptions, function( index, value ) {
                    url += '&' + index + '=' + value;
                });
                if (window.checkoutConfig.cleever_shipping_active) {
                    if (checkoutData.getSelectedShippingRate() === 'cleever_cleever') {
                        url += '&slfs=1';
                    } else {
                        url += '&slfs=0';
                    }
                }
                if (window.checkoutConfig.cleever_payment.slot_product) {
                    const check_cookie_slot_product = $.mage.cookies.get('cleever_slot_product');
                    if (check_cookie_slot_product) {
                        url += '&slpcbx=1&pda=' + check_cookie_slot_product;
                    } else {
                        url += '&slpcbx=0';
                    }
                }

               return url;
            },

            initPaymentMethodInfos: function() {
                var self = this;
                var serviceUrl = urlBuilder.build('cleever/payment/checkout'),
                    params = {
                        price: window.checkoutConfig.quoteData.grand_total,
                        isAjax: true,
                        method: 'cleever_payment',
                        form_key: $('input[name="form_key"]').val()
                    };

                fullScreenLoader.startLoader();

                $.ajax(
                    {url: serviceUrl, data: params, type: 'POST'}
                ).done(function (data) {
                    if (data.error) {
                        self.isVisible(false);
                    }
                    self.paymentMethod(data);
                }).fail(function (response) {
                    errorProcessor.process(response);
                });
                fullScreenLoader.stopLoader();
            },

            bindEventIframePayment: function () {
                var self = this;

                window.addEventListener('message', handleEvent, false);
                function handleEvent(e) {
                    if(
                        e.data.source !== undefined &&
                        e.data.action !== undefined &&
                        e.data.source === 'Moona'
                    ) {
                        var action = e.data.action;

                        if (action === 'form-validation:valid') {

                            if (self.validate() && additionalValidators.validate()) {
                                self.isPlaceOrderActionAllowed(false);

                                if (self.creationOrder() === 'payment') {
                                    var serviceUrl = urlBuilder.build('cleever/payment/reservation'),
                                        params = {
                                            isAjax: true,
                                            form_key: $('input[name="form_key"]').val()
                                        };
                                    $.ajax(
                                        {url: serviceUrl, data: params, type: 'POST'}
                                    ).done(function (data) {
                                        if (!data) {
                                            fullScreenLoader.stopLoader(true);
                                            errorProcessor.process(data);
                                            return false;
                                        } else {
                                            self.afterPlaceOrder(e.data.payload);
                                        }
                                    }).always(function (data) {
                                        self.isPlaceOrderActionAllowed(true);
                                    });
                                } else {
                                    self.getPlaceOrderDeferredObject()
                                    .fail(
                                        function () {
                                            self.isPlaceOrderActionAllowed(true);
                                        }
                                    ).done(
                                        function () {
                                            self.afterPlaceOrder(e.data.payload);
                                        }
                                    );
                                }

                                return true;
                            }
                        } else if(action === 'form-validation:errors') {
                            self.isPlaceOrderActionAllowed(true);
                        } else if (action === 'form:resize') {
                            $('#cleever-integrated-container').css('height', e.data.payload.height);
                            $('#cleever-integrated-container > div').css('height', e.data.payload.height);
                        }
                    }

                }
            },

            bindEventIframeSuccess: function () {
                window.addEventListener('message', handleEvent, false);
                function handleEvent(e) {
                    if(
                        e.data.source !== undefined &&
                        e.data.action !== undefined &&
                        e.data.source === 'Moona'
                    ) {
                        var action = e.data.action;

                        if(action === 'cancel') {
                            if (window.checkoutConfig.cleever_payment.cancel_redirect_type === 'to_checkout' &&
                                window.checkoutConfig.cleever_payment.creation_order === 'payment' ) {
                                $('.cleever-popin-container').remove();
                            } else {
                                window.location = window.checkoutConfig.cleever_payment.cancel_url;
                            }
                        } else if(action === 'go-to-cart') {
                            window.location = window.checkoutConfig.cartUrl;
                        } else if(action === 'go-to-checkout') {
                            window.location = window.checkoutConfig.checkoutUrl;
                        } else if(action === 'paid') {

                        } else if(action === 'finish-moona-process') {
                            window.location = e.data.url !== undefined ? e.data.url : window.checkoutConfig.cleever_payment.return_url;
                        } else if(action === 'open-new-tab') {
                            window.open(e.data.url,'_blank');
                        } else if(action === 'redirect') {
                            window.location = e.data.url;
                        }
                    }
                }
            },

            getData: function () {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'front_title': this.getPaymentMethodTitle()
                    }
                };
            }
        });
    }
);
