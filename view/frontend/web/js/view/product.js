/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */

define([
    'jquery',
    'mage/url',
    'Magento_Catalog/js/price-utils',
    'mage/cookies',
], function ($, urlBuilder, priceUtils) {
    'use strict';
    return function (options, element) {
        $(document).on('change','#cleever-slot-product', $.proxy(function (e) {
            if ($(this).is(":checked")) {
                $('#cleever-product-description').show();
                const serviceUrl = urlBuilder.build('cleever/product/checkSlot'),
                    params = {
                        isAjax: true,
                        form_key: $('input[name="form_key"]').val(),
                        title: $('.cleever-product label').html(),
                        description: $('#cleever-product-description').html(),
                        type: 'Click - Product page - Checkbox',
                        discount: $('#cleever_discount').val()
                    };
                $.ajax({
                    url: serviceUrl,
                    data: params,
                    type: 'POST',
                    async: true
                });
                if ($(this).attr('reloadPrice') === '1') {
                    changePrice($(this));
                }
            } else {
                $('#cleever-product-description').hide();
                $.mage.cookies.clear('cleever_slot_product');
                if ($(this).attr('reloadPrice') === '1') {
                    changePrice($(this));
                }
            }
        }, this));

        $(document).on('click', '.swatch-option', $.proxy(function (e) {
            if ($('#cleever-slot-product').is(":checked")) {
                changePrice($('#cleever-slot-product'));
            } else {
                changePrice($('#cleever-slot-product'));
            }
        }, this));

        $(document).ready(function() {
            const check_cookie_slot_product = $.mage.cookies.get('cleever_slot_product');
            if (!check_cookie_slot_product) {
                $('.cleever-product').show();
            }
            const serviceUrl = urlBuilder.build('cleever/product/checkSlot'),
                params = {
                    isAjax: true,
                    form_key: $('input[name="form_key"]').val(),
                    title: $('.cleever-product label').html(),
                    description: $('#cleever-product-description').html(),
                    type: 'View - Product page',
                    discount: $('#cleever_discount').val()
                };
            $.ajax({
                url: serviceUrl,
                data: params,
                type: 'POST',
                async: true
            });
        });
    };

    function changePrice(element) {
        const serviceUrl = urlBuilder.build('cleever/product/price');
        $.ajax({
            url: serviceUrl,
            data: $('#product_addtocart_form').serialize(),
            type: 'POST',
            showLoader: true,
            async: true
        }).done(function (data) {
            if (data !== '') {
                let htmlObject;
                if (element.attr('isBundle') === '1') {
                    htmlObject = $('.price-configured_price').html(data.html);
                } else {
                    htmlObject = $('.product-info-price').html(data.html);
                }
                htmlObject.trigger('contentUpdated');
                if (element.is(':checked')) {
                    $('[data-price-type="oldPrice"]').css('text-decoration', 'line-through');
                }
            }
        });
    }

});
