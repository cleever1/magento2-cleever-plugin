define(
    [
        'jquery',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Customer/js/customer-data',
        'ko',
        'mage/url',
        'mage/cookies',
    ],
    function (
        $,
        Component,
        _,
        quote,
        priceUtils,
        customerData,
        ko,
        urlBuilder
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cleever_App/checkout/minicart/cleever_total'
            },
            cleeverMiniTotal: ko.observable([
                {
                    display_discount: false,
                    discount_amount: ''
                }
            ]),
            display_discount: null,
            initialize: function () {
                const self = this;
                self._super();

                $(document).ajaxComplete(function() {
                    let data = self.cleeverMiniTotal();
                    if ($.mage.cookies.get('cleever_slot_product')) {
                        data['display_discount'] = true;
                        data['discount_amount'] = $.mage.cookies.get('cleever_slot_product');
                    }
                    self.getCleeverMiniTotal(data);
                });

                $(document).on('ajax:addToCart', $.proxy(function () {
                    let data = self.cleeverMiniTotal();
                    if ($.mage.cookies.get('cleever_slot_product')) {
                        data['display_discount'] = true;
                        data['discount_amount'] = $.mage.cookies.get('cleever_slot_product');
                    }

                    self.getCleeverMiniTotal(data);
                }, this));

                // Subscribe only if App is active
                if (window.checkoutConfig.cleever_payment) {
                    if (window.checkoutConfig.cleever_payment.active) {
                        quote.paymentMethod.subscribe(
                            function(payment) {
                                let data = self.cleeverMiniTotal();
                                data['display_discount'] = false;
                                if (payment.method === 'cleever') {
                                    const serviceUrl = urlBuilder.build('cleever/payment/total'),
                                        params = {
                                            grand_total: quote.totals().grand_total,
                                            isAjax: true,
                                            form_key: $('input[name="form_key"]').val(),
                                            method: payment,
                                            mp_checked: false,
                                        };

                                    self.request = $.ajax(
                                        {
                                            url: serviceUrl,
                                            data: params,
                                            type: 'POST',
                                            async: false
                                        }
                                    ).done(function (response) {
                                        if (response.status === 'success') {
                                            data['display_discount'] = true
                                            data['discount_amount'] = response.discount_amount;
                                        }
                                    }).fail(function (response) {
                                    });
                                } else {
                                    $('.block-minicart .subtotal .price').css('text-decoration','none');
                                    $('[data-block="minicart"]').find('.cart-price').css('text-decoration','none');
                                    $('.cleaver-minicart-discount').remove();
                                }
                                self.getCleeverMiniTotal(data);
                            },
                            null,
                            'change'
                        );
                    }
                }

                this.bindEventIframePayment();

                return self;
            },
            getCleeverMiniTotal(datas) {
                this.cleeverMiniTotal(datas);
            },
            getCookieDiscount: function () {
                return this.cleeverMiniTotal().display_discount;
            },
            getTextWithDiscount: function () {
                if (this.getCookieDiscount()) {
                    this.lineThroughMiniTotal();
                }
                return _('New Total');
            },
            getTotalWithDiscount: function () {
                if (this.getCookieDiscount()) {
                    this.lineThroughMiniTotal();
                    $('[data-block="minicart"]').find('.cart-price').css('text-decoration','line-through');
                    if ($('.cleaver-minicart-discount').length > 0) {
                        $('.cleaver-minicart-discount').remove();
                    }
                    $('[data-block="minicart"]').find('.cart-price').after('<span class="cleaver-minicart-discount">' + this.getTextCookieDiscount() + '</span>');
                } else {
                    if ($('.cleaver-minicart-discount').length > 0) {
                        $('.block-minicart .subtotal .price').css('text-decoration','none');
                        $('.cleaver-minicart-discount').remove();
                    }
                }
                return this.getTextCookieDiscount();
            },
            getTextCookieDiscount: function() {
                var cart = customerData.get('cart');
                return this.getFormattedPrice(cart().subtotalAmount - (this.cleeverMiniTotal().discount_amount / 100));
            },
            lineThroughMiniTotal: function () {
                $('.block-minicart .subtotal .price').css('text-decoration','line-through');
            },
            /**
             * Format shipping price.
             * @returns {String}
             */
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },
            bindEventIframePayment: function () {
                const self = this;

                window.addEventListener('message', handleEvent, false);
                function handleEvent(e) {
                    if(
                        e.data.source !== undefined &&
                        e.data.action !== undefined &&
                        e.data.source === 'App'
                    ) {
                        const action = e.data.action;
                        if(action === 'form-cb:state') {
                            let data = self.cleeverMiniTotal();
                            if (e.data.payload.is_checked === true) {
                                data['display_discount'] = true;
                                data['discount_amount'] = e.data.payload.discount_amount;
                            } else {
                                data['display_discount'] = false;
                                $('.block-minicart .subtotal .price').css('text-decoration','none');
                                $('[data-block="minicart"]').find('.cart-price').css('text-decoration','none');
                                $('.cleaver-minicart-discount').remove();
                            }
                            self.getCleeverMiniTotal(data);
                        }
                    }
                }
            },
        });
    }
);
