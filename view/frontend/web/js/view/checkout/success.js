/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */

define([
    'jquery',
    'Magento_Customer/js/customer-data',
    'mage/cookies',
], function ($, customerData) {
    'use strict';

    return function (options, element) {
        $(document).ready(function() {
            $.mage.cookies.clear('cleever_slot_product');
            const clearData = {
                'selectedShippingAddress': null,
                'shippingAddressFromData': null,
                'newCustomerShippingAddress': null,
                'selectedShippingRate': null,
                'selectedPaymentMethod': null,
                'selectedBillingAddress': null,
                'billingAddressFromData': null,
                'newCustomerBillingAddress': null,
            };

            customerData.set('checkout-data', clearData);
            customerData.set('cart', {});
        });
    };
});
