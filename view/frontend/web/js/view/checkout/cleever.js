define(
    [
        'uiComponent',
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/checkout-data'
    ],
    function (
        Component,
        $,
        urlBuilder,
        quote,
        shippingRateRegistry,
        checkoutData
    ) {
        'use strict';


        return Component.extend({
            initialize: function () {
                var self = this;

                $(document).on('change', '#customer-email', function() {
                    if (self.validateEmail() || ($('#customer-email').val() === '')) {
                        self.getSubscription($('#customer-email').val());
                    }
                });
                $(document).on('ajax:updateCartItemQty', $.proxy(function () {
                    self.reloadShippinglist();
                }, this));
                $(document).on('ajax:removeFromCart', $.proxy(function () {
                    self.reloadShippinglist();
                }, this));
                $(document).ready(function() {
                    if (checkoutData.getValidatedEmailValue() !== '') {
                        self.getSubscription(checkoutData.getValidatedEmailValue());
                    }
                });
                if (window.checkoutConfig.cleever_payment.active || window.checkoutConfig.cleever_payment.mp_active) {
                    quote.shippingMethod.subscribe(function(method) {
                        const serviceUrl = urlBuilder.build('cleever/payment/changeShipping'),
                            params = {
                                isAjax: true,
                                form_key: $('input[name="form_key"]').val(),
                                method: method
                            };
                        $.ajax({
                            url: serviceUrl,
                            data: params,
                            type: 'POST',
                            async: true
                        });
                    },
                    null,
                    'change');

                    quote.paymentMethod.subscribe(function(payment) {
                         const serviceUrl = urlBuilder.build('cleever/payment/changePayment'),
                            params = {
                                isAjax: true,
                                form_key: $('input[name="form_key"]').val(),
                                method: payment.method,
                                additional_data: payment.additional_data,
                            };

                        $.ajax({
                            url: serviceUrl,
                            data: params,
                            type: 'POST',
                            async: true
                        });
                    },
                    null,
                    'change');
                }
                $(document).on('click','.checkout', $.proxy(function () {
                    const serviceUrl = urlBuilder.build(
                            'cleever/payment/placeOrder'),
                        params = {
                            isAjax: true,
                            form_key: $('input[name="form_key"]').val(),
                            paymentMethod: $('input[name="payment[method]"]').
                                val(),
                        };
                    $.ajax({
                        url: serviceUrl,
                        data: params,
                        type: 'POST',
                        async: true
                    });
                }, this));
            },
            getSubscription: function (email) {
                var self = this;

                var serviceUrl = urlBuilder.build('cleever/payment/subscriptions'),
                    params = {
                        isAjax: true,
                        form_key: $('input[name="form_key"]').val(),
                        email: email,
                    };
                $.ajax(
                    {
                        url: serviceUrl,
                        data: params,
                        type: 'POST',
                        async: false
                    }
                ).done(function (data) {
                    if (data.status === 'success') {
                        window.checkoutConfig.cleever_subscriptions = data.subscriptions;
                        self.reloadShippinglist();
                    }
                }).fail(function (response) {
                });
            },
            reloadShippinglist: function() {
                var address = quote.shippingAddress();
                if (address !== null) {
                    shippingRateRegistry.set(address.getKey(), null);
                    shippingRateRegistry.set(address.getCacheKey(), null);
                    quote.shippingAddress(address);
                }
            },
            /**
             * Local email validation.
             *
             * @param {Boolean} focused - input focus.
             * @returns {Boolean} - validation result.
             */
            validateEmail: function (focused) {
                var loginFormSelector = 'form[data-role=email-with-possible-login]',
                    usernameSelector = loginFormSelector + ' input[name=username]',
                    loginForm = $(loginFormSelector),
                    validator,
                    valid;

                loginForm.validation();

                if (focused === false && !!this.email()) {
                    valid = !!$(usernameSelector).valid();

                    if (valid) {
                        $(usernameSelector).removeAttr('aria-invalid aria-describedby');
                    }

                    return valid;
                }

                if (loginForm.is(':visible')) {
                    validator = loginForm.validate();

                    return validator.check(usernameSelector);
                }

                return true;
            }
        });
    }
);
