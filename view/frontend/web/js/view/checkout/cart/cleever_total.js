define(
    [
        'jquery',
        'Magento_Checkout/js/view/cart/totals',
        'underscore',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'mage/cookies',
    ],
    function (
        $,
        Component,
        _,
        quote,
        priceUtils
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cleever_App/checkout/cart/cleever_total'
            },

            initialize: function () {
                const self = this;
                self._super();

                return self;
            },
            getTextWithDiscount: function () {
                if (this.getCookieDiscount()) {
                    this.lineThroughTotal();
                }
                return _('New Total');
            },
            getTotalWithDiscount: function () {
                if (this.getCookieDiscount()) {
                    this.lineThroughTotal();
                }
                return this.getTextCookieDiscount();
            },
            getCookieDiscount: function() {
                return $.mage.cookies.get('cleever_slot_product');
            },
            getTextCookieDiscount: function() {
                return this.getFormattedPrice(quote.totals().grand_total - (this.getCookieDiscount() / 100));
            },
            lineThroughTotal: function () {
                $('.grand .price').css('text-decoration','line-through');
            },
            /**
             * Format shipping price.
             * @returns {String}
             */
            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },
        });
    }
);
