define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'mage/url',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote',
        'ko',
        'Magento_Catalog/js/price-utils',
        'mage/cookies'
    ],
    function (
        $,
        Component,
        totals,
        urlBuilder,
        checkout,
        quote,
        ko,
        priceUtils
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cleever_App/checkout/summary/cleever_total'
            },
            isIncludedInSubtotal: false,
            previousMethod: -1,
            cleeverTotal: ko.observable([
                {
                    display_mode : 'text_with_discount',
                    text_with_discount: '',
                    display_with_discount: false,
                    total_with_discount: '',
                    text_without_discount: '',
                    display_without_discount: false
                }
            ]),
            already_call_total: false,
            display_with_discount: null,
            display_without_discount: null,
            request: null,
            requestAmplitude: null,

            initialize: function () {
                // By default, no payment method is selected

                const self = this;
                self._super();

                // Subscribe only if App is active
                if (window.checkoutConfig.cleever_payment.active) {
                    quote.paymentMethod.subscribe(
                        function(payment) {
                            let data = self.cleeverTotal();
                            if (payment.method === 'cleever') {
                                data['display_with_discount'] =  self.display_with_discount;
                                data['display_without_discount'] =  self.display_without_discount;
                                if (self.already_call_total === false) {
                                    self.initTotal(payment.method);
                                    data = self.cleeverTotal();
                                }
                            } else {
                                data['display_with_discount'] =  false;
                                data['display_without_discount'] =  false;
                            }
                            self.getCleeverTotal(data);
                        },
                        null,
                        'change'
                    );
                    totals.totals.subscribe(function () {
                        self.already_call_total = false;
                        self.initTotal();
                    }, this);
                }

                $(document).ajaxComplete(function() {
                    self.displayTotalMobile([]);
                });

                quote.shippingMethod.subscribe(function(method) {
                        self.displayTotalMobile([]);
                    },
                    null,
                    'change');

                this.bindEventIframePayment();

                return self;
            },

            bindEventIframePayment: function () {
                const self = this;

                window.addEventListener('message', handleEvent, false);
                function handleEvent(e) {
                    if(
                        e.data.source !== undefined &&
                        e.data.action !== undefined &&
                        e.data.source === 'App'
                    ) {
                        const action = e.data.action;
                        if(action === 'form-cb:state') {

                            let data = self.cleeverTotal();
                            if (e.data.payload.is_checked === true) {
                                self.already_call_total = false;
                                self.initTotal(null, e.data.payload.is_checked);
                                data = self.cleeverTotal();
                            } else {
                                data['display_with_discount'] =  false;
                                data['display_without_discount'] =  false;
                                $.mage.cookies.clear('cleever_slot_product');
                            }
                            self.getCleeverTotal(data);
                        }
                    }
                }
            },

            initObservable: function () {
                this.observe('cleeverTotal');
                this.displayTotalMobile([]);
                return this;
            },

            getCleeverTotal(datas) {
                this.cleeverTotal(datas);
                this.displayTotalMobile(datas);
            },

            displayTotalMobile: function(datas) {
                let check_cookie_slot_product = false;
                if (window.checkoutConfig.cleever_payment.slot_product) {
                    check_cookie_slot_product = $.mage.cookies.get('cleever_slot_product');
                }
                if (datas.length === 0) {
                    if (check_cookie_slot_product) {
                        var totalToDisplay =  '<span style="color:red;font-weight:bold;">' + this.getFormattedPrice(quote.totals().grand_total - (check_cookie_slot_product / 100)) + '</span>';
                    } else {
                        var datas = this.cleeverTotal();
                        var totalToDisplay = datas.total_with_discount;
                    }
                } else {
                    var totalToDisplay = datas.total_with_discount;
                }

                if((datas.display_mode === 'text_with_discount' && datas.display_with_discount === true) || check_cookie_slot_product) {
                    $('.estimated-block .estimated-price').css('text-decoration','line-through');
                    if ($('.cleaver-mobile-discount').length > 0) {
                        $('.cleaver-mobile-discount').remove();
                    }
                    $('.estimated-block .estimated-price').after('<span class="cleaver-mobile-discount">' + totalToDisplay + '</span>');
                } else {
                    $('.estimated-block .estimated-price').css('text-decoration','none');
                    $('.cleaver-mobile-discount').remove();
                }
            },

            getFormattedPrice: function (price) {
                return priceUtils.formatPrice(price, quote.getPriceFormat());
            },

            initTotal: function(changePayment = null, mpchecked = false) {
                let payment;
                const self = this;
                if (window.checkoutConfig.cleever_payment.slot_product) {
                    var check_cookie_slot_product = $.mage.cookies.get('cleever_slot_product');
                    if (mpchecked === false && check_cookie_slot_product) {
                        mpchecked = true;
                    }
                }
                if (changePayment === null) {
                    if (quote.paymentMethod()) {
                        payment = quote.paymentMethod().method === null ? 0 : quote.paymentMethod().method;
                    } else {
                        payment = checkout.getSelectedPaymentMethod() === null ? 0 : checkout.getSelectedPaymentMethod();
                    }
                } else {
                    payment = changePayment;
                }

                if (payment === 0 && $("input[name='payment[method]']").length === 1) {
                    payment = $("input[name='payment[method]']").val();
                }

                // Launch the request only if it's App (or nothing) and the price is updated
                if (((payment === 'cleever' || payment === 'cleever_payment')  &&
                        quote.totals().grand_total > 0)
                    && (self.already_call_total === false)
                    || mpchecked === true
                ) {
                    if ((self.previousMethod !== payment) || mpchecked === true) {
                        const serviceUrl = urlBuilder.build('cleever/payment/total'),
                            params = {
                                grand_total: quote.totals().grand_total,
                                isAjax: true,
                                form_key: $('input[name="form_key"]').val(),
                                method: payment,
                                mp_checked: mpchecked,
                            };

                        // Cancel the previous request if there is a new one
                        if (self.request !== null){
                            self.request.abort();
                            self.request = null;
                        }

                        self.request = $.ajax(
                            {
                                url: serviceUrl,
                                data: params,
                                type: 'POST',
                                async: false
                            }
                        ).done(function (data) {
                            if (data.status === 'success') {
                                self.getCleeverTotal(data);
                                self.already_call_total = true;
                                self.display_with_discount = data.display_with_discount;
                                self.display_without_discount = data.display_without_discount;
                                self.previousMethod = payment;
                                self.force = false;
                            }
                        }).fail(function (response) {
                        });
                    }
                }
                return true;
            },

            getDisplayMode: function () {
                return this.cleeverTotal().display_mode;
            },

            getTextWithDiscount: function () {
                return this.cleeverTotal().text_with_discount;
            },

            getTotalWithDiscount: function () {
                return this.cleeverTotal().total_with_discount;
            },

            getTextWithoutDiscount: function () {
                return this.cleeverTotal().text_without_discount;
            },

            getDisplayWithDiscount: function () {
                this.lineThroughTotal();
                return this.cleeverTotal().display_with_discount;
            },

            getDisplayWithoutDiscount: function () {
                this.lineThroughTotal();
                return this.cleeverTotal().display_without_discount;
            },

            lineThroughTotal: function () {
                if(this.cleeverTotal().display_mode === 'text_with_discount' && this.cleeverTotal().display_with_discount === true) {
                    $('.grand .price').css('text-decoration','line-through');
                } else {
                    $('.grand .price').css('text-decoration','none');
                }
            }
        });
    }
);
