/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor'
    ],
    function (
        $,
        urlBuilder,
        fullScreenLoader,
        errorProcessor
    ) {
        'use strict';


        return function (methodCode, display, methodTitle, payload = false) {
            var serviceUrl = urlBuilder.build('cleever/payment/iframe'),
                params = {
                    method_code: methodCode,
                    method_title: methodTitle,
                    isAjax: true,
                    form_key: $('input[name="form_key"]').val()
                };

            if(payload) {
                params['payload'] = payload;
                params['mode'] = 'integrated'
            }

            fullScreenLoader.startLoader();

            $.ajax(
                {url: serviceUrl, data: params, type: 'POST'}
            ).done(function (data) {
                if (!data) {
                    errorProcessor.process(data);
                } else {
                    if(display === 'B') {
                        var container = $('body');
                        container.prepend(data.html);
                    } else {
                        var container = $('#cleever-iframe-container');
                        container.html(data.html);
                    }
                    fullScreenLoader.stopLoader();
                }
            }).fail(function (response) {
                errorProcessor.process(response);
            });
        };

    }
);
