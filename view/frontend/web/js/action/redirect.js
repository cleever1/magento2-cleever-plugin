/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor'
    ],
    function (
        $,
        urlBuilder,
        fullScreenLoader,
        errorProcessor
    ) {
        'use strict';


        return function (methodCode, mode, methodTitle) {
            var serviceUrl = urlBuilder.build('cleever/payment/redirect'),
                params = {
                    method_code: methodCode,
                    method_title: methodTitle,
                    mode: mode,
                    isAjax: true,
                    form_key: $('input[name="form_key"]').val()
                };

            fullScreenLoader.startLoader();

            $.ajax(
                {url: serviceUrl, data: params, type: 'POST'}
            ).done(function (data) {
                fullScreenLoader.stopLoader();
                if (!data) {
                    errorProcessor.process(data);
                } else {
                    window.location = data.url;
                }
            }).fail(function (response) {
                errorProcessor.process(response);
            });
        };
    }
);
