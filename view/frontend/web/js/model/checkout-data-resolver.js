define([
    'underscore',
    'mage/utils/wrapper',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/action/select-payment-method',
    'jquery',
    'mage/cookies'
],function (_, wrapper, checkoutData, paymentService, selectShippingMethodAction, selectPaymentMethodAction, $) {
    'use strict';

    return function(checkoutDataResolver) {
        checkoutDataResolver.resolvePaymentMethod = function() {
            var availablePaymentMethods = paymentService.getAvailablePaymentMethods(),
                paymentMethod = 'cleever_payment';
            if (window.checkoutConfig.cleever_payment.slot_product) {
                const check_cookie_slot_product = $.mage.cookies.get('cleever_slot_product');
                if (availablePaymentMethods.length > 0) {
                    availablePaymentMethods.some(function (payment) {
                        if (payment.method === paymentMethod && check_cookie_slot_product) {
                            selectPaymentMethodAction(payment);
                        }
                    });
                }
            }
        };

        return checkoutDataResolver;
    };
});
