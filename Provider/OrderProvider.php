<?php

declare(strict_types=1);

namespace Cleever\App\Provider;

use Exception;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\AbstractExtensibleModel;
use Magento\Framework\Phrase;
use Magento\Payment\Model\InfoInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Api\Data\TransactionInterface;
use Magento\Sales\Api\InvoiceRepositoryInterface;
use Magento\Sales\Api\OrderPaymentRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\ShipmentRepositoryInterface;
use Magento\Sales\Model\Convert\Order as ConvertOrder;
use Magento\Sales\Model\Convert\OrderFactory as ConvertOrderFactory;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\Shipment\Item;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Service\CreditmemoService;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Service\ShipmentService;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;
use Cleever\App\Model\Method\Cleever as MethodCleever;
use Cleever\App\Model\Method\CleeverPayment;

/**
 * Order provider
 *
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class OrderProvider
{
    /**
     * Status installed
     *
     * @var string STATUS_INSTALLED
     */
    public const STATUS_INSTALLED = 'installed';
    /**
     * Status enable
     *
     * @var string STATUS_ENABLED
     */
    public const STATUS_ENABLED = 'enabled';
    /**
     * Status disabled
     *
     * @var string STATUS_DISABLED
     */
    public const STATUS_DISABLED = 'disabled';
    /**
     * Status uninstalled
     *
     * @var string STATUS_UNINSTALLED
     */
    public const STATUS_UNINSTALLED = 'uninstalled';

    /**
     * Cart management interface
     *
     * @var CartManagementInterface
     */
    protected CartManagementInterface $cartManagement;
    /**
     * Debug
     *
     * @var Debug
     */
    protected Debug $debug;
    /**
     * Config provider
     *
     * @var ConfigProvider
     */
    protected ConfigProvider $config;
    /**
     * Order repository interface
     *
     * @var OrderRepositoryInterface
     */
    protected OrderRepositoryInterface $orderRepository;
    /**
     * Order factory
     *
     * @var OrderFactory
     */
    protected OrderFactory $orderFactory;
    /**
     * Quote factory
     *
     * @var QuoteFactory
     */
    protected QuoteFactory $quoteFactory;
    /**
     * Credit memo factory
     *
     * @var CreditmemoFactory
     */
    protected CreditmemoFactory $creditmemoFactory;
    /**
     * Credit memo service
     *
     * @var CreditmemoService
     */
    protected CreditmemoService $creditmemoService;
    /**
     * Credit memo repository interface
     *
     * @var CreditmemoRepositoryInterface
     */
    protected CreditmemoRepositoryInterface $creditmemoRepository;
    /**
     * Invoice repository interface
     *
     * @var InvoiceRepositoryInterface
     */
    protected InvoiceRepositoryInterface $invoiceRepository;
    /**
     * Invoice service
     *
     * @var InvoiceService
     */
    protected InvoiceService $invoiceService;
    /**
     * Shipment repository interface
     *
     * @var ShipmentRepositoryInterface
     */
    protected ShipmentRepositoryInterface $shipmentRepository;
    /**
     * Convert order factory
     *
     * @var ConvertOrder
     */
    protected ConvertOrder $convertOrder;
    /**
     * Shipment service
     *
     * @var ShipmentService
     */
    protected ShipmentService $shipmentService;
    /**
     * Order sender
     *
     * @var OrderSender
     */
    protected OrderSender $orderSender;
    /**
     * Order payment repository interface
     *
     * @var OrderPaymentRepositoryInterface
     */
    protected OrderPaymentRepositoryInterface $orderPaymentRepository;
    /**
     * Message Manager interface
     *
     * @var ManagerInterface
     */
    protected ManagerInterface $messageManager;
    /**
     * Cleever api
     *
     * @var Api
     */
    protected Api $api;

    /**
     * PlaceOrder constructor
     *
     * @param QuoteFactory                    $quoteFactory
     * @param CartManagementInterface         $cartManagement
     * @param Debug                           $debug
     * @param ConfigProvider                  $config
     * @param OrderRepositoryInterface        $orderRepository
     * @param OrderFactory                    $orderFactory
     * @param CreditmemoRepositoryInterface   $creditmemoRepository
     * @param CreditmemoFactory               $creditmemoFactory
     * @param CreditmemoService               $creditmemoService
     * @param InvoiceRepositoryInterface      $invoiceRepository
     * @param InvoiceService                  $invoiceService
     * @param ShipmentRepositoryInterface     $shipmentRepository
     * @param ConvertOrderFactory             $convertOrderFactory
     * @param ShipmentService                 $shipmentService
     * @param OrderSender                     $orderSender
     * @param OrderPaymentRepositoryInterface $orderPaymentRepository
     * @param ManagerInterface                $messageManager
     * @param Api                             $api
     */
    public function __construct(
        QuoteFactory $quoteFactory,
        CartManagementInterface $cartManagement,
        Debug $debug,
        ConfigProvider $config,
        OrderRepositoryInterface $orderRepository,
        OrderFactory $orderFactory,
        CreditmemoRepositoryInterface $creditmemoRepository,
        CreditmemoFactory $creditmemoFactory,
        CreditmemoService $creditmemoService,
        InvoiceRepositoryInterface $invoiceRepository,
        InvoiceService $invoiceService,
        ShipmentRepositoryInterface $shipmentRepository,
        ConvertOrderFactory $convertOrderFactory,
        ShipmentService $shipmentService,
        OrderSender $orderSender,
        OrderPaymentRepositoryInterface $orderPaymentRepository,
        ManagerInterface $messageManager,
        Api $api
    ) {
        $this->quoteFactory           = $quoteFactory;
        $this->cartManagement         = $cartManagement;
        $this->debug                  = $debug;
        $this->config                 = $config;
        $this->orderRepository        = $orderRepository;
        $this->orderFactory           = $orderFactory;
        $this->creditmemoFactory      = $creditmemoFactory;
        $this->creditmemoService      = $creditmemoService;
        $this->creditmemoRepository   = $creditmemoRepository;
        $this->invoiceRepository      = $invoiceRepository;
        $this->invoiceService         = $invoiceService;
        $this->shipmentRepository     = $shipmentRepository;
        $this->shipmentService        = $shipmentService;
        $this->orderSender            = $orderSender;
        $this->orderPaymentRepository = $orderPaymentRepository;
        $this->messageManager         = $messageManager;
        $this->api                    = $api;
        $this->convertOrder           = $convertOrderFactory->create();
    }

    /**
     * Create order
     *
     * @return bool|AbstractExtensibleModel|OrderInterface|object
     */
    public function createOrder(int $cartId)
    {
        try {
            /** @var Int $orderId */
            $orderId = $this->cartManagement->placeOrder($cartId);

        } catch (Exception | CouldNotSaveException $e) {
            $this->debug->addDebugMessage(__('Error on create order : ' . $e->getMessage()));

            return false;
        }

        if (!$orderId) {
            $this->debug->addDebugMessage(__('Error on create order'));

            return false;
        }

        try {
            /** @var OrderInterface $order */
            $order = $this->orderRepository->get($orderId);
        } catch (Exception $e) {
            $this->debug->addDebugMessage(__('Error on load order : ' . $e->getMessage()));

            return false;
        }

        /** @var string $status */
        $status = $this->config->getSystemConfigValue(
            'order_status',
            $order->getStoreId(),
            MethodCleever::METHOD_CODE
        );

        $order->setStatus($status);

        /** @var string $method */
        $method = $order->getPayment()->getMethod();

        $this->config->setMethodCode($method);

        /** @var string $cleeverSessionId */
        $cleeverSessionId = $this->getCleeverSessionId($order);

        if ($method === CleeverPayment::METHOD_CODE) {
            $this->addNote($order, __('Checkout started Cleever payment session ID : ') . $cleeverSessionId);
        } else {
            $this->addNote($order, __('Checkout started Cleever discount session ID : ') . $cleeverSessionId);
        }

        return $order;
    }

    /**
     * Get Cleever session id
     *
     * @param OrderInterface $order
     *
     * @return string
     */
    private function getCleeverSessionId(OrderInterface $order): string
    {
        return $order->getPayment()->getAdditionalInformation()['cleever_session_id'];
    }

    /**
     * Add note
     *
     * @param OrderInterface $order
     * @param string         $message
     *
     * @return void
     */
    public function addNote(OrderInterface $order, string $message): void
    {
        $order->addCommentToStatusHistory($message);
        $this->orderRepository->save($order);
    }

    /**
     * Refund
     *
     * @param OrderInterface $order
     * @param float          $refundAmount
     * @param string         $refundId
     *
     * @return void
     * @throws LocalizedException
     */
    public function refundIpn(OrderInterface $order, float $refundAmount, string $refundId): void
    {
        /** @var InvoiceInterface $item */
        foreach ($order->getInvoiceCollection() as $item) {
            $invoice = $item;
            break;
        }

        if ($invoice === null) {
            throw new LocalizedException(
                __("Could not find an invoice with order ID " . $order->getIncrementId())
            );
        }

        if (!$invoice->canRefund()) {
            throw new LocalizedException(
                __("Invoice #{$invoice->getIncrementId()} cannot be (or has already been) refunded.")
            );
        }

        /** @var float|null $baseTotalNotRefunded */
        $baseTotalNotRefunded = $invoice->getBaseGrandTotal() - $invoice->getBaseTotalRefunded();
        /** @var float|null $baseToOrderRate */
        $baseToOrderRate = $order->getBaseToOrderRate();
        /** @var string $amountFormatted */
        $amountFormatted = number_format($refundAmount, 2) . ' ' . $order->getOrderCurrency()->getCurrencySymbol();

        if ($baseTotalNotRefunded < $refundAmount) {
            throw new LocalizedException(
                __("Error: Trying to refund an amount that is larger than the invoice amount")
            );
        }

        /** @var Creditmemo $creditmemo */
        $creditmemo = $this->creditmemoFactory->createByOrder($order);
        $creditmemo->setInvoice($invoice);

        if ($baseTotalNotRefunded === $refundAmount) {
            $creditmemo->setBaseSubtotal($baseTotalNotRefunded);
            $creditmemo->setSubtotal($baseTotalNotRefunded * $baseToOrderRate);
            $creditmemo->setBaseGrandTotal($refundAmount);
            $creditmemo->setGrandTotal($refundAmount * $baseToOrderRate);

            $message = __('Order fully refunded from Cleever dashboard.');
            $this->creditmemoService->refund($creditmemo, false);
        } else {
            $creditmemo->setShippingAmount(0);
            $creditmemo->setBaseSubtotal($refundAmount);
            $creditmemo->setSubtotal($refundAmount);
            $creditmemo->setAdjustmentPositive($refundAmount);
            $creditmemo->setBaseGrandTotal($refundAmount);
            $creditmemo->setGrandTotal($refundAmount);

            $message = __('Order partially refunded from Cleever dashboard.');
            $this->creditmemoService->refund($creditmemo, true);
        }

        $this->addNote($order, (string)$message);
        $this->addNote(
            $order,
            sprintf(
                (string)__('Refund processed from the Cleever dashboard. Amount : %1$s (%2$s)'),
                $amountFormatted,
                $refundId
            )
        );

        $this->creditmemoRepository->save($creditmemo);
        $this->orderRepository->save($order);
    }

    /**
     * Save invoice
     *
     * @param OrderInterface $order
     * @param string         $transactionId
     *
     * @return bool
     * @throws LocalizedException
     */
    public function saveInvoice(OrderInterface $order, string $transactionId): bool
    {
        /** @var bool $status */
        $status = false;

        if ($order->canInvoice()) {

            /** @var Invoice $invoice */
            $invoice = $this->invoiceService->prepareInvoice($order)->register();

            if ($invoice->canCapture()) {
                $invoice->capture();
            }

            $invoice->setTransactionId($transactionId);
            $this->invoiceRepository->save($invoice);
            $status = true;
        }

        return $status;
    }

    /**
     * Save shipment
     *
     * @param OrderInterface $order
     *
     * @return bool
     * @throws LocalizedException
     */
    public function saveShipment(OrderInterface $order): bool
    {
        /** @var bool $status */
        $status = false;

        if ($order->canShip()) {
            /** @var Shipment $shipment */
            $shipment = $this->convertOrder->toShipment($order);

            foreach ($order->getAllItems() as $item) {
                if (!$item->getQtyToShip() || $item->getIsVirtual()) {
                    continue;
                }

                /** @var Item $shipmentItem */
                $shipmentItem = $this->convertOrder->itemToShipmentItem($item)->setQty($item->getQtyToShip());
                $shipment->addItem($shipmentItem);
            }

            $shipment->register();
            $this->shipmentRepository->save($shipment);
            $this->shipmentService->notify($shipment->getId());
            $status = true;
        }

        return $status;
    }

    /**
     * Update Order
     *
     * @param OrderInterface $order
     * @param string         $message
     * @param string|null    $status
     *
     * @return OrderInterface
     */
    public function updateOrder(OrderInterface $order, string $message, string $status = null): OrderInterface
    {
        if (!empty($status)) {
            $order->setState($status);
            $order->addStatusToHistory($status, $message, true);
        } else {
            $this->addNote($order, $message);
        }

        if ($order->getCanSendNewEmailFlag()) {
            $this->orderSender->send($order);
        }

        $this->orderRepository->save($order);

        return $order;
    }

    /**
     * Add transaction
     *
     * @param OrderInterface $order
     * @param mixed[]        $transactionAdditionalInfo
     *
     * @return void
     */
    public function addTransaction(OrderInterface $order, array $transactionAdditionalInfo): void
    {
        if ($order->getId() && $order->getPayment() && $order->getPayment()->getMethod()) {
            /** @var OrderPaymentInterface $payment */
            $payment = $order->getPayment();
            $payment->setTransactionId($transactionAdditionalInfo['transaction_id']);

            foreach ($transactionAdditionalInfo as $key => $value) {
                $payment->setTransactionAdditionalInfo($key, $value);
            }

            $payment->addTransaction(
                TransactionInterface::TYPE_CAPTURE,
                null,
                true
            );

            $this->orderPaymentRepository->save($payment);
            $this->orderRepository->save($order);
        }
    }

    /**
     * Refund
     *
     * @param InfoInterface $payment
     * @param float         $amount
     *
     * @return void
     * @throws Exception
     */
    public function refund(InfoInterface $payment, $amount): void
    {
        /** @var OrderInterface $order */
        $order = $this->orderRepository->get($payment->getOrder()->getId());

        if (!$order) {
            throw new LocalizedException(__('Order is not found'));
        }

        /** @var float $amount */
        $amount = (float)(empty($amount)) ? (float)$order->getBaseTotalPaid() : $amount;
        $amount = (float)$amount * $order->getBaseToOrderRate();
        /** @var string $amountFormatted */
        $amountFormatted = number_format($amount, 2) . ' ' . $order->getOrderCurrency()->getCurrencySymbol();
        /** @var int $transactionId */
        $transactionId = $payment->getParentTransactionId();

        if (empty($transactionId)) {
            $transactionId = $payment->getLastTransId();
        }

        if (empty($transactionId)) {
            $message = sprintf(
                (string)__("Failed to send refund of %s to Cleever payment (no charge ID associated)."),
                $amountFormatted
            );
            $this->addNote($order, $message);
            throw new LocalizedException(__($message));
        }

        /** @var mixed[] $dataApi */
        $dataApi = [
            'amount'                => (isset($amount)) ? round(number_format((float)$amount, 2, '.', '') * 100) : null,
            'transaction_id'        => $transactionId,
            'order_id'              => $order->getIncrementId(),
            'reason'                => '',
            'cleever_discount_amount' => $payment->getAdditionalInformation(
                'cleever_discount_amount'
            ),
            'api_version'           => '1',
        ];

        /** @var string[] $response */
        $response = $this->api->sendRefund($dataApi, (int)$order->getStoreId());
        /** @var mixed $body */
        $body = json_decode($response['response'], true);

        if ($response['error'] && $body["error"]["message"] !== 'Refund already done') {
            if ($body["error"]["message"] && is_string($body["error"]["message"])) {
                $messageBo = __('There is an issue. Retry later or contact your administrator. It will have more information in the log file (cleever.log). Failed to send refund of ' . $amountFormatted . ' to Cleever
            (' . $transactionId . ' => ' . $body["error"]["code"] . " : " . $body["error"]["message"] . ').');
                $message = __('Failed to send refund of '. $amountFormatted . ' to Cleever
            (' . $transactionId . ' => ' . $body["error"]["code"] . " : " . $body["error"]["message"] . ').');
            } else {
                $messageBo = __('There is an issue. Retry later or contact your administrator. It will have more information in the log file (cleever.log). Failed to send refund of ' . $amountFormatted . ' to Cleever
            (' . $transactionId . ' => ' . $body["error"]["code"]);
                $message = __('Failed to send refund of ' . $amountFormatted . ' to Cleever (' . $transactionId . ' => ' . $body["error"]["code"]);
            }

            $this->debug->addDebugMessage($message);
            $this->debug->addDebugMessage($response['response']);

            throw new LocalizedException($messageBo);
        }

        if (!$response['error']) {
            /** @var Phrase $message */
            $message = __("Refunded : {$amountFormatted} - Payment ID : {$transactionId} - Reason: ");
            $message .= (isset($body['refundId'])) ? __(" - Refund ID: " . $body['refundId'] . "") : '';
            $message .= (isset($body['refundCleeverId'])) ? __(" - Refund Cleever ID: " . $body['refundCleeverId'] . "") : '';
            $this->addNote($order, $message);

            if (isset($body['isFullRefund']) && $body['isFullRefund'] === true && (isset($body['refundCleeverId']) || (isset($body['transfer_group']) && strncmp(
                $body['transfer_group'], "group_", 6) !== 0))) {
                $this->addNote($order, 'Full refund processed');

                /** @var float $maxRefund */
                $maxRefund = $order->getGrandTotal() - ($order->getTotalRefunded() + $amount);

                if ($maxRefund) {
                    /** @var String $message */
                    $message = 'WARNING : A part of this transaction was funded by Cleever.
                        The amount you refunded exceeds the part paid by the shopper,
                        and therefore Stripe refunded the Cleever funding as well.
                        You should enter the total transaction amount as refunded
                        here so that your accounting line matches the order.';
                    $this->addNote($order, $message);
                    $this->messageManager->addWarningMessage($message);
                }
            }
        }
    }
}
