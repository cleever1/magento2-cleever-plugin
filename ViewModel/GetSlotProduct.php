<?php

declare(strict_types=1);

namespace Cleever\App\ViewModel;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Cleever\App\Model\Api;
use Cleever\App\Model\ConfigProvider;
use Cleever\App\Model\Debug;

/**
 * @author    Cleever <contact@cleever.com>
 * @copyright 2022 Processing Technology Ltd, Inc. All rights reserved.
 * @license   https://opensource.org/licenses/osl-3.0.php (OSL 3.0)
 * @link      https://www.cleever.com/
 */
class GetSlotProduct implements ArgumentInterface
{
    protected ConfigProvider $configProvider;
    protected Api $api;
    protected Resolver $locale;
    protected Registry $registry;
    protected Debug $debug;
    protected PriceCurrencyInterface $priceCurrency;

    /**
     * @param ConfigProvider $configProvider
     * @param Api $api
     * @param Resolver $locale
     * @param Registry $registry
     * @param Debug $debug
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        ConfigProvider $configProvider,
        Api $api,
        Resolver $locale,
        Registry $registry,
        Debug $debug,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->configProvider = $configProvider;
        $this->api = $api;
        $this->locale = $locale;
        $this->registry = $registry;
        $this->debug = $debug;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function getIsActive(): bool
    {
        return $this->configProvider->getSlotProductActive();
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function getSlotProductInfos(): array
    {
        $default = [
            'title' => 'Apply £5 discount now',
            'description' => "Get £5 now and unlock 100's of offers on YCoupon.<br/><b>3-day FREE trial, then £29.99 per month.</b><br/><b>Cancel anytime.</b>",
            'discount' => 500,
        ];
        $productSlotInfos = $this->api->getProductInfos([
                'lang' => $this->locale->getLocale(),
                'price' => $this->getProduct()->getFinalPrice(),
            ]
        );

        if ($productSlotInfos['error']) {
            $this->debug->addDebugMessage('An error has occured with api product_page_offer. Error: ' . $productSlotInfos['response']);

            return $default;
        }

        $productSlotInfos = json_decode($productSlotInfos['response'], true);

        if (!isset($productSlotInfos['discount'])) {
            return $default;
        }

        $productSlotInfos['currency'] = $this->priceCurrency->getCurrency()->getCurrencySymbol();

        return $productSlotInfos;
    }

    /**
     * @return ProductInterface|null
     */
    public function getProduct(): ?ProductInterface
    {
        return $this->registry->registry('product');
    }

}
